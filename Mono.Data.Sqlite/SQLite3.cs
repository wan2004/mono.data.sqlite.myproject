using System;
using System.Data;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
namespace Mono.Data.Sqlite
{
	internal class SQLite3 : SQLiteBase
	{
		protected SqliteConnectionHandle _sql = null;
		protected string _fileName;
		protected bool _usePool;
		protected int _poolVersion;
		private bool _buildingSchema;
		protected SqliteFunction[] _functionsArray;
		
		static SQLite3()
		{
#if DEBUG 
			System.Console.Error.WriteLine("## sqlite3_initialize ");
#endif			
			
			UnsafeNativeMethods.my_sqlite3_init();
#if DEBUG 
			System.Console.Error.WriteLine("## sqlite3_initialize End");
#endif
		}
		
		internal override SqliteConnectionHandle GetSqliteHandle()
		{
			return _sql;
		}
		
		internal override bool IsOpen()
	    {
	        return (_sql != null) && !_sql.IsInvalid && !_sql.IsClosed;
	    }
		
		internal override string Version
		{
			get
			{
				return SQLite3.SQLiteVersion;
			}
		}
		internal static string SQLiteVersion
		{
			get
			{
				return SqliteConvert.UTF8ToString(UnsafeNativeMethods.my_sqlite3_libversion(), -1);
			}
		}
		internal override int Changes
		{
			get
			{
				return UnsafeNativeMethods.my_sqlite3_changes(this._sql);
			}
		}
		internal SQLite3(SQLiteDateFormats fmt) : base(fmt)
		{
		}

		protected override void Dispose(bool bDisposing)
		{
			if (bDisposing)
			{
				this.Close();
			}
		}
		internal override void Close()
		{
			if (this._sql != null)
			{
				if (this._usePool)
				{
					SQLiteBase.ResetConnection(this._sql);
					SqliteConnectionPool.Add(this._fileName, this._sql, this._poolVersion);
				}
				else
				{
					if(this._sql!=null)
						this._sql.Dispose();
				}
			}
			this._sql = null;
		}
		
		internal override void Cancel()
		{
			UnsafeNativeMethods.my_sqlite3_interrupt(this._sql);
		}
		
		internal override void Open(string strFilename, SQLiteOpenFlagsEnum flags, int maxPoolSize, bool usePool)
		{
			if (this._sql != null)
			{
//				if(!initHandler)Close();
				return;
			}
			
			this._usePool = usePool;
			if (usePool)
			{
				this._fileName = strFilename;
				this._sql = SqliteConnectionPool.Remove(strFilename, maxPoolSize, out this._poolVersion);
			}
			
			if (this._sql == null)
			{
				IntPtr db = IntPtr.Zero;
				int num = 0;
				try{
					

					num = UnsafeNativeMethods.my_sqlite3_open_v2(SqliteConvert.ToUTF8(strFilename), out db, (int)flags, IntPtr.Zero);
//					num = Sqlite3Extend.OpenDataBase(strFilename,out db, (int)flags);
					
					if (num > 0)
					{
						throw new SqliteException(num, "my_sqlite3_open_v2 fail");
					}
//					lock(db){ /* lock handle */ }
					_sql = new SqliteConnectionHandle(db);
				}
				catch(Exception e)
				{
#if DEBUG					
					if(_sql !=null)
						Console.Error.WriteLine(" SQLite3 Open ErrorMsg = " + SQLiteLastError() + " \nexception = " + e.Message);	
					else
						Console.Error.WriteLine(" SQLite3 Open Fail relnum =  "+ num+" strFilename = "+strFilename +" flags=  "+flags.ToString()+"  usePool " + usePool +"  \nMessage = "+ e.Message);
#endif			
					throw e;
				}
				
				lock(_sql){ /* lock handle */ }
				
			}
			this._functionsArray = SqliteFunction.BindFunctions(this);
			this.SetTimeout(0);
			
		}
		
		internal override void ClearPool()
		{
			SqliteConnectionPool.ClearPool(this._fileName);
		}
		internal override void SetTimeout(int nTimeoutMS)
		{
			int num = UnsafeNativeMethods.my_sqlite3_busy_timeout(this._sql, nTimeoutMS);
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
		}
//		internal override bool Step(SqliteStatement stmt)
//		{
//			Random random = null;
//			uint tickCount = (uint)Environment.TickCount;
//			uint num = (uint)(stmt._command._commandTimeout * 1000);
//			int num2;
//			int num3;
//			while (true)
//			{
//				num2 = UnsafeNativeMethods.sqlite3_step(stmt._sqlite_stmt);
//				if (num2 == 100)
//				{
//					break;
//				}
//				if (num2 == 101)
//				{
//					return false;
//				}
//				if (num2 > 0)
//				{
//					num3 = this.Reset(stmt);
//					if (num3 == 0)
//					{
//						goto Block_4;
//					}
//					if ((num3 == 6 || num3 == 5) && stmt._command != null)
//					{
//						if (random == null)
//						{
//							random = new Random();
//						}
//						if (Environment.TickCount - (int)tickCount > (int)num)
//						{
//							goto Block_8;
//						}
//						Thread.CurrentThread.Join(random.Next(1, 150));
//					}
//				}
//			}
//			return true;
//			Block_4:
//			throw new SqliteException(num2, this.SQLiteLastError());
//			Block_8:
//			throw new SqliteException(num3, this.SQLiteLastError());
//		}
		
	  	internal override bool Step(SqliteStatement stmt)
	    {
#if DEBUG
			Console.WriteLine(" SQLite3 Reset " + stmt);		
#endif			
			SQLiteErrorCode n;
			Random rnd = null;
			uint starttick = (uint)Environment.TickCount;
			uint timeout = (uint)(stmt._command._commandTimeout * 1000);
			
			while (true)
			{
				n = (SQLiteErrorCode)UnsafeNativeMethods.my_sqlite3_step(stmt._sqlite_stmt);
				
				if (n == SQLiteErrorCode.Row) return true;
				if (n == SQLiteErrorCode.Done) return false;
				
				if (n != SQLiteErrorCode.Ok)
				{
				  SQLiteErrorCode r;
				
				  // An error occurred, attempt to reset the statement.  If the reset worked because the
				  // schema has changed, re-try the step again.  If it errored our because the database
				  // is locked, then keep retrying until the command timeout occurs.
				  r = (SQLiteErrorCode)Reset(stmt);
				
				  if (r == SQLiteErrorCode.Ok)
				    throw new SqliteException((int)n,SQLiteLastError() );
				
				  else if ((r == SQLiteErrorCode.Locked || r == SQLiteErrorCode.Busy) && stmt._command != null)
				  {
				    // Keep trying
				    if (rnd == null) // First time we've encountered the lock
				      rnd = new Random();
				
				    // If we've exceeded the command's timeout, give up and throw an error
				    if ((uint)Environment.TickCount - starttick > timeout)
				    {
				      throw new SqliteException((int)r,SQLiteLastError() );
				    }
				    else
				    {
				      // Otherwise sleep for a random amount of time up to 150ms
				      System.Threading.Thread.Sleep(rnd.Next(1, 150));
				    }
				  }
				}
			}
	    }
		
		internal override int Reset(SqliteStatement stmt)
		{
#if DEBUG
			Console.WriteLine("### SQLite3 Reset " + stmt);		
#endif
			int num = UnsafeNativeMethods.my_sqlite3_reset(stmt._sqlite_stmt);
			if (num == (int)SQLiteErrorCode.Schema)
			{
				string text;
				using (SqliteStatement sqliteStatement = this.Prepare(null, stmt._sqlStatement, null, (uint)(stmt._command._commandTimeout * 1000), out text))
				{
					stmt._sqlite_stmt.Dispose();
					stmt._sqlite_stmt = sqliteStatement._sqlite_stmt;
					sqliteStatement._sqlite_stmt = null;
					stmt.BindParameters();
				}
				return -1;
			}
			if (num == (int)SQLiteErrorCode.Locked || num == (int)SQLiteErrorCode.Busy)
			{
				return num;
			}
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
			return 0;
		}
		internal override string SQLiteLastError()
		{
			return SQLiteBase.SQLiteLastError(this._sql);
		}
		internal override SqliteStatement Prepare(SqliteConnection cnn, string strSql, SqliteStatement previous, uint timeoutMS, out string strRemain)
		{
			IntPtr stmt = IntPtr.Zero;
			IntPtr remainPtr = IntPtr.Zero;
			int nativestringlen = 0;
			int num = (int)SQLiteErrorCode.Schema;
			int num2 = (int)SQLiteErrorCode.Ok;
			byte[] array = SqliteConvert.ToUTF8(strSql);
			SqliteStatement sqliteStatement = null;
			Random random = null;
			uint tickCount = (uint)Environment.TickCount;
			GCHandle gCHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
			IntPtr pSql = gCHandle.AddrOfPinnedObject();
			SqliteStatement result;
			try
			{
				while ((num == (int)SQLiteErrorCode.Schema || num == (int)SQLiteErrorCode.Locked || num == (int)SQLiteErrorCode.Busy) && num2 < 3)
				{
					num = UnsafeNativeMethods.my_sqlite3_prepare(this._sql, pSql, array.Length - 1, out stmt, out remainPtr);
					nativestringlen = -1;
					if (num == (int)SQLiteErrorCode.Schema)
					{
						num2++;
					}
					else
					{
						if (num == (int)SQLiteErrorCode.Error)
						{
							if (string.Compare(this.SQLiteLastError(), "near \"TYPES\": syntax error", StringComparison.OrdinalIgnoreCase) == 0)
							{
								int num3 = strSql.IndexOf(';');
								if (num3 == -1)
								{
									num3 = strSql.Length - 1;
								}
								string types = strSql.Substring(0, num3 + 1);
								strSql = strSql.Substring(num3 + 1);
								strRemain = string.Empty;
								while (sqliteStatement == null && strSql.Length > 0)
								{
									sqliteStatement = this.Prepare(cnn, strSql, previous, timeoutMS, out strRemain);
									strSql = strRemain;
								}
								if (sqliteStatement != null)
								{
									sqliteStatement.SetTypes(types);
								}
								result = sqliteStatement;
								return result;
							}
							if (!this._buildingSchema && string.Compare(this.SQLiteLastError(), 0, "no such table: TEMP.SCHEMA", 0, 26, StringComparison.OrdinalIgnoreCase) == 0)
							{
								strRemain = string.Empty;
								this._buildingSchema = true;
								try
								{
									ISQLiteSchemaExtensions iSQLiteSchemaExtensions = ((IServiceProvider)SqliteFactory.Instance).GetService(typeof(ISQLiteSchemaExtensions)) as ISQLiteSchemaExtensions;
									if (iSQLiteSchemaExtensions != null)
									{
										iSQLiteSchemaExtensions.BuildTempSchema(cnn);
									}
									while (sqliteStatement == null && strSql.Length > 0)
									{
										sqliteStatement = this.Prepare(cnn, strSql, previous, timeoutMS, out strRemain);
										strSql = strRemain;
									}
									result = sqliteStatement;
									return result;
								}
								finally
								{
									this._buildingSchema = false;
								}
							}
						}
						else
						{
							if ( num == (int)SQLiteErrorCode.Locked || num == (int)SQLiteErrorCode.Busy )
							{
								if (random == null)
								{
									random = new Random();
								}
								if (Environment.TickCount - (int)tickCount > (int)timeoutMS)
								{
									throw new SqliteException(num, this.SQLiteLastError());
								}
								Thread.CurrentThread.Join(random.Next(1, 150));
							}
						}
					}
				}
				if (num > 0)
				{
					throw new SqliteException(num, this.SQLiteLastError());
				}
				strRemain = SqliteConvert.UTF8ToString(remainPtr, nativestringlen);
				if (stmt != IntPtr.Zero)
				{
					SqliteStatementHandle st = new SqliteStatementHandle(stmt,_sql);
					sqliteStatement = new SqliteStatement(this, st , strSql.Substring(0, strSql.Length - strRemain.Length), previous);
				}
				result = sqliteStatement;
			}
			finally
			{
				gCHandle.Free();
			}
			return result;
		}
		internal override void Bind_Double(SqliteStatement stmt, int index, double value)
		{
			int num = UnsafeNativeMethods.my_sqlite3_bind_double(stmt._sqlite_stmt, index, value);
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
		}
		internal override void Bind_Int32(SqliteStatement stmt, int index, int value)
		{
			int num = UnsafeNativeMethods.my_sqlite3_bind_int(stmt._sqlite_stmt, index, value);
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
		}
		internal override void Bind_Int64(SqliteStatement stmt, int index, long value)
		{
			int num = UnsafeNativeMethods.my_sqlite3_bind_int64(stmt._sqlite_stmt, index, value);
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
		}
		internal override void Bind_Text(SqliteStatement stmt, int index, string value)
		{
			byte[] array = SqliteConvert.ToUTF8(value);
			int num = UnsafeNativeMethods.my_sqlite3_bind_text(stmt._sqlite_stmt, index, array, array.Length - 1, (IntPtr)(-1));
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
		}
		internal override void Bind_DateTime(SqliteStatement stmt, int index, DateTime dt)
		{
			byte[] array = base.ToUTF8(dt);
			int num = UnsafeNativeMethods.my_sqlite3_bind_text(stmt._sqlite_stmt, index, array, array.Length - 1, (IntPtr)(-1));
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
		}
		internal override void Bind_Blob(SqliteStatement stmt, int index, byte[] blobData)
		{
			int num = UnsafeNativeMethods.my_sqlite3_bind_blob(stmt._sqlite_stmt, index, blobData, blobData.Length, (IntPtr)(-1));
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
		}
		internal override void Bind_Null(SqliteStatement stmt, int index)
		{
			int num = UnsafeNativeMethods.my_sqlite3_bind_null(stmt._sqlite_stmt, index);
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
		}
		internal override int Bind_ParamCount(SqliteStatement stmt)
		{
			return UnsafeNativeMethods.my_sqlite3_bind_parameter_count(stmt._sqlite_stmt);
		}
		internal override string Bind_ParamName(SqliteStatement stmt, int index)
		{
			return SqliteConvert.UTF8ToString(UnsafeNativeMethods.my_sqlite3_bind_parameter_name(stmt._sqlite_stmt, index), -1);
		}
		internal override int Bind_ParamIndex(SqliteStatement stmt, string paramName)
		{
			return UnsafeNativeMethods.my_sqlite3_bind_parameter_index(stmt._sqlite_stmt, SqliteConvert.ToUTF8(paramName));
		}
		internal override int ColumnCount(SqliteStatement stmt)
		{
			return UnsafeNativeMethods.my_sqlite3_column_count(stmt._sqlite_stmt);
		}
		internal override string ColumnName(SqliteStatement stmt, int index)
		{
			return SqliteConvert.UTF8ToString(UnsafeNativeMethods.my_sqlite3_column_name(stmt._sqlite_stmt, index), -1);
		}
		internal override TypeAffinity ColumnAffinity(SqliteStatement stmt, int index)
		{
			return UnsafeNativeMethods.my_sqlite3_column_type(stmt._sqlite_stmt, index);
		}
		internal override string ColumnType(SqliteStatement stmt, int index, out TypeAffinity nAffinity)
		{
			int nativestringlen = -1;
			IntPtr intPtr = UnsafeNativeMethods.my_sqlite3_column_decltype(stmt._sqlite_stmt, index);
			nAffinity = this.ColumnAffinity(stmt, index);
			if (intPtr != IntPtr.Zero)
			{
				return SqliteConvert.UTF8ToString(intPtr, nativestringlen);
			}
			string[] typeDefinitions = stmt.TypeDefinitions;
			if (typeDefinitions != null && index < typeDefinitions.Length && typeDefinitions[index] != null)
			{
				return typeDefinitions[index];
			}
			return string.Empty;
		}
		internal override int ColumnIndex(SqliteStatement stmt, string columnName)
		{
			int num = this.ColumnCount(stmt);
			for (int i = 0; i < num; i++)
			{
				if (string.Compare(columnName, this.ColumnName(stmt, i), true, CultureInfo.InvariantCulture) == 0)
				{
					return i;
				}
			}
			return -1;
		}
		internal override string ColumnOriginalName(SqliteStatement stmt, int index)
		{
			return SqliteConvert.UTF8ToString(UnsafeNativeMethods.my_sqlite3_column_origin_name(stmt._sqlite_stmt, index), -1);
		}
		internal override string ColumnDatabaseName(SqliteStatement stmt, int index)
		{
			return SqliteConvert.UTF8ToString(UnsafeNativeMethods.my_sqlite3_column_database_name(stmt._sqlite_stmt, index), -1);
		}
		internal override string ColumnTableName(SqliteStatement stmt, int index)
		{
			return SqliteConvert.UTF8ToString(UnsafeNativeMethods.my_sqlite3_column_table_name(stmt._sqlite_stmt, index), -1);
		}
		internal override void ColumnMetaData(string dataBase, string table, string column, out string dataType, out string collateSequence, out bool notNull, out bool primaryKey, out bool autoIncrement)
		{
			int nativestringlen = -1;
			int nativestringlen2 = -1;
			IntPtr nativestring;
			IntPtr nativestring2;
			int num2;
			int num3;
			int num4;
			int num = UnsafeNativeMethods.my_sqlite3_table_column_metadata(this._sql, SqliteConvert.ToUTF8(dataBase), SqliteConvert.ToUTF8(table), SqliteConvert.ToUTF8(column), out nativestring, out nativestring2, out num2, out num3, out num4);
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
			dataType = SqliteConvert.UTF8ToString(nativestring, nativestringlen);
			collateSequence = SqliteConvert.UTF8ToString(nativestring2, nativestringlen2);
			notNull = (num2 == 1);
			primaryKey = (num3 == 1);
			autoIncrement = (num4 == 1);
		}
		internal override double GetDouble(SqliteStatement stmt, int index)
		{
			return UnsafeNativeMethods.my_sqlite3_column_double(stmt._sqlite_stmt, index);
		}
		internal override int GetInt32(SqliteStatement stmt, int index)
		{
			return UnsafeNativeMethods.my_sqlite3_column_int(stmt._sqlite_stmt, index);
		}
		internal override long GetInt64(SqliteStatement stmt, int index)
		{
			return UnsafeNativeMethods.my_sqlite3_column_int64(stmt._sqlite_stmt, index);
		}
		internal override string GetText(SqliteStatement stmt, int index)
		{
			return SqliteConvert.UTF8ToString(UnsafeNativeMethods.my_sqlite3_column_text(stmt._sqlite_stmt, index), -1);
		}
		internal override DateTime GetDateTime(SqliteStatement stmt, int index)
		{
			return base.ToDateTime(UnsafeNativeMethods.my_sqlite3_column_text(stmt._sqlite_stmt, index), -1);
		}
		
		internal override long GetBytes(SqliteStatement stmt, int index, int nDataOffset, byte[] bDest, int nStart, int nLength)
	    {
	      int nlen = UnsafeNativeMethods.my_sqlite3_column_bytes(stmt._sqlite_stmt, index);
	
	      // If no destination buffer, return the size needed.
	      if (bDest == null) return nlen;
	
	      int nCopied = nLength;
	
	      if (nCopied + nStart > bDest.Length) nCopied = bDest.Length - nStart;
	      if (nCopied + nDataOffset > nlen) nCopied = nlen - nDataOffset;
	
	      if (nCopied > 0)
	      {
	        IntPtr ptr = UnsafeNativeMethods.my_sqlite3_column_blob(stmt._sqlite_stmt, index);
	
	        Marshal.Copy((IntPtr)(ptr.ToInt64() + nDataOffset), bDest, nStart, nCopied);
	      }
	      else
	      {
	        nCopied = 0;
	      }
	
	      return nCopied;
	    }
		
		internal override long GetChars(SqliteStatement stmt, int index, int nDataOffset, char[] bDest, int nStart, int nLength)
		{
			int num = nLength;
			string text = this.GetText(stmt, index);
			int length = text.Length;
			if (bDest == null)
			{
				return (long)length;
			}
			if (num + nStart > bDest.Length)
			{
				num = bDest.Length - nStart;
			}
			if (num + nDataOffset > length)
			{
				num = length - nDataOffset;
			}
			if (num > 0)
			{
				text.CopyTo(nDataOffset, bDest, nStart, num);
			}
			else
			{
				num = 0;
			}
			return (long)num;
		}
		internal override bool IsNull(SqliteStatement stmt, int index)
		{
			return this.ColumnAffinity(stmt, index) == TypeAffinity.Null;
		}
		internal override int AggregateCount(IntPtr context)
		{
			return UnsafeNativeMethods.my_sqlite3_aggregate_count(context);
		}
		internal override void CreateFunction(string strFunction, int nArgs, bool needCollSeq, SQLiteCallback func, SQLiteCallback funcstep, SQLiteFinalCallback funcfinal)
		{
			int num = UnsafeNativeMethods.my_sqlite3_create_function(this._sql, SqliteConvert.ToUTF8(strFunction), nArgs, 4, IntPtr.Zero, func, funcstep, funcfinal);
			if (num == 0)
			{
				num = UnsafeNativeMethods.my_sqlite3_create_function(this._sql, SqliteConvert.ToUTF8(strFunction), nArgs, 1, IntPtr.Zero, func, funcstep, funcfinal);
			}
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
		}
		internal override void CreateCollation(string strCollation, SQLiteCollation func, SQLiteCollation func16)
		{
			int num = UnsafeNativeMethods.my_sqlite3_create_collation(this._sql, SqliteConvert.ToUTF8(strCollation), 2, IntPtr.Zero, func16);
			if (num == 0)
			{
				UnsafeNativeMethods.my_sqlite3_create_collation(this._sql, SqliteConvert.ToUTF8(strCollation), 1, IntPtr.Zero, func);
			}
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
		}
		internal override int ContextCollateCompare(CollationEncodingEnum enc, IntPtr context, string s1, string s2)
		{
			throw new NotImplementedException();
		}
		internal override int ContextCollateCompare(CollationEncodingEnum enc, IntPtr context, char[] c1, char[] c2)
		{
			throw new NotImplementedException();
		}
		internal override CollationSequence GetCollationSequence(SqliteFunction func, IntPtr context)
		{
			throw new NotImplementedException();
		}
		
		internal override long GetParamValueBytes(IntPtr p, int nDataOffset, byte[] bDest, int nStart, int nLength)
	    {
	      int nlen = UnsafeNativeMethods.my_sqlite3_value_bytes(p);
	
	      // If no destination buffer, return the size needed.
	      if (bDest == null) return nlen;
	
	      int nCopied = nLength;
	
	      if (nCopied + nStart > bDest.Length) nCopied = bDest.Length - nStart;
	      if (nCopied + nDataOffset > nlen) nCopied = nlen - nDataOffset;
	
	      if (nCopied > 0)
	      {
	        IntPtr ptr = UnsafeNativeMethods.my_sqlite3_value_blob(p);
	
	        Marshal.Copy((IntPtr)(ptr.ToInt64() + nDataOffset), bDest, nStart, nCopied);
	      }
	      else
	      {
	        nCopied = 0;
	      }
	
	      return nCopied;
	    }
		internal override double GetParamValueDouble(IntPtr ptr)
		{
			return UnsafeNativeMethods.my_sqlite3_value_double(ptr);
		}
		internal override int GetParamValueInt32(IntPtr ptr)
		{
			return UnsafeNativeMethods.my_sqlite3_value_int(ptr);
		}
		internal override long GetParamValueInt64(IntPtr ptr)
		{
			return UnsafeNativeMethods.my_sqlite3_value_int64(ptr);
		}
		internal override string GetParamValueText(IntPtr ptr)
		{
			return SqliteConvert.UTF8ToString(UnsafeNativeMethods.my_sqlite3_value_text(ptr), -1);
		}
		internal override TypeAffinity GetParamValueType(IntPtr ptr)
		{
			return UnsafeNativeMethods.my_sqlite3_value_type(ptr);
		}
		internal override void ReturnBlob(IntPtr context, byte[] value)
		{
			UnsafeNativeMethods.my_sqlite3_result_blob(context, value, value.Length, (IntPtr)(-1));
		}
		internal override void ReturnDouble(IntPtr context, double value)
		{
			UnsafeNativeMethods.my_sqlite3_result_double(context, value);
		}
		internal override void ReturnError(IntPtr context, string value)
		{
			UnsafeNativeMethods.my_sqlite3_result_error(context, SqliteConvert.ToUTF8(value), value.Length);
		}
		internal override void ReturnInt32(IntPtr context, int value)
		{
			UnsafeNativeMethods.my_sqlite3_result_int(context, value);
		}
		internal override void ReturnInt64(IntPtr context, long value)
		{
			UnsafeNativeMethods.my_sqlite3_result_int64(context, value);
		}
		internal override void ReturnNull(IntPtr context)
		{
			UnsafeNativeMethods.my_sqlite3_result_null(context);
		}
		internal override void ReturnText(IntPtr context, string value)
		{
			byte[] array = SqliteConvert.ToUTF8(value);
			UnsafeNativeMethods.my_sqlite3_result_text(context, SqliteConvert.ToUTF8(value), array.Length - 1, (IntPtr)(-1));
		}
		internal override IntPtr AggregateContext(IntPtr context)
		{
			return UnsafeNativeMethods.my_sqlite3_aggregate_context(context, 1);
		}
		internal override void SetPassword(byte[] passwordBytes)
		{
			int num = UnsafeNativeMethods.my_sqlite3_key(this._sql, passwordBytes, passwordBytes.Length);
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
		}
		internal override void ChangePassword(byte[] newPasswordBytes)
		{
			int num = UnsafeNativeMethods.my_sqlite3_rekey(this._sql, newPasswordBytes, (newPasswordBytes != null) ? newPasswordBytes.Length : 0);
			if (num > 0)
			{
				throw new SqliteException(num, this.SQLiteLastError());
			}
		}
		internal override void SetUpdateHook(SQLiteUpdateCallback func)
		{
			UnsafeNativeMethods.my_sqlite3_update_hook(this._sql, func, IntPtr.Zero);
		}
		internal override void SetCommitHook(SQLiteCommitCallback func)
		{
			UnsafeNativeMethods.my_sqlite3_commit_hook(this._sql, func, IntPtr.Zero);
		}
		internal override void SetRollbackHook(SQLiteRollbackCallback func)
		{
			UnsafeNativeMethods.my_sqlite3_rollback_hook(this._sql, func, IntPtr.Zero);
		}
		 /// <summary>
	    /// Helper function to retrieve a column of data from an active statement.
	    /// </summary>
	    /// <param name="stmt">The statement being step()'d through</param>
	    /// <param name="flags">The flags associated with the connection.</param>
	    /// <param name="index">The column index to retrieve</param>
	    /// <param name="typ">The type of data contained in the column.  If Uninitialized, this function will retrieve the datatype information.</param>
	    /// <returns>Returns the data in the column</returns>
	    internal override object GetValue(SqliteStatement stmt, int index, SQLiteType typ)
	    {
	      TypeAffinity aff = typ.Affinity;
	      if (aff == TypeAffinity.Null) return DBNull.Value;
	      Type t = null;
	
	      if (typ.Type != DbType.Object)
	      {
	        t = SqliteConvert.SQLiteTypeToType(typ);
	        aff = TypeToAffinity(t);
	      }
	
	
	      switch (aff)
	      {
	        case TypeAffinity.Blob:
	          if (typ.Type == DbType.Guid && typ.Affinity == TypeAffinity.Text)
		       {
		           return new Guid(this.GetText(stmt, index));
		       }
		       int num = (int)this.GetBytes(stmt, index, 0, null, 0, 0);
		       byte[] array = new byte[num];
		       this.GetBytes(stmt, index, 0, array, 0, num);
		       if (typ.Type == DbType.Guid && num == 16)
		       {
		           return new Guid(array);
		       }
		       return array;

	        case TypeAffinity.DateTime:
	          return GetDateTime(stmt, index);
	        case TypeAffinity.Double:
	          if (t == null) return GetDouble(stmt, index);
	          return Convert.ChangeType(GetDouble(stmt, index), t, null);
	        case TypeAffinity.Int64:
	          if (t == null) return GetInt64(stmt, index);
	          if (t == typeof(Int32)) return GetInt32(stmt, index);
	          return Convert.ChangeType(GetInt64(stmt, index), t, null);
	        default:
	          return GetText(stmt, index);
	      }
	    }
		internal override int GetCursorForTable(SqliteStatement stmt, int db, int rootPage)
		{
			return -1;
		}
		internal override long GetRowIdForCursor(SqliteStatement stmt, int cursor)
		{
			return 0L;
		}
		internal override void GetIndexColumnExtendedInfo(string database, string index, string column, out int sortMode, out int onError, out string collationSequence)
		{
			sortMode = 0;
			onError = 2;
			collationSequence = "BINARY";
		}
		
		
		
	}
}
