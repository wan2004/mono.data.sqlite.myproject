using System;
using System.Runtime.InteropServices;
namespace Mono.Data.Sqlite
{
	public class SqliteStatementHandle : CriticalHandle
	{
		private SqliteConnectionHandle connection;
		public override bool IsInvalid
		{
			get
			{
				return this.handle == IntPtr.Zero;
			}
		}
		internal SqliteStatementHandle(IntPtr stmt,SqliteConnectionHandle connection) : this()
		{
			this.connection = connection;
			base.SetHandle(stmt);
		}
		internal SqliteStatementHandle() : base(IntPtr.Zero)
		{
		}
		protected override bool ReleaseHandle()
		{
			try
			{
				SQLiteBase.FinalizeStatement(this);
			}
			catch (SqliteException)
			{
			}
			return true;
		}
		public static implicit operator IntPtr(SqliteStatementHandle stmt)
		{
			return stmt.handle;
		}
//		public static implicit operator SqliteStatementHandle(IntPtr stmt)
//		{
//			return new SqliteStatementHandle(stmt);
//		}
		
		public override string ToString ()
		{
			return handle.ToString();
		}
	}
}
