using System;
using System.Runtime.InteropServices;
using System.Threading;
namespace Mono.Data.Sqlite
{
	internal class SqliteConnectionHandle : CriticalHandle
	{
		public bool ownHandle = false;
		
		public override bool IsInvalid
		{
			
			get
			{
				return this.handle == IntPtr.Zero;
			}
		}
		public SqliteConnectionHandle(IntPtr db) : this()
		{
			base.SetHandle(db);
			ownHandle = (db != IntPtr.Zero);
		}
		internal SqliteConnectionHandle() : base(IntPtr.Zero)
		{
			
		}
		protected override bool ReleaseHandle()
		{
			if(!ownHandle) return true;
			try
			{
				
//				IntPtr localHandle = Interlocked.Exchange(ref handle, IntPtr.Zero);
//				if(localHandle!=IntPtr.Zero)
				if(handle!=IntPtr.Zero)
					SQLiteBase.CloseConnection(this);
				
				return true;
			}
			catch 
			{
				
			}finally{
				SetHandleAsInvalid();
			}
			
			return false;
		}
		
		public override string ToString ()
		{
			return handle.ToString();
		}
		
		public static implicit operator IntPtr(SqliteConnectionHandle db)
		{
			return db.handle;
		}
//		public static implicit operator SqliteConnectionHandle(IntPtr db)
//		{
//			return new SqliteConnectionHandle(db);
//		}
	}
}
