using System;
using System.Data;
using System.Globalization;
namespace Mono.Data.Sqlite
{
	public sealed class SqliteStatement : IDisposable
	{
		internal SQLiteBase _sql;
		internal string _sqlStatement;
		public SqliteStatementHandle _sqlite_stmt;
		internal int _unnamedParameters;
		internal string[] _paramNames;
		internal SqliteParameter[] _paramValues;
		internal SqliteCommand _command;
		private string[] _types;
		internal string[] TypeDefinitions
		{
			get
			{
				return this._types;
			}
		}
		internal SqliteStatement(SQLiteBase sqlbase, SqliteStatementHandle stmt, string strCommand, SqliteStatement previous)
		{
			this._sql = sqlbase;
			this._sqlite_stmt = stmt;
			this._sqlStatement = strCommand;
			int num = 0;
			int num2 = this._sql.Bind_ParamCount(this);
			if (num2 > 0)
			{
				if (previous != null)
				{
					num = previous._unnamedParameters;
				}
				this._paramNames = new string[num2];
				this._paramValues = new SqliteParameter[num2];
				for (int i = 0; i < num2; i++)
				{
					string text = this._sql.Bind_ParamName(this, i + 1);
					if (string.IsNullOrEmpty(text))
					{
						text = string.Format(CultureInfo.InvariantCulture, ";{0}", new object[]
						{
							num
						});
						num++;
						this._unnamedParameters++;
					}
					this._paramNames[i] = text;
					this._paramValues[i] = null;
				}
			}
		}
		internal bool MapParameter(string s, SqliteParameter p)
		{
			if (this._paramNames == null)
			{
				return false;
			}
			int num = 0;
			if (s.Length > 0 && ":$@;".IndexOf(s[0]) == -1)
			{
				num = 1;
			}
			int num2 = this._paramNames.Length;
			for (int i = 0; i < num2; i++)
			{
				if (string.Compare(this._paramNames[i], num, s, 0, System.Math.Max(this._paramNames[i].Length - num, s.Length), true, CultureInfo.InvariantCulture) == 0)
				{
					this._paramValues[i] = p;
					return true;
				}
			}
			return false;
		}
		internal bool TryGetChanges(ref int changes)
	    {
	        if ((_sql != null) && _sql.IsOpen())
	        {
	            changes = _sql.Changes;
	            return true;
	        }
	
	        return false;
	    }

		
		public void Dispose()
		{
			if (this._sqlite_stmt != null)
			{
				this._sqlite_stmt.Dispose();
			}
			this._sqlite_stmt = null;
			this._paramNames = null;
			this._paramValues = null;
			this._sql = null;
			this._sqlStatement = null;
		}
		internal void BindParameters()
		{
			if (this._paramNames == null)
			{
				return;
			}
			int num = this._paramNames.Length;
			for (int i = 0; i < num; i++)
			{
				this.BindParameter(i + 1, this._paramValues[i]);
			}
		}
		private void BindParameter(int index, SqliteParameter param)
		{
			if (param == null)
			{
				throw new SqliteException(1, "Insufficient parameters supplied to the command");
			}
			object value = param.Value;
			DbType dbType = param.DbType;
			if (Convert.IsDBNull(value) || value == null)
			{
				this._sql.Bind_Null(this, index);
				return;
			}
			if (dbType == DbType.Object)
			{
				dbType = SqliteConvert.TypeToDbType(value.GetType());
			}
			switch (dbType)
			{
			case DbType.Binary:
				this._sql.Bind_Blob(this, index, (byte[])value);
				return;
			case DbType.Byte:
			case DbType.Boolean:
			case DbType.Int16:
			case DbType.Int32:
			case DbType.SByte:
			case DbType.UInt16:
			case DbType.UInt32:
				this._sql.Bind_Int32(this, index, Convert.ToInt32(value, CultureInfo.CurrentCulture));
				return;
			case DbType.Currency:
			case DbType.Double:
			case DbType.Single:
				this._sql.Bind_Double(this, index, Convert.ToDouble(value, CultureInfo.CurrentCulture));
				return;
			case DbType.Date:
			case DbType.DateTime:
			case DbType.Time:
				this._sql.Bind_DateTime(this, index, Convert.ToDateTime(value, CultureInfo.CurrentCulture));
				return;
			case DbType.Decimal:
				this._sql.Bind_Text(this, index, Convert.ToDecimal(value, CultureInfo.CurrentCulture).ToString(CultureInfo.InvariantCulture));
				return;
			case DbType.Guid:
				if (this._command.Connection._binaryGuid)
				{
					this._sql.Bind_Blob(this, index, ((Guid)value).ToByteArray());
				}
				else
				{
					this._sql.Bind_Text(this, index, value.ToString());
				}
				return;
			case DbType.Int64:
			case DbType.UInt64:
				this._sql.Bind_Int64(this, index, Convert.ToInt64(value, CultureInfo.CurrentCulture));
				return;
			}
			this._sql.Bind_Text(this, index, value.ToString());
		}
		internal void SetTypes(string typedefs)
		{
			int num = typedefs.IndexOf("TYPES", 0, StringComparison.OrdinalIgnoreCase);
			if (num == -1)
			{
				throw new ArgumentOutOfRangeException();
			}
			string[] array = typedefs.Substring(num + 6).Replace(" ", string.Empty).Replace(";", string.Empty).Replace("\"", string.Empty).Replace("[", string.Empty).Replace("]", string.Empty).Replace("`", string.Empty).Split(new char[]
			{
				',',
				'\r',
				'\n',
				'\t'
			});
			for (int i = 0; i < array.Length; i++)
			{
				if (string.IsNullOrEmpty(array[i]))
				{
					array[i] = null;
				}
			}
			this._types = array;
		}
		
		public override string ToString ()
		{
			return ""+_sqlite_stmt;
		}
	}
}
