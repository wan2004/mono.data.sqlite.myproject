using System;
namespace Mono.Data.Sqlite
{
	public enum TypeAffinity
	{
		Uninitialized,
		Int64,
		Double,
		Text,
		Blob,
		Null,
		DateTime = 10,
		None
	}
}
