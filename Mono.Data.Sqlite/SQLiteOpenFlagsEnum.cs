using System;
namespace Mono.Data.Sqlite
{
	[Flags]
	internal enum SQLiteOpenFlagsEnum
	{
		None = 0,
		ReadOnly = 1,
		ReadWrite = 2,
		Create = 4,
		SharedCache = 16777216,
		Default = 6
	}
}
