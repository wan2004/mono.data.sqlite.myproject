using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Reflection;
namespace Mono.Data.Sqlite
{
	[DefaultProperty("DataSource"), DefaultMember("Item")]
	public sealed class SqliteConnectionStringBuilder : DbConnectionStringBuilder
	{
		private Hashtable _properties;
		[Browsable(true), DefaultValue(3)]
		public int Version
		{
			get
			{
				object value;
				this.TryGetValue("version", out value);
				return Convert.ToInt32(value, CultureInfo.CurrentCulture);
			}
			set
			{
				if (value != 3)
				{
					throw new NotSupportedException();
				}
				this["version"] = value;
			}
		}
		[Browsable(true), DefaultValue(SynchronizationModes.Normal), DisplayName("Synchronous")]
		public SynchronizationModes SyncMode
		{
			get
			{
				object obj;
				this.TryGetValue("synchronous", out obj);
				if (obj is string)
				{
					return (SynchronizationModes)((int)TypeDescriptor.GetConverter(typeof(SynchronizationModes)).ConvertFrom(obj));
				}
				return (SynchronizationModes)((int)obj);
			}
			set
			{
				this["synchronous"] = value;
			}
		}
		[Browsable(true), DefaultValue(false)]
		public bool UseUTF16Encoding
		{
			get
			{
				object source;
				this.TryGetValue("useutf16encoding", out source);
				return SqliteConvert.ToBoolean(source);
			}
			set
			{
				this["useutf16encoding"] = value;
			}
		}
		[Browsable(true), DefaultValue(false)]
		public bool Pooling
		{
			get
			{
				object source;
				this.TryGetValue("pooling", out source);
				return SqliteConvert.ToBoolean(source);
			}
			set
			{
				this["pooling"] = value;
			}
		}
		[Browsable(true), DefaultValue(true)]
		public bool BinaryGUID
		{
			get
			{
				object source;
				this.TryGetValue("binaryguid", out source);
				return SqliteConvert.ToBoolean(source);
			}
			set
			{
				this["binaryguid"] = value;
			}
		}
		[Browsable(true), DefaultValue(""), DisplayName("Data Source")]
		public string DataSource
		{
			get
			{
				object obj;
				this.TryGetValue("data source", out obj);
				return obj.ToString();
			}
			set
			{
				this["data source"] = value;
			}
		}
		[Browsable(false)]
		public string Uri
		{
			get
			{
				object obj;
				this.TryGetValue("uri", out obj);
				return obj.ToString();
			}
			set
			{
				this["uri"] = value;
			}
		}
		[Browsable(true), DefaultValue(30), DisplayName("Default Timeout")]
		public int DefaultTimeout
		{
			get
			{
				object value;
				this.TryGetValue("default timeout", out value);
				return Convert.ToInt32(value, CultureInfo.CurrentCulture);
			}
			set
			{
				this["default timeout"] = value;
			}
		}
		[Browsable(true), DefaultValue(true)]
		public bool Enlist
		{
			get
			{
				object source;
				this.TryGetValue("enlist", out source);
				return SqliteConvert.ToBoolean(source);
			}
			set
			{
				this["enlist"] = value;
			}
		}
		[Browsable(true), DefaultValue(false)]
		public bool FailIfMissing
		{
			get
			{
				object source;
				this.TryGetValue("failifmissing", out source);
				return SqliteConvert.ToBoolean(source);
			}
			set
			{
				this["failifmissing"] = value;
			}
		}
		[Browsable(true), DefaultValue(false), DisplayName("Legacy Format")]
		public bool LegacyFormat
		{
			get
			{
				object source;
				this.TryGetValue("legacy format", out source);
				return SqliteConvert.ToBoolean(source);
			}
			set
			{
				this["legacy format"] = value;
			}
		}
		[Browsable(true), DefaultValue(false), DisplayName("Read Only")]
		public bool ReadOnly
		{
			get
			{
				object source;
				this.TryGetValue("read only", out source);
				return SqliteConvert.ToBoolean(source);
			}
			set
			{
				this["read only"] = value;
			}
		}
		[Browsable(true), DefaultValue(""), PasswordPropertyText(true)]
		public string Password
		{
			get
			{
				object obj;
				this.TryGetValue("password", out obj);
				return obj.ToString();
			}
			set
			{
				this["password"] = value;
			}
		}
		[Browsable(true), DefaultValue(1024), DisplayName("Page Size")]
		public int PageSize
		{
			get
			{
				object value;
				this.TryGetValue("page size", out value);
				return Convert.ToInt32(value, CultureInfo.CurrentCulture);
			}
			set
			{
				this["page size"] = value;
			}
		}
		[Browsable(true), DefaultValue(0), DisplayName("Max Page Count")]
		public int MaxPageCount
		{
			get
			{
				object value;
				this.TryGetValue("max page count", out value);
				return Convert.ToInt32(value, CultureInfo.CurrentCulture);
			}
			set
			{
				this["max page count"] = value;
			}
		}
		[Browsable(true), DefaultValue(2000), DisplayName("Cache Size")]
		public int CacheSize
		{
			get
			{
				object value;
				this.TryGetValue("cache size", out value);
				return Convert.ToInt32(value, CultureInfo.CurrentCulture);
			}
			set
			{
				this["cache size"] = value;
			}
		}
		[Browsable(true), DefaultValue(SQLiteDateFormats.ISO8601)]
		public SQLiteDateFormats DateTimeFormat
		{
			get
			{
				object obj;
				this.TryGetValue("datetimeformat", out obj);
				if (obj is string)
				{
					return (SQLiteDateFormats)((int)TypeDescriptor.GetConverter(typeof(SQLiteDateFormats)).ConvertFrom(obj));
				}
				return (SQLiteDateFormats)((int)obj);
			}
			set
			{
				this["datetimeformat"] = value;
			}
		}
		[Browsable(true), DefaultValue(SQLiteJournalModeEnum.Delete), DisplayName("Journal Mode")]
		public SQLiteJournalModeEnum JournalMode
		{
			get
			{
				object obj;
				this.TryGetValue("journal mode", out obj);
				if (obj is string)
				{
					return (SQLiteJournalModeEnum)((int)TypeDescriptor.GetConverter(typeof(SQLiteJournalModeEnum)).ConvertFrom(obj));
				}
				return (SQLiteJournalModeEnum)((int)obj);
			}
			set
			{
				this["journal mode"] = value;
			}
		}
		[Browsable(true), DefaultValue(IsolationLevel.Serializable), DisplayName("Default Isolation Level")]
		public IsolationLevel DefaultIsolationLevel
		{
			get
			{
				object obj;
				this.TryGetValue("default isolationlevel", out obj);
				if (obj is string)
				{
					return (IsolationLevel)((int)TypeDescriptor.GetConverter(typeof(IsolationLevel)).ConvertFrom(obj));
				}
				return (IsolationLevel)((int)obj);
			}
			set
			{
				this["default isolationlevel"] = value;
			}
		}
		public SqliteConnectionStringBuilder()
		{
			this.Initialize(null);
		}
		public SqliteConnectionStringBuilder(string connectionString)
		{
			this.Initialize(connectionString);
		}
		private void Initialize(string cnnString)
		{
			this._properties = new Hashtable(StringComparer.InvariantCultureIgnoreCase);
			try
			{
				base.GetProperties(this._properties);
			}
			catch (NotImplementedException)
			{
				this.FallbackGetProperties(this._properties);
			}
			if (!string.IsNullOrEmpty(cnnString))
			{
				base.ConnectionString = cnnString;
			}
		}
		public override bool TryGetValue(string keyword, out object value)
		{
			bool flag = base.TryGetValue(keyword, out value);
			if (!this._properties.ContainsKey(keyword))
			{
				return flag;
			}
			PropertyDescriptor propertyDescriptor = this._properties[keyword] as PropertyDescriptor;
			if (propertyDescriptor == null)
			{
				return flag;
			}
			if (flag)
			{
				if (propertyDescriptor.PropertyType == typeof(bool))
				{
					value = SqliteConvert.ToBoolean(value);
				}
				else
				{
					value = TypeDescriptor.GetConverter(propertyDescriptor.PropertyType).ConvertFrom(value);
				}
			}
			else
			{
				DefaultValueAttribute defaultValueAttribute = propertyDescriptor.Attributes[typeof(DefaultValueAttribute)] as DefaultValueAttribute;
				if (defaultValueAttribute != null)
				{
					value = defaultValueAttribute.Value;
					flag = true;
				}
			}
			return flag;
		}
		private void FallbackGetProperties(Hashtable propertyList)
		{
			foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(this, true))
			{
				if (propertyDescriptor.Name != "ConnectionString" && !propertyList.ContainsKey(propertyDescriptor.DisplayName))
				{
					propertyList.Add(propertyDescriptor.DisplayName, propertyDescriptor);
				}
			}
		}
	}
}
