using System;
namespace Mono.Data.Sqlite
{
	public enum CollationTypeEnum
	{
		Binary = 1,
		NoCase,
		Reverse,
		Custom = 0
	}
}
