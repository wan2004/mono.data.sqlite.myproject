using System;
namespace Mono.Data.Sqlite
{
	public enum SQLiteErrorCode
	{
		Ok,
		Error,
		Internal,
		Perm,
		Abort,
		Busy,
		Locked,
		NoMem,
		ReadOnly,
		Interrupt,
		IOErr,
		Corrupt,
		NotFound,
		Full,
		CantOpen,
		Protocol,
		Empty,
		Schema,
		TooBig,
		Constraint,
		Mismatch,
		Misuse,
		NOLFS,
		Auth,
		Format,
		Range,
		NotADatabase,
		Row = 100,
		Done
	}
}
