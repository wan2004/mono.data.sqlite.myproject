using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
namespace Mono.Data.Sqlite
{
	public abstract class SqliteFunction : IDisposable
	{
		private class AggregateData
		{
			internal int _count = 1;
			internal object _data;
		}
		internal SQLiteBase _base;
		private Dictionary<long, SqliteFunction.AggregateData> _contextDataList;
		private SQLiteCallback _InvokeFunc;
		private SQLiteCallback _StepFunc;
		private SQLiteFinalCallback _FinalFunc;
		private SQLiteCollation _CompareFunc;
		private SQLiteCollation _CompareFunc16;
		internal IntPtr _context;
		private static List<SqliteFunctionAttribute> _registeredFunctions;
		public SqliteConvert SqliteConvert
		{
			get
			{
				return this._base;
			}
		}
		protected SqliteFunction()
		{
			this._contextDataList = new Dictionary<long, SqliteFunction.AggregateData>();
		}
		[PermissionSet(SecurityAction.Assert, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"/>\n</PermissionSet>\n")]
		static SqliteFunction()
		{
			SqliteFunction._registeredFunctions = new List<SqliteFunctionAttribute>();
			try
			{
				Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
				int num = assemblies.Length;
				AssemblyName name = Assembly.GetCallingAssembly().GetName();
				int i = 0;
				while (i < num)
				{
					bool flag = false;
					Type[] types;
					try
					{
						AssemblyName[] referencedAssemblies = assemblies[i].GetReferencedAssemblies();
						int num2 = referencedAssemblies.Length;
						for (int j = 0; j < num2; j++)
						{
							if (referencedAssemblies[j].Name == name.Name)
							{
								flag = true;
								break;
							}
						}
						if (!flag)
						{
							goto IL_12C;
						}
						types = assemblies[i].GetTypes();
					}
					catch (ReflectionTypeLoadException ex)
					{
						types = ex.Types;
					}
					goto IL_A5;
					IL_12C:
					i++;
					continue;
					IL_A5:
					int num3 = types.Length;
					for (int k = 0; k < num3; k++)
					{
						if (types[k] != null)
						{
							object[] customAttributes = types[k].GetCustomAttributes(typeof(SqliteFunctionAttribute), false);
							int num4 = customAttributes.Length;
							for (int l = 0; l < num4; l++)
							{
								SqliteFunctionAttribute sqliteFunctionAttribute = customAttributes[l] as SqliteFunctionAttribute;
								if (sqliteFunctionAttribute != null)
								{
									sqliteFunctionAttribute._instanceType = types[k];
									SqliteFunction._registeredFunctions.Add(sqliteFunctionAttribute);
								}
							}
						}
					}
					goto IL_12C;
				}
			}
			catch
			{
			}
		}
		public virtual object Invoke(object[] args)
		{
			return null;
		}
		public virtual void Step(object[] args, int stepNumber, ref object contextData)
		{
		}
		public virtual object Final(object contextData)
		{
			return null;
		}
		public virtual int Compare(string param1, string param2)
		{
			return 0;
		}
		internal object[] ConvertParams(int nArgs, IntPtr argsptr)
		{
			object[] array = new object[nArgs];
			IntPtr[] array2 = new IntPtr[nArgs];
			Marshal.Copy(argsptr, array2, 0, nArgs);
			for (int i = 0; i < nArgs; i++)
			{
				switch (this._base.GetParamValueType(array2[i]))
				{
				case TypeAffinity.Int64:
					array[i] = this._base.GetParamValueInt64(array2[i]);
					break;
				case TypeAffinity.Double:
					array[i] = this._base.GetParamValueDouble(array2[i]);
					break;
				case TypeAffinity.Text:
					array[i] = this._base.GetParamValueText(array2[i]);
					break;
				case TypeAffinity.Blob:
				{
					int num = (int)this._base.GetParamValueBytes(array2[i], 0, null, 0, 0);
					byte[] array3 = new byte[num];
					this._base.GetParamValueBytes(array2[i], 0, array3, 0, num);
					array[i] = array3;
					break;
				}
				case TypeAffinity.Null:
					array[i] = DBNull.Value;
					break;
				case TypeAffinity.DateTime:
					array[i] = this._base.ToDateTime(this._base.GetParamValueText(array2[i]));
					break;
				}
			}
			return array;
		}
		private void SetReturnValue(IntPtr context, object returnValue)
		{
			if (returnValue == null || returnValue == DBNull.Value)
			{
				this._base.ReturnNull(context);
				return;
			}
			Type type = returnValue.GetType();
			if (type == typeof(DateTime))
			{
				this._base.ReturnText(context, this._base.ToString((DateTime)returnValue));
				return;
			}
			Exception ex = returnValue as Exception;
			if (ex != null)
			{
				this._base.ReturnError(context, ex.Message);
				return;
			}
			switch (SqliteConvert.TypeToAffinity(type))
			{
			case TypeAffinity.Int64:
				this._base.ReturnInt64(context, Convert.ToInt64(returnValue, CultureInfo.CurrentCulture));
				return;
			case TypeAffinity.Double:
				this._base.ReturnDouble(context, Convert.ToDouble(returnValue, CultureInfo.CurrentCulture));
				return;
			case TypeAffinity.Text:
				this._base.ReturnText(context, returnValue.ToString());
				return;
			case TypeAffinity.Blob:
				this._base.ReturnBlob(context, (byte[])returnValue);
				return;
			case TypeAffinity.Null:
				this._base.ReturnNull(context);
				return;
			default:
				return;
			}
		}
		internal void ScalarCallback(IntPtr context, int nArgs, IntPtr argsptr)
		{
			this._context = context;
			this.SetReturnValue(context, this.Invoke(this.ConvertParams(nArgs, argsptr)));
		}
		internal int CompareCallback(IntPtr ptr, int len1, IntPtr ptr1, int len2, IntPtr ptr2)
		{
			return this.Compare(SqliteConvert.UTF8ToString(ptr1, len1), SqliteConvert.UTF8ToString(ptr2, len2));
		}
		internal int CompareCallback16(IntPtr ptr, int len1, IntPtr ptr1, int len2, IntPtr ptr2)
		{
			return this.Compare(SQLite3_UTF16.UTF16ToString(ptr1, len1), SQLite3_UTF16.UTF16ToString(ptr2, len2));
		}
		internal void StepCallback(IntPtr context, int nArgs, IntPtr argsptr)
		{
			long key = (long)this._base.AggregateContext(context);
			SqliteFunction.AggregateData aggregateData;
			if (!this._contextDataList.TryGetValue(key, out aggregateData))
			{
				aggregateData = new SqliteFunction.AggregateData();
				this._contextDataList[key] = aggregateData;
			}
			try
			{
				this._context = context;
				this.Step(this.ConvertParams(nArgs, argsptr), aggregateData._count, ref aggregateData._data);
			}
			finally
			{
				aggregateData._count++;
			}
		}
		internal void FinalCallback(IntPtr context)
		{
			long key = (long)this._base.AggregateContext(context);
			object obj = null;
			if (this._contextDataList.ContainsKey(key))
			{
				obj = this._contextDataList[key]._data;
				this._contextDataList.Remove(key);
			}
			this._context = context;
			this.SetReturnValue(context, this.Final(obj));
			IDisposable disposable = obj as IDisposable;
			if (disposable != null)
			{
				disposable.Dispose();
			}
		}
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				foreach (KeyValuePair<long, SqliteFunction.AggregateData> current in this._contextDataList)
				{
					IDisposable disposable = current.Value._data as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}
				this._contextDataList.Clear();
				this._InvokeFunc = null;
				this._StepFunc = null;
				this._FinalFunc = null;
				this._CompareFunc = null;
				this._base = null;
				this._contextDataList = null;
			}
		}
		public void Dispose()
		{
			this.Dispose(true);
		}
		public static void RegisterFunction(Type typ)
		{
			object[] customAttributes = typ.GetCustomAttributes(typeof(SqliteFunctionAttribute), false);
			int num = customAttributes.Length;
			for (int i = 0; i < num; i++)
			{
				SqliteFunctionAttribute sqliteFunctionAttribute = customAttributes[i] as SqliteFunctionAttribute;
				if (sqliteFunctionAttribute != null)
				{
					sqliteFunctionAttribute._instanceType = typ;
					SqliteFunction._registeredFunctions.Add(sqliteFunctionAttribute);
				}
			}
		}
		internal static SqliteFunction[] BindFunctions(SQLiteBase sqlbase)
		{
			List<SqliteFunction> list = new List<SqliteFunction>();
			foreach (SqliteFunctionAttribute current in SqliteFunction._registeredFunctions)
			{
				SqliteFunction sqliteFunction = (SqliteFunction)Activator.CreateInstance(current._instanceType);
				sqliteFunction._base = sqlbase;
				sqliteFunction._InvokeFunc = ((current.FuncType != FunctionType.Scalar) ? null : new SQLiteCallback(sqliteFunction.ScalarCallback));
				sqliteFunction._StepFunc = ((current.FuncType != FunctionType.Aggregate) ? null : new SQLiteCallback(sqliteFunction.StepCallback));
				sqliteFunction._FinalFunc = ((current.FuncType != FunctionType.Aggregate) ? null : new SQLiteFinalCallback(sqliteFunction.FinalCallback));
				sqliteFunction._CompareFunc = ((current.FuncType != FunctionType.Collation) ? null : new SQLiteCollation(sqliteFunction.CompareCallback));
				sqliteFunction._CompareFunc16 = ((current.FuncType != FunctionType.Collation) ? null : new SQLiteCollation(sqliteFunction.CompareCallback16));
				if (current.FuncType != FunctionType.Collation)
				{
					sqlbase.CreateFunction(current.Name, current.Arguments, sqliteFunction is SqliteFunctionEx, sqliteFunction._InvokeFunc, sqliteFunction._StepFunc, sqliteFunction._FinalFunc);
				}
				else
				{
					sqlbase.CreateCollation(current.Name, sqliteFunction._CompareFunc, sqliteFunction._CompareFunc16);
				}
				list.Add(sqliteFunction);
			}
			SqliteFunction[] array = new SqliteFunction[list.Count];
			list.CopyTo(array, 0);
			return array;
		}
	}
}
