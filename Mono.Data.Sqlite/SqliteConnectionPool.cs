using System;
using System.Collections.Generic;
using System.Threading;
namespace Mono.Data.Sqlite
{
	internal static class SqliteConnectionPool
	{
		internal class Pool
		{
			internal readonly Queue<WeakReference> Queue = new Queue<WeakReference>();
			internal int PoolVersion;
			internal int MaxPoolSize;
			internal Pool(int version, int maxSize)
			{
				this.PoolVersion = version;
				this.MaxPoolSize = maxSize;
			}
		}
		private static SortedList<string, SqliteConnectionPool.Pool> _connections = new SortedList<string, SqliteConnectionPool.Pool>(StringComparer.OrdinalIgnoreCase);
		private static int _poolVersion = 1;
		internal static SqliteConnectionHandle Remove(string fileName, int maxPoolSize, out int version)
		{
			SortedList<string, SqliteConnectionPool.Pool> connections = SqliteConnectionPool._connections;
			Monitor.Enter(connections);
			SqliteConnectionHandle result;
			try
			{
				version = SqliteConnectionPool._poolVersion;
				SqliteConnectionPool.Pool pool;
				if (!SqliteConnectionPool._connections.TryGetValue(fileName, out pool))
				{
					pool = new SqliteConnectionPool.Pool(SqliteConnectionPool._poolVersion, maxPoolSize);
					SqliteConnectionPool._connections.Add(fileName, pool);
					result = null;
				}
				else
				{
					version = pool.PoolVersion;
					pool.MaxPoolSize = maxPoolSize;
					SqliteConnectionPool.ResizePool(pool, false);
					while (pool.Queue.Count > 0)
					{
						WeakReference weakReference = pool.Queue.Dequeue();
						SqliteConnectionHandle sqliteConnectionHandle = weakReference.Target as SqliteConnectionHandle;
						if (sqliteConnectionHandle != null)
						{
							result = sqliteConnectionHandle;
							return result;
						}
					}
					result = null;
				}
			}
			finally
			{
				Monitor.Exit(connections);
			}
			return result;
		}
		internal static void ClearAllPools()
		{
			SortedList<string, SqliteConnectionPool.Pool> connections = SqliteConnectionPool._connections;
			Monitor.Enter(connections);
			try
			{
				foreach (KeyValuePair<string, SqliteConnectionPool.Pool> current in SqliteConnectionPool._connections)
				{
					while (current.Value.Queue.Count > 0)
					{
						WeakReference weakReference = current.Value.Queue.Dequeue();
						SqliteConnectionHandle sqliteConnectionHandle = weakReference.Target as SqliteConnectionHandle;
						if (sqliteConnectionHandle != null)
						{
							sqliteConnectionHandle.Dispose();
						}
					}
					if (SqliteConnectionPool._poolVersion <= current.Value.PoolVersion)
					{
						SqliteConnectionPool._poolVersion = current.Value.PoolVersion + 1;
					}
				}
				SqliteConnectionPool._connections.Clear();
			}
			finally
			{
				Monitor.Exit(connections);
			}
		}
		internal static void ClearPool(string fileName)
		{
			SortedList<string, SqliteConnectionPool.Pool> connections = SqliteConnectionPool._connections;
			Monitor.Enter(connections);
			try
			{
				SqliteConnectionPool.Pool pool;
				if (SqliteConnectionPool._connections.TryGetValue(fileName, out pool))
				{
					pool.PoolVersion++;
					while (pool.Queue.Count > 0)
					{
						WeakReference weakReference = pool.Queue.Dequeue();
						SqliteConnectionHandle sqliteConnectionHandle = weakReference.Target as SqliteConnectionHandle;
						if (sqliteConnectionHandle != null)
						{
							sqliteConnectionHandle.Dispose();
						}
					}
				}
			}
			finally
			{
				Monitor.Exit(connections);
			}
		}
		internal static void Add(string fileName, SqliteConnectionHandle hdl, int version)
		{
			SortedList<string, SqliteConnectionPool.Pool> connections = SqliteConnectionPool._connections;
			Monitor.Enter(connections);
			try
			{
				SqliteConnectionPool.Pool pool;
				if (SqliteConnectionPool._connections.TryGetValue(fileName, out pool) && version == pool.PoolVersion)
				{
					SqliteConnectionPool.ResizePool(pool, true);
					pool.Queue.Enqueue(new WeakReference(hdl, false));
					GC.KeepAlive(hdl);
				}
				else
				{
					hdl.Close();
				}
			}
			finally
			{
				Monitor.Exit(connections);
			}
		}
		private static void ResizePool(SqliteConnectionPool.Pool queue, bool forAdding)
		{
			int num = queue.MaxPoolSize;
			if (forAdding && num > 0)
			{
				num--;
			}
			while (queue.Queue.Count > num)
			{
				WeakReference weakReference = queue.Queue.Dequeue();
				SqliteConnectionHandle sqliteConnectionHandle = weakReference.Target as SqliteConnectionHandle;
				if (sqliteConnectionHandle != null)
				{
					sqliteConnectionHandle.Dispose();
				}
			}
		}
	}
}
