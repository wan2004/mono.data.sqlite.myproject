using System;
namespace Mono.Data.Sqlite
{
	public enum CollationEncodingEnum
	{
		UTF8 = 1,
		UTF16LE,
		UTF16BE
	}
}
