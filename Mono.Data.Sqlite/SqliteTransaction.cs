using System;
using System.Data;
using System.Data.Common;
using System.Threading;
namespace Mono.Data.Sqlite
{
	public sealed class SqliteTransaction : DbTransaction
	{
		internal SqliteConnection _cnn;
		internal long _version;
		private IsolationLevel _level;
		public new SqliteConnection Connection
		{
			get
			{
				return this._cnn;
			}
		}
		protected override DbConnection DbConnection
		{
			get
			{
				return this.Connection;
			}
		}
		public override IsolationLevel IsolationLevel
		{
			get
			{
				return this._level;
			}
		}
		internal SqliteTransaction(SqliteConnection connection, bool deferredLock)
		{
			this._cnn = connection;
			this._version = this._cnn._version;
			this._level = ((!deferredLock) ? IsolationLevel.Serializable : IsolationLevel.ReadCommitted);
			if (this._cnn._transactionLevel++ == 0)
			{
				try
				{
					using (SqliteCommand sqliteCommand = this._cnn.CreateCommand())
					{
						if (!deferredLock)
						{
							sqliteCommand.CommandText = "BEGIN IMMEDIATE";
						}
						else
						{
							sqliteCommand.CommandText = "BEGIN";
						}
						sqliteCommand.ExecuteNonQuery();
					}
				}
				catch (SqliteException)
				{
					this._cnn._transactionLevel--;
					this._cnn = null;
					throw;
				}
			}
		}
		public override void Commit()
		{
			this.IsValid(true);
			if (this._cnn._transactionLevel - 1 == 0)
			{
				using (SqliteCommand sqliteCommand = this._cnn.CreateCommand())
				{
					sqliteCommand.CommandText = "COMMIT";
					sqliteCommand.ExecuteNonQuery();
				}
			}
			this._cnn._transactionLevel--;
			this._cnn = null;
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				Monitor.Enter(this);
				try
				{
					if (this.IsValid(false))
					{
						this.Rollback();
					}
					this._cnn = null;
				}
				finally
				{
					Monitor.Exit(this);
				}
			}
			base.Dispose(disposing);
		}
		public override void Rollback()
		{
			this.IsValid(true);
			SqliteTransaction.IssueRollback(this._cnn);
			this._cnn._transactionLevel = 0;
			this._cnn = null;
		}
		internal static void IssueRollback(SqliteConnection cnn)
		{
			using (SqliteCommand sqliteCommand = cnn.CreateCommand())
			{
				sqliteCommand.CommandText = "ROLLBACK";
				sqliteCommand.ExecuteNonQuery();
			}
		}
		internal bool IsValid(bool throwError)
		{
			if (this._cnn == null)
			{
				if (throwError)
				{
					throw new ArgumentNullException("No connection associated with this transaction");
				}
				return false;
			}
			else
			{
				if (this._cnn._transactionLevel == 0)
				{
					if (throwError)
					{
						throw new SqliteException(21, "No transaction is active on this connection");
					}
					return false;
				}
				else
				{
					if (this._cnn._version != this._version)
					{
						if (throwError)
						{
							throw new SqliteException(21, "The connection was closed and re-opened, changes were rolled back");
						}
						return false;
					}
					else
					{
						if (this._cnn.State == ConnectionState.Open)
						{
							return true;
						}
						if (throwError)
						{
							throw new SqliteException(21, "Connection was closed");
						}
						return false;
					}
				}
			}
		}
	}
}
