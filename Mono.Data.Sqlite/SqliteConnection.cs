using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Transactions;
namespace Mono.Data.Sqlite
{
	public sealed class SqliteConnection : DbConnection, ICloneable
	{
		private const string _dataDirectory = "|DataDirectory|";
		private const string _masterdb = "sqlite_master";
		private const string _tempmasterdb = "sqlite_temp_master";
		private ConnectionState _connectionState;
		private string _connectionString;
		internal int _transactionLevel;
		private System.Data.IsolationLevel _defaultIsolation;
		internal SQLiteEnlistment _enlistment = null;
		internal SQLiteBase _sql = null;
		private string _dataSource;
		private byte[] _password;
		private int _defaultTimeout = 30;
		internal bool _binaryGuid;
		internal long _version;
		private SQLiteUpdateCallback _updateCallback;
		private SQLiteCommitCallback _commitCallback;
		private SQLiteRollbackCallback _rollbackCallback;
		
		private event SQLiteUpdateEventHandler _updateHandler;
		
		private event SQLiteCommitHandler _commitHandler;
		
		private event EventHandler _rollbackHandler;
		
		public override event StateChangeEventHandler StateChange;
		/// <summary>
	    /// This event is raised whenever SQLite makes an update/delete/insert into the database on
	    /// this connection.  It only applies to the given connection.
	    /// </summary>
	    public event SQLiteUpdateEventHandler Update
	    {
	      add
	      {
	
	        if (_updateHandler == null)
	        {
	          _updateCallback = new SQLiteUpdateCallback(UpdateCallback);
	          if (_sql != null) _sql.SetUpdateHook(_updateCallback);
	        }
	        _updateHandler += value;
	      }
	      remove
	      {
	
	        _updateHandler -= value;
	        if (_updateHandler == null)
	        {
	          if (_sql != null) _sql.SetUpdateHook(null);
	          _updateCallback = null;
	        }
	      }
	    }
		
		
		/// <summary>
	    /// This event is raised whenever SQLite is committing a transaction.
	    /// Return non-zero to trigger a rollback.
	    /// </summary>
	    public event SQLiteCommitHandler Commit
	    {
	      add
	      {
	        if (_commitHandler == null)
	        {
	          _commitCallback = new SQLiteCommitCallback(CommitCallback);
	          if (_sql != null) _sql.SetCommitHook(_commitCallback);
	        }
	        _commitHandler += value;
	      }
	      remove
	      {
	        _commitHandler -= value;
	        if (_commitHandler == null)
	        {
	          if (_sql != null) _sql.SetCommitHook(null);
	          _commitCallback = null;
	        }
	      }
	    }
		
		 /// <summary>
	    /// This event is raised whenever SQLite is rolling back a transaction.
	    /// </summary>
	    public event EventHandler RollBack
	    {
	      add
	      {
	
	        if (_rollbackHandler == null)
	        {
	          _rollbackCallback = new SQLiteRollbackCallback(RollbackCallback);
	          if (_sql != null) _sql.SetRollbackHook(_rollbackCallback);
	        }
	        _rollbackHandler += value;
	      }
	      remove
	      {
	
	        _rollbackHandler -= value;
	        if (_rollbackHandler == null)
	        {
	          if (_sql != null) _sql.SetRollbackHook(null);
	          _rollbackCallback = null;
	        }
	      }
	    }
		[DefaultValue(""), Editor("SQLite.Designer.SqliteConnectionStringEditor, SQLite.Designer, Version=1.0.36.0, Culture=neutral, PublicKeyToken=db937bc2d44ff139", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), RefreshProperties(RefreshProperties.All)]
		public override string ConnectionString
		{
			get
			{
				return this._connectionString;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				if (this._connectionState != ConnectionState.Closed)
				{
					throw new InvalidOperationException();
				}
				this._connectionString = value;
			}
		}
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public override string DataSource
		{
			get
			{
				return this._dataSource;
			}
		}
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public override string Database
		{
			get
			{
				return "main";
			}
		}
		public int DefaultTimeout
		{
			get
			{
				return this._defaultTimeout;
			}
			set
			{
				this._defaultTimeout = value;
			}
		}
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public override string ServerVersion
		{
			get
			{
				if (this._connectionState != ConnectionState.Open)
				{
					throw new InvalidOperationException();
				}
				return this._sql.Version;
			}
		}
		public static string SQLiteVersion
		{
			get
			{
				return SQLite3.SQLiteVersion;
			}
		}
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public override ConnectionState State
		{
			get
			{
				return this._connectionState;
			}
		}
		protected override DbProviderFactory DbProviderFactory
		{
			get
			{
				return SqliteFactory.Instance;
			}
		}
		public SqliteConnection() : this(string.Empty)
		{
		}
		public SqliteConnection(string connectionString)
		{
			this._sql = null;
			this._connectionState = ConnectionState.Closed;
			this._connectionString = string.Empty;
			this._transactionLevel = 0;
			this._version = 0L;
			if (connectionString != null)
			{
				this.ConnectionString = connectionString;
			}
		}
		public SqliteConnection(SqliteConnection connection) : this(connection.ConnectionString)
		{
			if (connection.State == ConnectionState.Open)
			{
				this.Open();
				using (DataTable schema = connection.GetSchema("Catalogs"))
				{
					foreach (DataRow dataRow in schema.Rows)
					{
						string strA = dataRow[0].ToString();
						if (string.Compare(strA, "main", true, CultureInfo.InvariantCulture) != 0 && string.Compare(strA, "temp", true, CultureInfo.InvariantCulture) != 0)
						{
							using (SqliteCommand sqliteCommand = this.CreateCommand())
							{
								sqliteCommand.CommandText = string.Format(CultureInfo.InvariantCulture, "ATTACH DATABASE '{0}' AS [{1}]", new object[]
								{
									dataRow[1],
									dataRow[0]
								});
								sqliteCommand.ExecuteNonQuery();
							}
						}
					}
				}
			}
		}
		public object Clone()
		{
			return new SqliteConnection(this);
		}
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (disposing)
			{
				this.Close();
			}
		}
		public static void CreateFile(string databaseFileName)
		{
			FileStream fileStream = File.Create(databaseFileName);
			fileStream.Close();
		}
		internal void OnStateChange(ConnectionState newState)
		{
			ConnectionState connectionState = this._connectionState;
			this._connectionState = newState;
			if (this.StateChange != null && connectionState != newState)
			{
				StateChangeEventArgs e = new StateChangeEventArgs(connectionState, newState);
				this.StateChange(this, e);
			}
		}
		[Obsolete("Use one of the standard BeginTransaction methods, this one will be removed soon")]
		public SqliteTransaction BeginTransaction(System.Data.IsolationLevel isolationLevel, bool deferredLock)
		{
			return (SqliteTransaction)this.BeginDbTransaction(deferredLock ? System.Data.IsolationLevel.ReadCommitted : System.Data.IsolationLevel.Serializable);
		}
		[Obsolete("Use one of the standard BeginTransaction methods, this one will be removed soon")]
		public SqliteTransaction BeginTransaction(bool deferredLock)
		{
			return (SqliteTransaction)this.BeginDbTransaction(deferredLock ? System.Data.IsolationLevel.ReadCommitted : System.Data.IsolationLevel.Serializable);
		}
		public new SqliteTransaction BeginTransaction(System.Data.IsolationLevel isolationLevel)
		{
			return (SqliteTransaction)this.BeginDbTransaction(isolationLevel);
		}
		public new SqliteTransaction BeginTransaction()
		{
			return (SqliteTransaction)this.BeginDbTransaction(this._defaultIsolation);
		}
		protected override DbTransaction BeginDbTransaction(System.Data.IsolationLevel isolationLevel)
		{
			if (this._connectionState != ConnectionState.Open)
			{
				throw new InvalidOperationException();
			}
			if (isolationLevel == System.Data.IsolationLevel.Unspecified)
			{
				isolationLevel = this._defaultIsolation;
			}
			if (isolationLevel != System.Data.IsolationLevel.Serializable && isolationLevel != System.Data.IsolationLevel.ReadCommitted)
			{
				throw new ArgumentException("isolationLevel");
			}
			return new SqliteTransaction(this, isolationLevel != System.Data.IsolationLevel.Serializable);
		}
		public override void ChangeDatabase(string databaseName)
		{
			throw new NotImplementedException();
		}
		public override void Close()
		{
		
			if (this._sql != null)
			{
#if DEBUG			
				System.Console.Error.WriteLine("###SqliteConnection Close()");
#endif					
				if (this._enlistment != null)
				{
					SqliteConnection sqliteConnection = new SqliteConnection();
					sqliteConnection._sql = this._sql;
					sqliteConnection._transactionLevel = this._transactionLevel;
					sqliteConnection._enlistment = this._enlistment;
					sqliteConnection._connectionState = this._connectionState;
					sqliteConnection._version = this._version;
					sqliteConnection._enlistment._transaction._cnn = sqliteConnection;
					sqliteConnection._enlistment._disposeConnection = true;
					this._sql = null;
					this._enlistment = null;
				}
				if (this._sql != null)
				{
					this._sql.Close();
				}
				this._sql = null;
				this._transactionLevel = 0;
			}
			this.OnStateChange(ConnectionState.Closed);
		}
		public static void ClearPool(SqliteConnection connection)
		{
			if (connection._sql == null)
			{
				return;
			}
			connection._sql.ClearPool();
		}
		public static void ClearAllPools()
		{
			SqliteConnectionPool.ClearAllPools();
		}
		public new SqliteCommand CreateCommand()
		{
			return new SqliteCommand(this);
		}
		protected override DbCommand CreateDbCommand()
		{
			return this.CreateCommand();
		}
		internal static void MapMonoKeyword(string[] arPiece, SortedList<string, string> ls)
		{
			string text = arPiece[0].ToLower(CultureInfo.InvariantCulture);
			string key;
			string value;
			if (text != null)
			{
				
				
				if (map1 == null)
				{
					map1 = new Dictionary<string, int>(1)
					{

						{
							"uri",
							0
						}
					};
				}
				int num;
				if (map1.TryGetValue(text, out num))
				{
					if (num == 0)
					{
						key = "Data Source";
						value = SqliteConnection.MapMonoUriPath(arPiece[1]);
						goto IL_76;
					}
				}
			}
			key = arPiece[0];
			value = arPiece[1];
			IL_76:
			ls.Add(key, value);
		}
		internal static string MapMonoUriPath(string path)
		{
			if (path.StartsWith("file://"))
			{
				return path.Substring(7);
			}
			if (path.StartsWith("file:"))
			{
				return path.Substring(5);
			}
			if (path.StartsWith("/"))
			{
				return path;
			}
			throw new InvalidOperationException("Invalid connection string: invalid URI");
		}
		internal static string MapUriPath(string path)
		{
			if (path.StartsWith("file://"))
			{
				return path.Substring(7);
			}
			if (path.StartsWith("file:"))
			{
				return path.Substring(5);
			}
			if (path.StartsWith("/"))
			{
				return path;
			}
			throw new InvalidOperationException("Invalid connection string: invalid URI");
		}
		internal static SortedList<string, string> ParseConnectionString(string connectionString)
		{
			string source = connectionString.Replace(',', ';');
			SortedList<string, string> sortedList = new SortedList<string, string>(StringComparer.OrdinalIgnoreCase);
			string[] array = SqliteConvert.Split(source, ';');
			int num = array.Length;
			for (int i = 0; i < num; i++)
			{
				string[] array2 = SqliteConvert.Split(array[i], '=');
				if (array2.Length != 2)
				{
					throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "Invalid ConnectionString format for parameter \"{0}\"", new object[]
					{
						(array2.Length <= 0) ? "null" : array2[0]
					}));
				}
				SqliteConnection.MapMonoKeyword(array2, sortedList);
			}
			return sortedList;
		}
		public override void EnlistTransaction(Transaction transaction)
		{
			if (this._transactionLevel > 0 && transaction != null)
			{
				throw new ArgumentException("Unable to enlist in transaction, a local transaction already exists");
			}
			if (this._enlistment != null && transaction != this._enlistment._scope)
			{
				throw new ArgumentException("Already enlisted in a transaction");
			}
			this._enlistment = new SQLiteEnlistment(this, transaction);
		}
		internal static string FindKey(SortedList<string, string> items, string key, string defValue)
		{
			string result;
			if (items.TryGetValue(key, out result))
			{
				return result;
			}
			return defValue;
		}
		public override void Open()
		{
			if (this._connectionState != ConnectionState.Closed)
			{
				throw new InvalidOperationException();
			}
			this.Close();
			SortedList<string, string> items = SqliteConnection.ParseConnectionString(this._connectionString);
			if (Convert.ToInt32(SqliteConnection.FindKey(items, "Version", "3"), CultureInfo.InvariantCulture) != 3)
			{
				throw new NotSupportedException("Only SQLite Version 3 is supported at this time");
			}
			string text = SqliteConnection.FindKey(items, "Data Source", string.Empty);
			if (string.IsNullOrEmpty(text))
			{
				text = SqliteConnection.FindKey(items, "Uri", string.Empty);
				if (string.IsNullOrEmpty(text))
				{
					throw new ArgumentException("Data Source cannot be empty.  Use :memory: to open an in-memory database");
				}
				text = SqliteConnection.MapUriPath(text);
			}
			if (string.Compare(text, ":MEMORY:", true, CultureInfo.InvariantCulture) == 0)
			{
				text = ":memory:";
			}
			else
			{
				text = this.ExpandFileName(text);
			}
			try
			{
				bool usePool = SqliteConvert.ToBoolean(SqliteConnection.FindKey(items, "Pooling", bool.FalseString));
				bool use_utf16 = SqliteConvert.ToBoolean(SqliteConnection.FindKey(items, "UseUTF16Encoding", bool.FalseString));
				int maxPoolSize = Convert.ToInt32(SqliteConnection.FindKey(items, "Max Pool Size", "100"));
				this._defaultTimeout = Convert.ToInt32(SqliteConnection.FindKey(items, "Default Timeout", "30"), CultureInfo.CurrentCulture);
				this._defaultIsolation = (System.Data.IsolationLevel)((int)Enum.Parse(typeof(System.Data.IsolationLevel), SqliteConnection.FindKey(items, "Default IsolationLevel", "Serializable"), true));
				if (this._defaultIsolation != System.Data.IsolationLevel.Serializable && this._defaultIsolation != System.Data.IsolationLevel.ReadCommitted)
				{
					throw new NotSupportedException("Invalid Default IsolationLevel specified");
				}
				SQLiteDateFormats fmt = (SQLiteDateFormats)((int)Enum.Parse(typeof(SQLiteDateFormats), SqliteConnection.FindKey(items, "DateTimeFormat", "ISO8601"), true));
				if (use_utf16)
				{
					this._sql = new SQLite3_UTF16(fmt);
				}
				else
				{
					this._sql = new SQLite3(fmt);
				}
				SQLiteOpenFlagsEnum sQLiteOpenFlagsEnum = SQLiteOpenFlagsEnum.None;
				if (!SqliteConvert.ToBoolean(SqliteConnection.FindKey(items, "FailIfMissing", bool.FalseString)))
				{
					sQLiteOpenFlagsEnum |= SQLiteOpenFlagsEnum.Create;
				}
				if (SqliteConvert.ToBoolean(SqliteConnection.FindKey(items, "Read Only", bool.FalseString)))
				{
					sQLiteOpenFlagsEnum |= SQLiteOpenFlagsEnum.ReadOnly;
				}
				else
				{
					sQLiteOpenFlagsEnum |= SQLiteOpenFlagsEnum.ReadWrite;
				}
				this._sql.Open(text, sQLiteOpenFlagsEnum, maxPoolSize, usePool);
				this._binaryGuid = SqliteConvert.ToBoolean(SqliteConnection.FindKey(items, "BinaryGUID", bool.TrueString));
				string text2 = SqliteConnection.FindKey(items, "Password", null);
				if (!string.IsNullOrEmpty(text2))
				{
					this._sql.SetPassword(Encoding.UTF8.GetBytes(text2));
				}
				else
				{
					if (this._password != null)
					{
						this._sql.SetPassword(this._password);
					}
				}
				this._password = null;
				this._dataSource = Path.GetFileNameWithoutExtension(text);
				this.OnStateChange(ConnectionState.Open);
				this._version += 1L;
				using (SqliteCommand sqliteCommand = this.CreateCommand())
				{
					string text3;
					if (text != ":memory:")
					{
						text3 = SqliteConnection.FindKey(items, "Page Size", "1024");
						if (Convert.ToInt32(text3, CultureInfo.InvariantCulture) != 1024)
						{
							sqliteCommand.CommandText = string.Format(CultureInfo.InvariantCulture, "PRAGMA page_size={0}", new object[]
							{
								text3
							});
							sqliteCommand.ExecuteNonQuery();
						}
					}
					text3 = SqliteConnection.FindKey(items, "Max Page Count", "0");
					if (Convert.ToInt32(text3, CultureInfo.InvariantCulture) != 0)
					{
						sqliteCommand.CommandText = string.Format(CultureInfo.InvariantCulture, "PRAGMA max_page_count={0}", new object[]
						{
							text3
						});
						sqliteCommand.ExecuteNonQuery();
					}
					text3 = SqliteConnection.FindKey(items, "Legacy Format", bool.FalseString);
					sqliteCommand.CommandText = string.Format(CultureInfo.InvariantCulture, "PRAGMA legacy_file_format={0}", new object[]
					{
						(!SqliteConvert.ToBoolean(text3)) ? "OFF" : "ON"
					});
					sqliteCommand.ExecuteNonQuery();
					text3 = SqliteConnection.FindKey(items, "Synchronous", "Normal");
					if (string.Compare(text3, "Full", StringComparison.OrdinalIgnoreCase) != 0)
					{
						sqliteCommand.CommandText = string.Format(CultureInfo.InvariantCulture, "PRAGMA synchronous={0}", new object[]
						{
							text3
						});
						sqliteCommand.ExecuteNonQuery();
					}
					text3 = SqliteConnection.FindKey(items, "Cache Size", "2000");
					if (Convert.ToInt32(text3, CultureInfo.InvariantCulture) != 2000)
					{
						sqliteCommand.CommandText = string.Format(CultureInfo.InvariantCulture, "PRAGMA cache_size={0}", new object[]
						{
							text3
						});
						sqliteCommand.ExecuteNonQuery();
					}
					text3 = SqliteConnection.FindKey(items, "Journal Mode", "Delete");
					if (string.Compare(text3, "Default", StringComparison.OrdinalIgnoreCase) != 0)
					{
						sqliteCommand.CommandText = string.Format(CultureInfo.InvariantCulture, "PRAGMA journal_mode={0}", new object[]
						{
							text3
						});
						sqliteCommand.ExecuteNonQuery();
					}
				}
				if (this._commitHandler != null)
				{
					this._sql.SetCommitHook(this._commitCallback);
				}
				if (this._updateHandler != null)
				{
					this._sql.SetUpdateHook(this._updateCallback);
				}
				if (this._rollbackHandler != null)
				{
					this._sql.SetRollbackHook(this._rollbackCallback);
				}
				if (Transaction.Current != null && SqliteConvert.ToBoolean(SqliteConnection.FindKey(items, "Enlist", bool.TrueString)))
				{
					this.EnlistTransaction(Transaction.Current);
				}
			}
			catch (SqliteException ex)
			{
#if DEBUG
				System.Console.Error.WriteLine(" SqliteConnection Open fail = "	+ ex.Message);
#endif				
				this.Close();
				throw ex;
			}
		}
		public void ChangePassword(string newPassword)
		{
			this.ChangePassword((!string.IsNullOrEmpty(newPassword)) ? Encoding.UTF8.GetBytes(newPassword) : null);
		}
		public void ChangePassword(byte[] newPassword)
		{
			if (this._connectionState != ConnectionState.Open)
			{
				throw new InvalidOperationException("Database must be opened before changing the password.");
			}
			this._sql.ChangePassword(newPassword);
		}
		public void SetPassword(string databasePassword)
		{
			this.SetPassword((!string.IsNullOrEmpty(databasePassword)) ? Encoding.UTF8.GetBytes(databasePassword) : null);
		}
		public void SetPassword(byte[] databasePassword)
		{
			if (this._connectionState != ConnectionState.Closed)
			{
				throw new InvalidOperationException("Password can only be set before the database is opened.");
			}
			if (databasePassword != null && databasePassword.Length == 0)
			{
				databasePassword = null;
			}
			this._password = databasePassword;
		}
		private string ExpandFileName(string sourceFile)
		{
			if (string.IsNullOrEmpty(sourceFile))
			{
				return sourceFile;
			}
			if (sourceFile.StartsWith("|DataDirectory|", StringComparison.OrdinalIgnoreCase))
			{
				string text = AppDomain.CurrentDomain.GetData("DataDirectory") as string;
				if (string.IsNullOrEmpty(text))
				{
					text = AppDomain.CurrentDomain.BaseDirectory;
				}
				if (sourceFile.Length > "|DataDirectory|".Length && (sourceFile["|DataDirectory|".Length] == Path.DirectorySeparatorChar || sourceFile["|DataDirectory|".Length] == Path.AltDirectorySeparatorChar))
				{
					sourceFile = sourceFile.Remove("|DataDirectory|".Length, 1);
				}
				sourceFile = Path.Combine(text, sourceFile.Substring("|DataDirectory|".Length));
			}
			sourceFile = Path.GetFullPath(sourceFile);
			return sourceFile;
		}
		public override DataTable GetSchema()
		{
			return this.GetSchema("MetaDataCollections", null);
		}
		public override DataTable GetSchema(string collectionName)
		{
			return this.GetSchema(collectionName, new string[0]);
		}
		public override DataTable GetSchema(string collectionName, string[] restrictionValues)
		{
			if (this._connectionState != ConnectionState.Open)
			{
				throw new InvalidOperationException();
			}
			string[] array = new string[5];
			if (restrictionValues == null)
			{
				restrictionValues = new string[0];
			}
			restrictionValues.CopyTo(array, 0);
			string text = collectionName.ToUpper(CultureInfo.InvariantCulture);
			switch (text)
			{
			case "METADATACOLLECTIONS":
				return SqliteConnection.Schema_MetaDataCollections();
			case "DATASOURCEINFORMATION":
				return this.Schema_DataSourceInformation();
			case "DATATYPES":
				return this.Schema_DataTypes();
			case "COLUMNS":
			case "TABLECOLUMNS":
				return this.Schema_Columns(array[0], array[2], array[3]);
			case "INDEXES":
				return this.Schema_Indexes(array[0], array[2], array[3]);
			case "TRIGGERS":
				return this.Schema_Triggers(array[0], array[2], array[3]);
			case "INDEXCOLUMNS":
				return this.Schema_IndexColumns(array[0], array[2], array[3], array[4]);
			case "TABLES":
				return this.Schema_Tables(array[0], array[2], array[3]);
			case "VIEWS":
				return this.Schema_Views(array[0], array[2]);
			case "VIEWCOLUMNS":
				return this.Schema_ViewColumns(array[0], array[2], array[3]);
			case "FOREIGNKEYS":
				return this.Schema_ForeignKeys(array[0], array[2], array[3]);
			case "CATALOGS":
				return this.Schema_Catalogs(array[0]);
			case "RESERVEDWORDS":
				return SqliteConnection.Schema_ReservedWords();
			}
			throw new NotSupportedException();
		}
		private static DataTable Schema_ReservedWords()
		{
			DataTable dataTable = new DataTable("MetaDataCollections");
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("ReservedWord", typeof(string));
			dataTable.Columns.Add("MaximumVersion", typeof(string));
			dataTable.Columns.Add("MinimumVersion", typeof(string));
			dataTable.BeginLoadData();
			string[] array = SR.Keywords.Split(new char[]
			{
				','
			});
			for (int i = 0; i < array.Length; i++)
			{
				string value = array[i];
				DataRow dataRow = dataTable.NewRow();
				dataRow[0] = value;
				dataTable.Rows.Add(dataRow);
			}
			dataTable.AcceptChanges();
			dataTable.EndLoadData();
			return dataTable;
		}
		private static DataTable Schema_MetaDataCollections()
		{
			DataTable dataTable = new DataTable("MetaDataCollections");
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("CollectionName", typeof(string));
			dataTable.Columns.Add("NumberOfRestrictions", typeof(int));
			dataTable.Columns.Add("NumberOfIdentifierParts", typeof(int));
			dataTable.BeginLoadData();
			StringReader stringReader = new StringReader(SR.MetaDataCollections);
			dataTable.ReadXml(stringReader);
			stringReader.Close();
			dataTable.AcceptChanges();
			dataTable.EndLoadData();
			return dataTable;
		}
		private DataTable Schema_DataSourceInformation()
		{
			DataTable dataTable = new DataTable("DataSourceInformation");
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add(DbMetaDataColumnNames.CompositeIdentifierSeparatorPattern, typeof(string));
			dataTable.Columns.Add(DbMetaDataColumnNames.DataSourceProductName, typeof(string));
			dataTable.Columns.Add(DbMetaDataColumnNames.DataSourceProductVersion, typeof(string));
			dataTable.Columns.Add(DbMetaDataColumnNames.DataSourceProductVersionNormalized, typeof(string));
			dataTable.Columns.Add(DbMetaDataColumnNames.GroupByBehavior, typeof(int));
			dataTable.Columns.Add(DbMetaDataColumnNames.IdentifierPattern, typeof(string));
			dataTable.Columns.Add(DbMetaDataColumnNames.IdentifierCase, typeof(int));
			dataTable.Columns.Add(DbMetaDataColumnNames.OrderByColumnsInSelect, typeof(bool));
			dataTable.Columns.Add(DbMetaDataColumnNames.ParameterMarkerFormat, typeof(string));
			dataTable.Columns.Add(DbMetaDataColumnNames.ParameterMarkerPattern, typeof(string));
			dataTable.Columns.Add(DbMetaDataColumnNames.ParameterNameMaxLength, typeof(int));
			dataTable.Columns.Add(DbMetaDataColumnNames.ParameterNamePattern, typeof(string));
			dataTable.Columns.Add(DbMetaDataColumnNames.QuotedIdentifierPattern, typeof(string));
			dataTable.Columns.Add(DbMetaDataColumnNames.QuotedIdentifierCase, typeof(int));
			dataTable.Columns.Add(DbMetaDataColumnNames.StatementSeparatorPattern, typeof(string));
			dataTable.Columns.Add(DbMetaDataColumnNames.StringLiteralPattern, typeof(string));
			dataTable.Columns.Add(DbMetaDataColumnNames.SupportedJoinOperators, typeof(int));
			dataTable.BeginLoadData();
			DataRow dataRow = dataTable.NewRow();
			dataRow.ItemArray = new object[]
			{
				null,
				"SQLite",
				this._sql.Version,
				this._sql.Version,
				3,
				"(^\\[\\p{Lo}\\p{Lu}\\p{Ll}_@#][\\p{Lo}\\p{Lu}\\p{Ll}\\p{Nd}@$#_]*$)|(^\\[[^\\]\\0]|\\]\\]+\\]$)|(^\\\"[^\\\"\\0]|\\\"\\\"+\\\"$)",
				1,
				false,
				"{0}",
				"@[\\p{Lo}\\p{Lu}\\p{Ll}\\p{Lm}_@#][\\p{Lo}\\p{Lu}\\p{Ll}\\p{Lm}\\p{Nd}\\uff3f_@#\\$]*(?=\\s+|$)",
				255,
				"^[\\p{Lo}\\p{Lu}\\p{Ll}\\p{Lm}_@#][\\p{Lo}\\p{Lu}\\p{Ll}\\p{Lm}\\p{Nd}\\uff3f_@#\\$]*(?=\\s+|$)",
				"(([^\\[]|\\]\\])*)",
				1,
				";",
				"'(([^']|'')*)'",
				15
			};
			dataTable.Rows.Add(dataRow);
			dataTable.AcceptChanges();
			dataTable.EndLoadData();
			return dataTable;
		}
		private DataTable Schema_Columns(string strCatalog, string strTable, string strColumn)
		{
			DataTable dataTable = new DataTable("Columns");
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("TABLE_CATALOG", typeof(string));
			dataTable.Columns.Add("TABLE_SCHEMA", typeof(string));
			dataTable.Columns.Add("TABLE_NAME", typeof(string));
			dataTable.Columns.Add("COLUMN_NAME", typeof(string));
			dataTable.Columns.Add("COLUMN_GUID", typeof(Guid));
			dataTable.Columns.Add("COLUMN_PROPID", typeof(long));
			dataTable.Columns.Add("ORDINAL_POSITION", typeof(int));
			dataTable.Columns.Add("COLUMN_HASDEFAULT", typeof(bool));
			dataTable.Columns.Add("COLUMN_DEFAULT", typeof(string));
			dataTable.Columns.Add("COLUMN_FLAGS", typeof(long));
			dataTable.Columns.Add("IS_NULLABLE", typeof(bool));
			dataTable.Columns.Add("DATA_TYPE", typeof(string));
			dataTable.Columns.Add("TYPE_GUID", typeof(Guid));
			dataTable.Columns.Add("CHARACTER_MAXIMUM_LENGTH", typeof(int));
			dataTable.Columns.Add("CHARACTER_OCTET_LENGTH", typeof(int));
			dataTable.Columns.Add("NUMERIC_PRECISION", typeof(int));
			dataTable.Columns.Add("NUMERIC_SCALE", typeof(int));
			dataTable.Columns.Add("DATETIME_PRECISION", typeof(long));
			dataTable.Columns.Add("CHARACTER_SET_CATALOG", typeof(string));
			dataTable.Columns.Add("CHARACTER_SET_SCHEMA", typeof(string));
			dataTable.Columns.Add("CHARACTER_SET_NAME", typeof(string));
			dataTable.Columns.Add("COLLATION_CATALOG", typeof(string));
			dataTable.Columns.Add("COLLATION_SCHEMA", typeof(string));
			dataTable.Columns.Add("COLLATION_NAME", typeof(string));
			dataTable.Columns.Add("DOMAIN_CATALOG", typeof(string));
			dataTable.Columns.Add("DOMAIN_NAME", typeof(string));
			dataTable.Columns.Add("DESCRIPTION", typeof(string));
			dataTable.Columns.Add("PRIMARY_KEY", typeof(bool));
			dataTable.Columns.Add("EDM_TYPE", typeof(string));
			dataTable.Columns.Add("AUTOINCREMENT", typeof(bool));
			dataTable.Columns.Add("UNIQUE", typeof(bool));
			dataTable.BeginLoadData();
			if (string.IsNullOrEmpty(strCatalog))
			{
				strCatalog = "main";
			}
			string text = (string.Compare(strCatalog, "temp", true, CultureInfo.InvariantCulture) != 0) ? "sqlite_master" : "sqlite_temp_master";
			using (SqliteCommand sqliteCommand = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "SELECT * FROM [{0}].[{1}] WHERE [type] LIKE 'table' OR [type] LIKE 'view'", new object[]
			{
				strCatalog,
				text
			}), this))
			{
				using (SqliteDataReader sqliteDataReader = sqliteCommand.ExecuteReader())
				{
					while (sqliteDataReader.Read())
					{
						if (!string.IsNullOrEmpty(strTable))
						{
							if (string.Compare(strTable, sqliteDataReader.GetString(2), true, CultureInfo.InvariantCulture) != 0)
							{
								continue;
							}
						}
						try
						{
							using (SqliteCommand sqliteCommand2 = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "SELECT * FROM [{0}].[{1}]", new object[]
							{
								strCatalog,
								sqliteDataReader.GetString(2)
							}), this))
							{
								using (SqliteDataReader sqliteDataReader2 = sqliteCommand2.ExecuteReader(CommandBehavior.SchemaOnly))
								{
									using (DataTable schemaTable = sqliteDataReader2.GetSchemaTable(true, true))
									{
										foreach (DataRow dataRow in schemaTable.Rows)
										{
											if (string.Compare(dataRow[SchemaTableColumn.ColumnName].ToString(), strColumn, true, CultureInfo.InvariantCulture) == 0 || strColumn == null)
											{
												DataRow dataRow2 = dataTable.NewRow();
												dataRow2["NUMERIC_PRECISION"] = dataRow[SchemaTableColumn.NumericPrecision];
												dataRow2["NUMERIC_SCALE"] = dataRow[SchemaTableColumn.NumericScale];
												dataRow2["TABLE_NAME"] = sqliteDataReader.GetString(2);
												dataRow2["COLUMN_NAME"] = dataRow[SchemaTableColumn.ColumnName];
												dataRow2["TABLE_CATALOG"] = strCatalog;
												dataRow2["ORDINAL_POSITION"] = dataRow[SchemaTableColumn.ColumnOrdinal];
												dataRow2["COLUMN_HASDEFAULT"] = (dataRow[SchemaTableOptionalColumn.DefaultValue] != DBNull.Value);
												dataRow2["COLUMN_DEFAULT"] = dataRow[SchemaTableOptionalColumn.DefaultValue];
												dataRow2["IS_NULLABLE"] = dataRow[SchemaTableColumn.AllowDBNull];
												dataRow2["DATA_TYPE"] = dataRow["DataTypeName"].ToString().ToLower(CultureInfo.InvariantCulture);
												dataRow2["EDM_TYPE"] = SqliteConvert.DbTypeToTypeName((DbType)((int)dataRow[SchemaTableColumn.ProviderType])).ToString().ToLower(CultureInfo.InvariantCulture);
												dataRow2["CHARACTER_MAXIMUM_LENGTH"] = dataRow[SchemaTableColumn.ColumnSize];
												dataRow2["TABLE_SCHEMA"] = dataRow[SchemaTableColumn.BaseSchemaName];
												dataRow2["PRIMARY_KEY"] = dataRow[SchemaTableColumn.IsKey];
												dataRow2["AUTOINCREMENT"] = dataRow[SchemaTableOptionalColumn.IsAutoIncrement];
												dataRow2["COLLATION_NAME"] = dataRow["CollationType"];
												dataRow2["UNIQUE"] = dataRow[SchemaTableColumn.IsUnique];
												dataTable.Rows.Add(dataRow2);
											}
										}
									}
								}
							}
						}
						catch (SqliteException)
						{
						}
					}
				}
			}
			dataTable.AcceptChanges();
			dataTable.EndLoadData();
			return dataTable;
		}
		private DataTable Schema_Indexes(string strCatalog, string strTable, string strIndex)
		{
			DataTable dataTable = new DataTable("Indexes");
			List<int> list = new List<int>();
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("TABLE_CATALOG", typeof(string));
			dataTable.Columns.Add("TABLE_SCHEMA", typeof(string));
			dataTable.Columns.Add("TABLE_NAME", typeof(string));
			dataTable.Columns.Add("INDEX_CATALOG", typeof(string));
			dataTable.Columns.Add("INDEX_SCHEMA", typeof(string));
			dataTable.Columns.Add("INDEX_NAME", typeof(string));
			dataTable.Columns.Add("PRIMARY_KEY", typeof(bool));
			dataTable.Columns.Add("UNIQUE", typeof(bool));
			dataTable.Columns.Add("CLUSTERED", typeof(bool));
			dataTable.Columns.Add("TYPE", typeof(int));
			dataTable.Columns.Add("FILL_FACTOR", typeof(int));
			dataTable.Columns.Add("INITIAL_SIZE", typeof(int));
			dataTable.Columns.Add("NULLS", typeof(int));
			dataTable.Columns.Add("SORT_BOOKMARKS", typeof(bool));
			dataTable.Columns.Add("AUTO_UPDATE", typeof(bool));
			dataTable.Columns.Add("NULL_COLLATION", typeof(int));
			dataTable.Columns.Add("ORDINAL_POSITION", typeof(int));
			dataTable.Columns.Add("COLUMN_NAME", typeof(string));
			dataTable.Columns.Add("COLUMN_GUID", typeof(Guid));
			dataTable.Columns.Add("COLUMN_PROPID", typeof(long));
			dataTable.Columns.Add("COLLATION", typeof(short));
			dataTable.Columns.Add("CARDINALITY", typeof(decimal));
			dataTable.Columns.Add("PAGES", typeof(int));
			dataTable.Columns.Add("FILTER_CONDITION", typeof(string));
			dataTable.Columns.Add("INTEGRATED", typeof(bool));
			dataTable.Columns.Add("INDEX_DEFINITION", typeof(string));
			dataTable.BeginLoadData();
			if (string.IsNullOrEmpty(strCatalog))
			{
				strCatalog = "main";
			}
			string text = (string.Compare(strCatalog, "temp", true, CultureInfo.InvariantCulture) != 0) ? "sqlite_master" : "sqlite_temp_master";
			using (SqliteCommand sqliteCommand = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "SELECT * FROM [{0}].[{1}] WHERE [type] LIKE 'table'", new object[]
			{
				strCatalog,
				text
			}), this))
			{
				using (SqliteDataReader sqliteDataReader = sqliteCommand.ExecuteReader())
				{
					while (sqliteDataReader.Read())
					{
						bool flag = false;
						list.Clear();
						if (!string.IsNullOrEmpty(strTable))
						{
							if (string.Compare(sqliteDataReader.GetString(2), strTable, true, CultureInfo.InvariantCulture) != 0)
							{
								continue;
							}
						}
						try
						{
							using (SqliteCommand sqliteCommand2 = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "PRAGMA [{0}].table_info([{1}])", new object[]
							{
								strCatalog,
								sqliteDataReader.GetString(2)
							}), this))
							{
								using (SqliteDataReader sqliteDataReader2 = sqliteCommand2.ExecuteReader())
								{
									while (sqliteDataReader2.Read())
									{
										if (sqliteDataReader2.GetInt32(5) == 1)
										{
											list.Add(sqliteDataReader2.GetInt32(0));
											if (string.Compare(sqliteDataReader2.GetString(2), "INTEGER", true, CultureInfo.InvariantCulture) == 0)
											{
												flag = true;
											}
										}
									}
								}
							}
						}
						catch (SqliteException)
						{
						}
						if (list.Count == 1 && flag)
						{
							DataRow dataRow = dataTable.NewRow();
							dataRow["TABLE_CATALOG"] = strCatalog;
							dataRow["TABLE_NAME"] = sqliteDataReader.GetString(2);
							dataRow["INDEX_CATALOG"] = strCatalog;
							dataRow["PRIMARY_KEY"] = true;
							dataRow["INDEX_NAME"] = string.Format(CultureInfo.InvariantCulture, "{1}_PK_{0}", new object[]
							{
								sqliteDataReader.GetString(2),
								text
							});
							dataRow["UNIQUE"] = true;
							if (string.Compare((string)dataRow["INDEX_NAME"], strIndex, true, CultureInfo.InvariantCulture) == 0 || strIndex == null)
							{
								dataTable.Rows.Add(dataRow);
							}
							list.Clear();
						}
						try
						{
							using (SqliteCommand sqliteCommand3 = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "PRAGMA [{0}].index_list([{1}])", new object[]
							{
								strCatalog,
								sqliteDataReader.GetString(2)
							}), this))
							{
								using (SqliteDataReader sqliteDataReader3 = sqliteCommand3.ExecuteReader())
								{
									while (sqliteDataReader3.Read())
									{
										if (string.Compare(sqliteDataReader3.GetString(1), strIndex, true, CultureInfo.InvariantCulture) == 0 || strIndex == null)
										{
											DataRow dataRow = dataTable.NewRow();
											dataRow["TABLE_CATALOG"] = strCatalog;
											dataRow["TABLE_NAME"] = sqliteDataReader.GetString(2);
											dataRow["INDEX_CATALOG"] = strCatalog;
											dataRow["INDEX_NAME"] = sqliteDataReader3.GetString(1);
											dataRow["UNIQUE"] = sqliteDataReader3.GetBoolean(2);
											dataRow["PRIMARY_KEY"] = false;
											using (SqliteCommand sqliteCommand4 = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "SELECT * FROM [{0}].[{2}] WHERE [type] LIKE 'index' AND [name] LIKE '{1}'", new object[]
											{
												strCatalog,
												sqliteDataReader3.GetString(1).Replace("'", "''"),
												text
											}), this))
											{
												using (SqliteDataReader sqliteDataReader4 = sqliteCommand4.ExecuteReader())
												{
													if (sqliteDataReader4.Read())
													{
														if (!sqliteDataReader4.IsDBNull(4))
														{
															dataRow["INDEX_DEFINITION"] = sqliteDataReader4.GetString(4);
														}
													}
												}
											}
											if (list.Count > 0 && sqliteDataReader3.GetString(1).StartsWith("sqlite_autoindex_" + sqliteDataReader.GetString(2), StringComparison.InvariantCultureIgnoreCase))
											{
												using (SqliteCommand sqliteCommand5 = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "PRAGMA [{0}].index_info([{1}])", new object[]
												{
													strCatalog,
													sqliteDataReader3.GetString(1)
												}), this))
												{
													using (SqliteDataReader sqliteDataReader5 = sqliteCommand5.ExecuteReader())
													{
														int num = 0;
														while (sqliteDataReader5.Read())
														{
															if (!list.Contains(sqliteDataReader5.GetInt32(1)))
															{
																num = 0;
																break;
															}
															num++;
														}
														if (num == list.Count)
														{
															dataRow["PRIMARY_KEY"] = true;
															list.Clear();
														}
													}
												}
											}
											dataTable.Rows.Add(dataRow);
										}
									}
								}
							}
						}
						catch (SqliteException)
						{
						}
					}
				}
			}
			dataTable.AcceptChanges();
			dataTable.EndLoadData();
			return dataTable;
		}
		private DataTable Schema_Triggers(string catalog, string table, string triggerName)
		{
			DataTable dataTable = new DataTable("Triggers");
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("TABLE_CATALOG", typeof(string));
			dataTable.Columns.Add("TABLE_SCHEMA", typeof(string));
			dataTable.Columns.Add("TABLE_NAME", typeof(string));
			dataTable.Columns.Add("TRIGGER_NAME", typeof(string));
			dataTable.Columns.Add("TRIGGER_DEFINITION", typeof(string));
			dataTable.BeginLoadData();
			if (string.IsNullOrEmpty(table))
			{
				table = null;
			}
			if (string.IsNullOrEmpty(catalog))
			{
				catalog = "main";
			}
			string text = (string.Compare(catalog, "temp", true, CultureInfo.InvariantCulture) != 0) ? "sqlite_master" : "sqlite_temp_master";
			using (SqliteCommand sqliteCommand = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "SELECT [type], [name], [tbl_name], [rootpage], [sql], [rowid] FROM [{0}].[{1}] WHERE [type] LIKE 'trigger'", new object[]
			{
				catalog,
				text
			}), this))
			{
				using (SqliteDataReader sqliteDataReader = sqliteCommand.ExecuteReader())
				{
					while (sqliteDataReader.Read())
					{
						if ((string.Compare(sqliteDataReader.GetString(1), triggerName, true, CultureInfo.InvariantCulture) == 0 || triggerName == null) && (table == null || string.Compare(table, sqliteDataReader.GetString(2), true, CultureInfo.InvariantCulture) == 0))
						{
							DataRow dataRow = dataTable.NewRow();
							dataRow["TABLE_CATALOG"] = catalog;
							dataRow["TABLE_NAME"] = sqliteDataReader.GetString(2);
							dataRow["TRIGGER_NAME"] = sqliteDataReader.GetString(1);
							dataRow["TRIGGER_DEFINITION"] = sqliteDataReader.GetString(4);
							dataTable.Rows.Add(dataRow);
						}
					}
				}
			}
			dataTable.AcceptChanges();
			dataTable.EndLoadData();
			return dataTable;
		}
		private DataTable Schema_Tables(string strCatalog, string strTable, string strType)
		{
			DataTable dataTable = new DataTable("Tables");
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("TABLE_CATALOG", typeof(string));
			dataTable.Columns.Add("TABLE_SCHEMA", typeof(string));
			dataTable.Columns.Add("TABLE_NAME", typeof(string));
			dataTable.Columns.Add("TABLE_TYPE", typeof(string));
			dataTable.Columns.Add("TABLE_ID", typeof(long));
			dataTable.Columns.Add("TABLE_ROOTPAGE", typeof(int));
			dataTable.Columns.Add("TABLE_DEFINITION", typeof(string));
			dataTable.BeginLoadData();
			if (string.IsNullOrEmpty(strCatalog))
			{
				strCatalog = "main";
			}
			string text = (string.Compare(strCatalog, "temp", true, CultureInfo.InvariantCulture) != 0) ? "sqlite_master" : "sqlite_temp_master";
			using (SqliteCommand sqliteCommand = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "SELECT [type], [name], [tbl_name], [rootpage], [sql], [rowid] FROM [{0}].[{1}] WHERE [type] LIKE 'table'", new object[]
			{
				strCatalog,
				text
			}), this))
			{
				using (SqliteDataReader sqliteDataReader = sqliteCommand.ExecuteReader())
				{
					while (sqliteDataReader.Read())
					{
						string text2 = sqliteDataReader.GetString(0);
						if (string.Compare(sqliteDataReader.GetString(2), 0, "SQLITE_", 0, 7, true, CultureInfo.InvariantCulture) == 0)
						{
							text2 = "SYSTEM_TABLE";
						}
						if ((string.Compare(strType, text2, true, CultureInfo.InvariantCulture) == 0 || strType == null) && (string.Compare(sqliteDataReader.GetString(2), strTable, true, CultureInfo.InvariantCulture) == 0 || strTable == null))
						{
							DataRow dataRow = dataTable.NewRow();
							dataRow["TABLE_CATALOG"] = strCatalog;
							dataRow["TABLE_NAME"] = sqliteDataReader.GetString(2);
							dataRow["TABLE_TYPE"] = text2;
							dataRow["TABLE_ID"] = sqliteDataReader.GetInt64(5);
							dataRow["TABLE_ROOTPAGE"] = sqliteDataReader.GetInt32(3);
							dataRow["TABLE_DEFINITION"] = sqliteDataReader.GetString(4);
							dataTable.Rows.Add(dataRow);
						}
					}
				}
			}
			dataTable.AcceptChanges();
			dataTable.EndLoadData();
			return dataTable;
		}
		private DataTable Schema_Views(string strCatalog, string strView)
		{
			DataTable dataTable = new DataTable("Views");
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("TABLE_CATALOG", typeof(string));
			dataTable.Columns.Add("TABLE_SCHEMA", typeof(string));
			dataTable.Columns.Add("TABLE_NAME", typeof(string));
			dataTable.Columns.Add("VIEW_DEFINITION", typeof(string));
			dataTable.Columns.Add("CHECK_OPTION", typeof(bool));
			dataTable.Columns.Add("IS_UPDATABLE", typeof(bool));
			dataTable.Columns.Add("DESCRIPTION", typeof(string));
			dataTable.Columns.Add("DATE_CREATED", typeof(DateTime));
			dataTable.Columns.Add("DATE_MODIFIED", typeof(DateTime));
			dataTable.BeginLoadData();
			if (string.IsNullOrEmpty(strCatalog))
			{
				strCatalog = "main";
			}
			string text = (string.Compare(strCatalog, "temp", true, CultureInfo.InvariantCulture) != 0) ? "sqlite_master" : "sqlite_temp_master";
			using (SqliteCommand sqliteCommand = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "SELECT * FROM [{0}].[{1}] WHERE [type] LIKE 'view'", new object[]
			{
				strCatalog,
				text
			}), this))
			{
				using (SqliteDataReader sqliteDataReader = sqliteCommand.ExecuteReader())
				{
					while (sqliteDataReader.Read())
					{
						if (string.Compare(sqliteDataReader.GetString(1), strView, true, CultureInfo.InvariantCulture) == 0 || string.IsNullOrEmpty(strView))
						{
							string text2 = sqliteDataReader.GetString(4).Replace('\r', ' ').Replace('\n', ' ').Replace('\t', ' ');
							int num = CultureInfo.InvariantCulture.CompareInfo.IndexOf(text2, " AS ", CompareOptions.IgnoreCase);
							if (num > -1)
							{
								text2 = text2.Substring(num + 4).Trim();
								DataRow dataRow = dataTable.NewRow();
								dataRow["TABLE_CATALOG"] = strCatalog;
								dataRow["TABLE_NAME"] = sqliteDataReader.GetString(2);
								dataRow["IS_UPDATABLE"] = false;
								dataRow["VIEW_DEFINITION"] = text2;
								dataTable.Rows.Add(dataRow);
							}
						}
					}
				}
			}
			dataTable.AcceptChanges();
			dataTable.EndLoadData();
			return dataTable;
		}
		private DataTable Schema_Catalogs(string strCatalog)
		{
			DataTable dataTable = new DataTable("Catalogs");
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("CATALOG_NAME", typeof(string));
			dataTable.Columns.Add("DESCRIPTION", typeof(string));
			dataTable.Columns.Add("ID", typeof(long));
			dataTable.BeginLoadData();
			using (SqliteCommand sqliteCommand = new SqliteCommand("PRAGMA database_list", this))
			{
				using (SqliteDataReader sqliteDataReader = sqliteCommand.ExecuteReader())
				{
					while (sqliteDataReader.Read())
					{
						if (string.Compare(sqliteDataReader.GetString(1), strCatalog, true, CultureInfo.InvariantCulture) == 0 || strCatalog == null)
						{
							DataRow dataRow = dataTable.NewRow();
							dataRow["CATALOG_NAME"] = sqliteDataReader.GetString(1);
							dataRow["DESCRIPTION"] = sqliteDataReader.GetString(2);
							dataRow["ID"] = sqliteDataReader.GetInt64(0);
							dataTable.Rows.Add(dataRow);
						}
					}
				}
			}
			dataTable.AcceptChanges();
			dataTable.EndLoadData();
			return dataTable;
		}
		private DataTable Schema_DataTypes()
		{
			DataTable dataTable = new DataTable("DataTypes");
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("TypeName", typeof(string));
			dataTable.Columns.Add("ProviderDbType", typeof(int));
			dataTable.Columns.Add("ColumnSize", typeof(long));
			dataTable.Columns.Add("CreateFormat", typeof(string));
			dataTable.Columns.Add("CreateParameters", typeof(string));
			dataTable.Columns.Add("DataType", typeof(string));
			dataTable.Columns.Add("IsAutoIncrementable", typeof(bool));
			dataTable.Columns.Add("IsBestMatch", typeof(bool));
			dataTable.Columns.Add("IsCaseSensitive", typeof(bool));
			dataTable.Columns.Add("IsFixedLength", typeof(bool));
			dataTable.Columns.Add("IsFixedPrecisionScale", typeof(bool));
			dataTable.Columns.Add("IsLong", typeof(bool));
			dataTable.Columns.Add("IsNullable", typeof(bool));
			dataTable.Columns.Add("IsSearchable", typeof(bool));
			dataTable.Columns.Add("IsSearchableWithLike", typeof(bool));
			dataTable.Columns.Add("IsLiteralSupported", typeof(bool));
			dataTable.Columns.Add("LiteralPrefix", typeof(string));
			dataTable.Columns.Add("LiteralSuffix", typeof(string));
			dataTable.Columns.Add("IsUnsigned", typeof(bool));
			dataTable.Columns.Add("MaximumScale", typeof(short));
			dataTable.Columns.Add("MinimumScale", typeof(short));
			dataTable.Columns.Add("IsConcurrencyType", typeof(bool));
			dataTable.BeginLoadData();
			StringReader stringReader = new StringReader(SR.DataTypes);
			dataTable.ReadXml(stringReader);
			stringReader.Close();
			dataTable.AcceptChanges();
			dataTable.EndLoadData();
			return dataTable;
		}
		private DataTable Schema_IndexColumns(string strCatalog, string strTable, string strIndex, string strColumn)
		{
			DataTable dataTable = new DataTable("IndexColumns");
			List<KeyValuePair<int, string>> list = new List<KeyValuePair<int, string>>();
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("CONSTRAINT_CATALOG", typeof(string));
			dataTable.Columns.Add("CONSTRAINT_SCHEMA", typeof(string));
			dataTable.Columns.Add("CONSTRAINT_NAME", typeof(string));
			dataTable.Columns.Add("TABLE_CATALOG", typeof(string));
			dataTable.Columns.Add("TABLE_SCHEMA", typeof(string));
			dataTable.Columns.Add("TABLE_NAME", typeof(string));
			dataTable.Columns.Add("COLUMN_NAME", typeof(string));
			dataTable.Columns.Add("ORDINAL_POSITION", typeof(int));
			dataTable.Columns.Add("INDEX_NAME", typeof(string));
			dataTable.Columns.Add("COLLATION_NAME", typeof(string));
			dataTable.Columns.Add("SORT_MODE", typeof(string));
			dataTable.Columns.Add("CONFLICT_OPTION", typeof(int));
			if (string.IsNullOrEmpty(strCatalog))
			{
				strCatalog = "main";
			}
			string text = (string.Compare(strCatalog, "temp", true, CultureInfo.InvariantCulture) != 0) ? "sqlite_master" : "sqlite_temp_master";
			dataTable.BeginLoadData();
			using (SqliteCommand sqliteCommand = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "SELECT * FROM [{0}].[{1}] WHERE [type] LIKE 'table'", new object[]
			{
				strCatalog,
				text
			}), this))
			{
				using (SqliteDataReader sqliteDataReader = sqliteCommand.ExecuteReader())
				{
					while (sqliteDataReader.Read())
					{
						bool flag = false;
						list.Clear();
						if (!string.IsNullOrEmpty(strTable))
						{
							if (string.Compare(sqliteDataReader.GetString(2), strTable, true, CultureInfo.InvariantCulture) != 0)
							{
								continue;
							}
						}
						try
						{
							using (SqliteCommand sqliteCommand2 = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "PRAGMA [{0}].table_info([{1}])", new object[]
							{
								strCatalog,
								sqliteDataReader.GetString(2)
							}), this))
							{
								using (SqliteDataReader sqliteDataReader2 = sqliteCommand2.ExecuteReader())
								{
									while (sqliteDataReader2.Read())
									{
										if (sqliteDataReader2.GetInt32(5) == 1)
										{
											list.Add(new KeyValuePair<int, string>(sqliteDataReader2.GetInt32(0), sqliteDataReader2.GetString(1)));
											if (string.Compare(sqliteDataReader2.GetString(2), "INTEGER", true, CultureInfo.InvariantCulture) == 0)
											{
												flag = true;
											}
										}
									}
								}
							}
						}
						catch (SqliteException)
						{
						}
						if (list.Count == 1 && flag)
						{
							DataRow dataRow = dataTable.NewRow();
							dataRow["CONSTRAINT_CATALOG"] = strCatalog;
							dataRow["CONSTRAINT_NAME"] = string.Format(CultureInfo.InvariantCulture, "{1}_PK_{0}", new object[]
							{
								sqliteDataReader.GetString(2),
								text
							});
							dataRow["TABLE_CATALOG"] = strCatalog;
							dataRow["TABLE_NAME"] = sqliteDataReader.GetString(2);
							dataRow["COLUMN_NAME"] = list[0].Value;
							dataRow["INDEX_NAME"] = dataRow["CONSTRAINT_NAME"];
							dataRow["ORDINAL_POSITION"] = 0;
							dataRow["COLLATION_NAME"] = "BINARY";
							dataRow["SORT_MODE"] = "ASC";
							dataRow["CONFLICT_OPTION"] = 2;
							if (string.IsNullOrEmpty(strIndex) || string.Compare(strIndex, (string)dataRow["INDEX_NAME"], true, CultureInfo.InvariantCulture) == 0)
							{
								dataTable.Rows.Add(dataRow);
							}
						}
						using (SqliteCommand sqliteCommand3 = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "SELECT * FROM [{0}].[{2}] WHERE [type] LIKE 'index' AND [tbl_name] LIKE '{1}'", new object[]
						{
							strCatalog,
							sqliteDataReader.GetString(2).Replace("'", "''"),
							text
						}), this))
						{
							using (SqliteDataReader sqliteDataReader3 = sqliteCommand3.ExecuteReader())
							{
								while (sqliteDataReader3.Read())
								{
									int num = 0;
									if (!string.IsNullOrEmpty(strIndex))
									{
										if (string.Compare(strIndex, sqliteDataReader3.GetString(1), true, CultureInfo.InvariantCulture) != 0)
										{
											continue;
										}
									}
									try
									{
										using (SqliteCommand sqliteCommand4 = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "PRAGMA [{0}].index_info([{1}])", new object[]
										{
											strCatalog,
											sqliteDataReader3.GetString(1)
										}), this))
										{
											using (SqliteDataReader sqliteDataReader4 = sqliteCommand4.ExecuteReader())
											{
												while (sqliteDataReader4.Read())
												{
													DataRow dataRow = dataTable.NewRow();
													dataRow["CONSTRAINT_CATALOG"] = strCatalog;
													dataRow["CONSTRAINT_NAME"] = sqliteDataReader3.GetString(1);
													dataRow["TABLE_CATALOG"] = strCatalog;
													dataRow["TABLE_NAME"] = sqliteDataReader3.GetString(2);
													dataRow["COLUMN_NAME"] = sqliteDataReader4.GetString(2);
													dataRow["INDEX_NAME"] = sqliteDataReader3.GetString(1);
													dataRow["ORDINAL_POSITION"] = num;
													int num2;
													int num3;
													string value;
													this._sql.GetIndexColumnExtendedInfo(strCatalog, sqliteDataReader3.GetString(1), sqliteDataReader4.GetString(2), out num2, out num3, out value);
													if (!string.IsNullOrEmpty(value))
													{
														dataRow["COLLATION_NAME"] = value;
													}
													dataRow["SORT_MODE"] = ((num2 != 0) ? "DESC" : "ASC");
													dataRow["CONFLICT_OPTION"] = num3;
													num++;
													if (string.IsNullOrEmpty(strColumn) || string.Compare(strColumn, dataRow["COLUMN_NAME"].ToString(), true, CultureInfo.InvariantCulture) == 0)
													{
														dataTable.Rows.Add(dataRow);
													}
												}
											}
										}
									}
									catch (SqliteException)
									{
									}
								}
							}
						}
					}
				}
			}
			dataTable.EndLoadData();
			dataTable.AcceptChanges();
			return dataTable;
		}
		private DataTable Schema_ViewColumns(string strCatalog, string strView, string strColumn)
		{
			DataTable dataTable = new DataTable("ViewColumns");
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("VIEW_CATALOG", typeof(string));
			dataTable.Columns.Add("VIEW_SCHEMA", typeof(string));
			dataTable.Columns.Add("VIEW_NAME", typeof(string));
			dataTable.Columns.Add("VIEW_COLUMN_NAME", typeof(string));
			dataTable.Columns.Add("TABLE_CATALOG", typeof(string));
			dataTable.Columns.Add("TABLE_SCHEMA", typeof(string));
			dataTable.Columns.Add("TABLE_NAME", typeof(string));
			dataTable.Columns.Add("COLUMN_NAME", typeof(string));
			dataTable.Columns.Add("ORDINAL_POSITION", typeof(int));
			dataTable.Columns.Add("COLUMN_HASDEFAULT", typeof(bool));
			dataTable.Columns.Add("COLUMN_DEFAULT", typeof(string));
			dataTable.Columns.Add("COLUMN_FLAGS", typeof(long));
			dataTable.Columns.Add("IS_NULLABLE", typeof(bool));
			dataTable.Columns.Add("DATA_TYPE", typeof(string));
			dataTable.Columns.Add("CHARACTER_MAXIMUM_LENGTH", typeof(int));
			dataTable.Columns.Add("NUMERIC_PRECISION", typeof(int));
			dataTable.Columns.Add("NUMERIC_SCALE", typeof(int));
			dataTable.Columns.Add("DATETIME_PRECISION", typeof(long));
			dataTable.Columns.Add("CHARACTER_SET_CATALOG", typeof(string));
			dataTable.Columns.Add("CHARACTER_SET_SCHEMA", typeof(string));
			dataTable.Columns.Add("CHARACTER_SET_NAME", typeof(string));
			dataTable.Columns.Add("COLLATION_CATALOG", typeof(string));
			dataTable.Columns.Add("COLLATION_SCHEMA", typeof(string));
			dataTable.Columns.Add("COLLATION_NAME", typeof(string));
			dataTable.Columns.Add("PRIMARY_KEY", typeof(bool));
			dataTable.Columns.Add("EDM_TYPE", typeof(string));
			dataTable.Columns.Add("AUTOINCREMENT", typeof(bool));
			dataTable.Columns.Add("UNIQUE", typeof(bool));
			if (string.IsNullOrEmpty(strCatalog))
			{
				strCatalog = "main";
			}
			string text = (string.Compare(strCatalog, "temp", true, CultureInfo.InvariantCulture) != 0) ? "sqlite_master" : "sqlite_temp_master";
			dataTable.BeginLoadData();
			using (SqliteCommand sqliteCommand = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "SELECT * FROM [{0}].[{1}] WHERE [type] LIKE 'view'", new object[]
			{
				strCatalog,
				text
			}), this))
			{
				using (SqliteDataReader sqliteDataReader = sqliteCommand.ExecuteReader())
				{
					while (sqliteDataReader.Read())
					{
						if (string.IsNullOrEmpty(strView) || string.Compare(strView, sqliteDataReader.GetString(2), true, CultureInfo.InvariantCulture) == 0)
						{
							using (SqliteCommand sqliteCommand2 = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "SELECT * FROM [{0}].[{1}]", new object[]
							{
								strCatalog,
								sqliteDataReader.GetString(2)
							}), this))
							{
								string text2 = sqliteDataReader.GetString(4).Replace('\r', ' ').Replace('\n', ' ').Replace('\t', ' ');
								int i = CultureInfo.InvariantCulture.CompareInfo.IndexOf(text2, " AS ", CompareOptions.IgnoreCase);
								if (i >= 0)
								{
									text2 = text2.Substring(i + 4);
									using (SqliteCommand sqliteCommand3 = new SqliteCommand(text2, this))
									{
										using (SqliteDataReader sqliteDataReader2 = sqliteCommand2.ExecuteReader(CommandBehavior.SchemaOnly))
										{
											using (SqliteDataReader sqliteDataReader3 = sqliteCommand3.ExecuteReader(CommandBehavior.SchemaOnly))
											{
												using (DataTable schemaTable = sqliteDataReader2.GetSchemaTable(false, false))
												{
													using (DataTable schemaTable2 = sqliteDataReader3.GetSchemaTable(false, false))
													{
														for (i = 0; i < schemaTable2.Rows.Count; i++)
														{
															DataRow dataRow = schemaTable.Rows[i];
															DataRow dataRow2 = schemaTable2.Rows[i];
															if (string.Compare(dataRow[SchemaTableColumn.ColumnName].ToString(), strColumn, true, CultureInfo.InvariantCulture) == 0 || strColumn == null)
															{
																DataRow dataRow3 = dataTable.NewRow();
																dataRow3["VIEW_CATALOG"] = strCatalog;
																dataRow3["VIEW_NAME"] = sqliteDataReader.GetString(2);
																dataRow3["TABLE_CATALOG"] = strCatalog;
																dataRow3["TABLE_SCHEMA"] = dataRow2[SchemaTableColumn.BaseSchemaName];
																dataRow3["TABLE_NAME"] = dataRow2[SchemaTableColumn.BaseTableName];
																dataRow3["COLUMN_NAME"] = dataRow2[SchemaTableColumn.BaseColumnName];
																dataRow3["VIEW_COLUMN_NAME"] = dataRow[SchemaTableColumn.ColumnName];
																dataRow3["COLUMN_HASDEFAULT"] = (dataRow[SchemaTableOptionalColumn.DefaultValue] != DBNull.Value);
																dataRow3["COLUMN_DEFAULT"] = dataRow[SchemaTableOptionalColumn.DefaultValue];
																dataRow3["ORDINAL_POSITION"] = dataRow[SchemaTableColumn.ColumnOrdinal];
																dataRow3["IS_NULLABLE"] = dataRow[SchemaTableColumn.AllowDBNull];
																dataRow3["DATA_TYPE"] = dataRow["DataTypeName"];
																dataRow3["EDM_TYPE"] = SqliteConvert.DbTypeToTypeName((DbType)((int)dataRow[SchemaTableColumn.ProviderType])).ToString().ToLower(CultureInfo.InvariantCulture);
																dataRow3["CHARACTER_MAXIMUM_LENGTH"] = dataRow[SchemaTableColumn.ColumnSize];
																dataRow3["TABLE_SCHEMA"] = dataRow[SchemaTableColumn.BaseSchemaName];
																dataRow3["PRIMARY_KEY"] = dataRow[SchemaTableColumn.IsKey];
																dataRow3["AUTOINCREMENT"] = dataRow[SchemaTableOptionalColumn.IsAutoIncrement];
																dataRow3["COLLATION_NAME"] = dataRow["CollationType"];
																dataRow3["UNIQUE"] = dataRow[SchemaTableColumn.IsUnique];
																dataTable.Rows.Add(dataRow3);
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			dataTable.EndLoadData();
			dataTable.AcceptChanges();
			return dataTable;
		}
		private DataTable Schema_ForeignKeys(string strCatalog, string strTable, string strKeyName)
		{
			DataTable dataTable = new DataTable("ForeignKeys");
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("CONSTRAINT_CATALOG", typeof(string));
			dataTable.Columns.Add("CONSTRAINT_SCHEMA", typeof(string));
			dataTable.Columns.Add("CONSTRAINT_NAME", typeof(string));
			dataTable.Columns.Add("TABLE_CATALOG", typeof(string));
			dataTable.Columns.Add("TABLE_SCHEMA", typeof(string));
			dataTable.Columns.Add("TABLE_NAME", typeof(string));
			dataTable.Columns.Add("CONSTRAINT_TYPE", typeof(string));
			dataTable.Columns.Add("IS_DEFERRABLE", typeof(bool));
			dataTable.Columns.Add("INITIALLY_DEFERRED", typeof(bool));
			dataTable.Columns.Add("FKEY_FROM_COLUMN", typeof(string));
			dataTable.Columns.Add("FKEY_FROM_ORDINAL_POSITION", typeof(int));
			dataTable.Columns.Add("FKEY_TO_CATALOG", typeof(string));
			dataTable.Columns.Add("FKEY_TO_SCHEMA", typeof(string));
			dataTable.Columns.Add("FKEY_TO_TABLE", typeof(string));
			dataTable.Columns.Add("FKEY_TO_COLUMN", typeof(string));
			if (string.IsNullOrEmpty(strCatalog))
			{
				strCatalog = "main";
			}
			string text = (string.Compare(strCatalog, "temp", true, CultureInfo.InvariantCulture) != 0) ? "sqlite_master" : "sqlite_temp_master";
			dataTable.BeginLoadData();
			using (SqliteCommand sqliteCommand = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "SELECT * FROM [{0}].[{1}] WHERE [type] LIKE 'table'", new object[]
			{
				strCatalog,
				text
			}), this))
			{
				using (SqliteDataReader sqliteDataReader = sqliteCommand.ExecuteReader())
				{
					while (sqliteDataReader.Read())
					{
						if (!string.IsNullOrEmpty(strTable))
						{
							if (string.Compare(strTable, sqliteDataReader.GetString(2), true, CultureInfo.InvariantCulture) != 0)
							{
								continue;
							}
						}
						try
						{
							using (SqliteCommandBuilder sqliteCommandBuilder = new SqliteCommandBuilder())
							{
								using (SqliteCommand sqliteCommand2 = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "PRAGMA [{0}].foreign_key_list([{1}])", new object[]
								{
									strCatalog,
									sqliteDataReader.GetString(2)
								}), this))
								{
									using (SqliteDataReader sqliteDataReader2 = sqliteCommand2.ExecuteReader())
									{
										while (sqliteDataReader2.Read())
										{
											DataRow dataRow = dataTable.NewRow();
											dataRow["CONSTRAINT_CATALOG"] = strCatalog;
											dataRow["CONSTRAINT_NAME"] = string.Format(CultureInfo.InvariantCulture, "FK_{0}_{1}", new object[]
											{
												sqliteDataReader[2],
												sqliteDataReader2.GetInt32(0)
											});
											dataRow["TABLE_CATALOG"] = strCatalog;
											dataRow["TABLE_NAME"] = sqliteCommandBuilder.UnquoteIdentifier(sqliteDataReader.GetString(2));
											dataRow["CONSTRAINT_TYPE"] = "FOREIGN KEY";
											dataRow["IS_DEFERRABLE"] = false;
											dataRow["INITIALLY_DEFERRED"] = false;
											dataRow["FKEY_FROM_COLUMN"] = sqliteCommandBuilder.UnquoteIdentifier(sqliteDataReader2[3].ToString());
											dataRow["FKEY_TO_CATALOG"] = strCatalog;
											dataRow["FKEY_TO_TABLE"] = sqliteCommandBuilder.UnquoteIdentifier(sqliteDataReader2[2].ToString());
											dataRow["FKEY_TO_COLUMN"] = sqliteCommandBuilder.UnquoteIdentifier(sqliteDataReader2[4].ToString());
											dataRow["FKEY_FROM_ORDINAL_POSITION"] = sqliteDataReader2[1];
											if (string.IsNullOrEmpty(strKeyName) || string.Compare(strKeyName, dataRow["CONSTRAINT_NAME"].ToString(), true, CultureInfo.InvariantCulture) == 0)
											{
												dataTable.Rows.Add(dataRow);
											}
										}
									}
								}
							}
						}
						catch (SqliteException)
						{
						}
					}
				}
			}
			dataTable.EndLoadData();
			dataTable.AcceptChanges();
			return dataTable;
		}
		private void UpdateCallback(IntPtr puser, int type, IntPtr database, IntPtr table, long rowid)
		{
			this._updateHandler(this, new UpdateEventArgs(SqliteConvert.UTF8ToString(database, -1), SqliteConvert.UTF8ToString(table, -1), (UpdateEventType)type, rowid));
		}
		private int CommitCallback(IntPtr parg)
		{
			CommitEventArgs commitEventArgs = new CommitEventArgs();
			this._commitHandler(this, commitEventArgs);
			return (!commitEventArgs.AbortTransaction) ? 0 : 1;
		}
		private void RollbackCallback(IntPtr parg)
		{
			this._rollbackHandler(this, EventArgs.Empty);
		}
		static Dictionary<string,int> map1;
	}
}
