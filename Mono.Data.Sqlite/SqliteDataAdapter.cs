using System;
using System.ComponentModel;
using System.Data.Common;
namespace Mono.Data.Sqlite
{
	[DefaultEvent("RowUpdated"), Designer("Microsoft.VSDesigner.Data.VS.SqlDataAdapterDesigner, Microsoft.VSDesigner, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), ToolboxItem("SQLite.Designer.SqliteDataAdapterToolboxItem, SQLite.Designer, Version=1.0.36.0, Culture=neutral, PublicKeyToken=db937bc2d44ff139")]
	public sealed class SqliteDataAdapter : DbDataAdapter
	{
		private static object _updatingEventPH = new object();
		private static object _updatedEventPH = new object();
		public event EventHandler<RowUpdatingEventArgs> RowUpdating
		{
			add
			{
				EventHandler<RowUpdatingEventArgs> eventHandler = (EventHandler<RowUpdatingEventArgs>)base.Events[SqliteDataAdapter._updatingEventPH];
				if (eventHandler != null && value.Target is DbCommandBuilder)
				{
					EventHandler<RowUpdatingEventArgs> eventHandler2 = (EventHandler<RowUpdatingEventArgs>)SqliteDataAdapter.FindBuilder(eventHandler);
					if (eventHandler2 != null)
					{
						base.Events.RemoveHandler(SqliteDataAdapter._updatingEventPH, eventHandler2);
					}
				}
				base.Events.AddHandler(SqliteDataAdapter._updatingEventPH, value);
			}
			remove
			{
				base.Events.RemoveHandler(SqliteDataAdapter._updatingEventPH, value);
			}
		}
		public event EventHandler<RowUpdatedEventArgs> RowUpdated
		{
			add
			{
				base.Events.AddHandler(SqliteDataAdapter._updatedEventPH, value);
			}
			remove
			{
				base.Events.RemoveHandler(SqliteDataAdapter._updatedEventPH, value);
			}
		}
		[DefaultValue(null), Editor("Microsoft.VSDesigner.Data.Design.DBCommandEditor, Microsoft.VSDesigner, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		public new SqliteCommand SelectCommand
		{
			get
			{
				return (SqliteCommand)base.SelectCommand;
			}
			set
			{
				base.SelectCommand = value;
			}
		}
		[DefaultValue(null), Editor("Microsoft.VSDesigner.Data.Design.DBCommandEditor, Microsoft.VSDesigner, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		public new SqliteCommand InsertCommand
		{
			get
			{
				return (SqliteCommand)base.InsertCommand;
			}
			set
			{
				base.InsertCommand = value;
			}
		}
		[DefaultValue(null), Editor("Microsoft.VSDesigner.Data.Design.DBCommandEditor, Microsoft.VSDesigner, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		public new SqliteCommand UpdateCommand
		{
			get
			{
				return (SqliteCommand)base.UpdateCommand;
			}
			set
			{
				base.UpdateCommand = value;
			}
		}
		[DefaultValue(null), Editor("Microsoft.VSDesigner.Data.Design.DBCommandEditor, Microsoft.VSDesigner, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		public new SqliteCommand DeleteCommand
		{
			get
			{
				return (SqliteCommand)base.DeleteCommand;
			}
			set
			{
				base.DeleteCommand = value;
			}
		}
		public SqliteDataAdapter()
		{
		}
		public SqliteDataAdapter(SqliteCommand cmd)
		{
			this.SelectCommand = cmd;
		}
		public SqliteDataAdapter(string commandText, SqliteConnection connection)
		{
			this.SelectCommand = new SqliteCommand(commandText, connection);
		}
		public SqliteDataAdapter(string commandText, string connectionString)
		{
			SqliteConnection connection = new SqliteConnection(connectionString);
			this.SelectCommand = new SqliteCommand(commandText, connection);
		}
		internal static Delegate FindBuilder(MulticastDelegate mcd)
		{
			if (mcd != null)
			{
				Delegate[] invocationList = mcd.GetInvocationList();
				for (int i = 0; i < invocationList.Length; i++)
				{
					if (invocationList[i].Target is DbCommandBuilder)
					{
						return invocationList[i];
					}
				}
			}
			return null;
		}
		protected override void OnRowUpdating(RowUpdatingEventArgs value)
		{
			EventHandler<RowUpdatingEventArgs> eventHandler = base.Events[SqliteDataAdapter._updatingEventPH] as EventHandler<RowUpdatingEventArgs>;
			if (eventHandler != null)
			{
				eventHandler(this, value);
			}
		}
		protected override void OnRowUpdated(RowUpdatedEventArgs value)
		{
			EventHandler<RowUpdatedEventArgs> eventHandler = base.Events[SqliteDataAdapter._updatedEventPH] as EventHandler<RowUpdatedEventArgs>;
			if (eventHandler != null)
			{
				eventHandler(this, value);
			}
		}
	}
}
