using System;
namespace Mono.Data.Sqlite
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
	public sealed class SqliteFunctionAttribute : Attribute
	{
		private string _name;
		private int _arguments;
		private FunctionType _functionType;
		internal Type _instanceType;
		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}
		public int Arguments
		{
			get
			{
				return this._arguments;
			}
			set
			{
				this._arguments = value;
			}
		}
		public FunctionType FuncType
		{
			get
			{
				return this._functionType;
			}
			set
			{
				this._functionType = value;
			}
		}
		public SqliteFunctionAttribute()
		{
			this.Name = string.Empty;
			this.Arguments = -1;
			this.FuncType = FunctionType.Scalar;
		}
	}
}
