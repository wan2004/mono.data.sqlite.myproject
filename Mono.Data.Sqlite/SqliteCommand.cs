using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
namespace Mono.Data.Sqlite
{
	[Designer("SQLite.Designer.SqliteCommandDesigner, SQLite.Designer, Version=1.0.36.0, Culture=neutral, PublicKeyToken=db937bc2d44ff139"), ToolboxItem(true)]
	public sealed class SqliteCommand : DbCommand, ICloneable
	{
		private string _commandText;
		private SqliteConnection _cnn;
		private long _version;
		private WeakReference _activeReader;
		internal int _commandTimeout;
		private bool _designTimeVisible;
		private UpdateRowSource _updateRowSource;
		private SqliteParameterCollection _parameterCollection;
		internal List<SqliteStatement> _statementList;
		internal string _remainingText;
		private SqliteTransaction _transaction;
		[DefaultValue(""), Editor("Microsoft.VSDesigner.Data.SQL.Design.SqlCommandTextEditor, Microsoft.VSDesigner, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), RefreshProperties(RefreshProperties.All)]
		public override string CommandText
		{
			get
			{
				return this._commandText;
			}
			set
			{
				if (this._commandText == value)
				{
					return;
				}
				if (this._activeReader != null && this._activeReader.IsAlive)
				{
					throw new InvalidOperationException("Cannot set CommandText while a DataReader is active");
				}
				this.ClearCommands();
				this._commandText = value;
				if (this._cnn == null)
				{
					return;
				}
			}
		}
		[DefaultValue(30)]
		public override int CommandTimeout
		{
			get
			{
				return this._commandTimeout;
			}
			set
			{
				this._commandTimeout = value;
			}
		}
		[DefaultValue(CommandType.Text), RefreshProperties(RefreshProperties.All)]
		public override CommandType CommandType
		{
			get
			{
				return CommandType.Text;
			}
			set
			{
				if (value != CommandType.Text)
				{
					throw new NotSupportedException();
				}
			}
		}
		[DefaultValue(null), Editor("Microsoft.VSDesigner.Data.Design.DbConnectionEditor, Microsoft.VSDesigner, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		public new SqliteConnection Connection
		{
			get
			{
				return this._cnn;
			}
			set
			{
				if (this._activeReader != null && this._activeReader.IsAlive)
				{
					throw new InvalidOperationException("Cannot set Connection while a DataReader is active");
				}
				if (this._cnn != null)
				{
					this.ClearCommands();
				}
				this._cnn = value;
				if (this._cnn != null)
				{
					this._version = this._cnn._version;
				}
			}
		}
		protected override DbConnection DbConnection
		{
			get
			{
				return this.Connection;
			}
			set
			{
				this.Connection = (SqliteConnection)value;
			}
		}
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new SqliteParameterCollection Parameters
		{
			get
			{
				return this._parameterCollection;
			}
		}
		protected override DbParameterCollection DbParameterCollection
		{
			get
			{
				return this.Parameters;
			}
		}
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new SqliteTransaction Transaction
		{
			get
			{
				return this._transaction;
			}
			set
			{
				if (this._cnn != null)
				{
					if (this._activeReader != null && this._activeReader.IsAlive)
					{
						throw new InvalidOperationException("Cannot set Transaction while a DataReader is active");
					}
					if (value != null && value._cnn != this._cnn)
					{
						throw new ArgumentException("Transaction is not associated with the command's connection");
					}
					this._transaction = value;
				}
				else
				{
					this.Connection = value.Connection;
					this._transaction = value;
				}
			}
		}
		protected override DbTransaction DbTransaction
		{
			get
			{
				return this.Transaction;
			}
			set
			{
				this.Transaction = (SqliteTransaction)value;
			}
		}
		[DefaultValue(UpdateRowSource.None)]
		public override UpdateRowSource UpdatedRowSource
		{
			get
			{
				return this._updateRowSource;
			}
			set
			{
				this._updateRowSource = value;
			}
		}
		[Browsable(false), DefaultValue(true), DesignOnly(true), EditorBrowsable(EditorBrowsableState.Never)]
		public override bool DesignTimeVisible
		{
			get
			{
				return this._designTimeVisible;
			}
			set
			{
				this._designTimeVisible = value;
				TypeDescriptor.Refresh(this);
			}
		}
		public SqliteCommand() : this(null, null)
		{
		}
		public SqliteCommand(string commandText) : this(commandText, null, null)
		{
		}
		public SqliteCommand(string commandText, SqliteConnection connection) : this(commandText, connection, null)
		{
		}
		public SqliteCommand(SqliteConnection connection) : this(null, connection, null)
		{
		}
		private SqliteCommand(SqliteCommand source) : this(source.CommandText, source.Connection, source.Transaction)
		{
			this.CommandTimeout = source.CommandTimeout;
			this.DesignTimeVisible = source.DesignTimeVisible;
			this.UpdatedRowSource = source.UpdatedRowSource;
			foreach (SqliteParameter sqliteParameter in source._parameterCollection)
			{
				this.Parameters.Add(sqliteParameter.Clone());
			}
		}
		public SqliteCommand(string commandText, SqliteConnection connection, SqliteTransaction transaction)
		{
			this._statementList = null;
			this._activeReader = null;
			this._commandTimeout = 30;
			this._parameterCollection = new SqliteParameterCollection(this);
			this._designTimeVisible = true;
			this._updateRowSource = UpdateRowSource.None;
			this._transaction = null;
			if (commandText != null)
			{
				this.CommandText = commandText;
			}
			if (connection != null)
			{
				this.DbConnection = connection;
				this._commandTimeout = connection.DefaultTimeout;
			}
			if (transaction != null)
			{
				this.Transaction = transaction;
			}
		}
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (disposing)
			{
				SqliteDataReader sqliteDataReader = null;
				if (this._activeReader != null)
				{
					try
					{
						sqliteDataReader = (this._activeReader.Target as SqliteDataReader);
					}
					catch
					{
					}
				}
				if (sqliteDataReader != null)
				{
					sqliteDataReader._disposeCommand = true;
					this._activeReader = null;
					return;
				}
				this.Connection = null;
				this._parameterCollection.Clear();
				this._commandText = null;
			}
		}
		internal void ClearCommands()
		{
			if (this._activeReader != null)
			{
				SqliteDataReader sqliteDataReader = null;
				try
				{
					sqliteDataReader = (this._activeReader.Target as SqliteDataReader);
				}
				catch
				{
				}
				if (sqliteDataReader != null)
				{
					sqliteDataReader.Close();
				}
				this._activeReader = null;
			}
			if (this._statementList == null)
			{
				return;
			}
			int count = this._statementList.Count;
			for (int i = 0; i < count; i++)
			{
				this._statementList[i].Dispose();
			}
			this._statementList = null;
			this._parameterCollection.Unbind();
		}
		internal SqliteStatement BuildNextCommand()
		{
			SqliteStatement sqliteStatement = null;
			SqliteStatement result;
			try
			{
				if (this._statementList == null)
				{
					this._remainingText = this._commandText;
				}
				sqliteStatement = this._cnn._sql.Prepare(this._cnn, this._remainingText, (this._statementList != null) ? this._statementList[this._statementList.Count - 1] : null, (uint)(this._commandTimeout * 1000), out this._remainingText);
				if (sqliteStatement != null)
				{
					sqliteStatement._command = this;
					if (this._statementList == null)
					{
						this._statementList = new List<SqliteStatement>();
					}
					this._statementList.Add(sqliteStatement);
					this._parameterCollection.MapParameters(sqliteStatement);
					sqliteStatement.BindParameters();
				}
				result = sqliteStatement;
			}
			catch (Exception)
			{
				if (sqliteStatement != null)
				{
					if (this._statementList.Contains(sqliteStatement))
					{
						this._statementList.Remove(sqliteStatement);
					}
					sqliteStatement.Dispose();
				}
				this._remainingText = null;
				throw;
			}
			return result;
		}
		internal SqliteStatement GetStatement(int index)
		{
			if (this._statementList == null)
			{
				return this.BuildNextCommand();
			}
			if (index != this._statementList.Count)
			{
				SqliteStatement sqliteStatement = this._statementList[index];
				sqliteStatement.BindParameters();
				return sqliteStatement;
			}
			if (!string.IsNullOrEmpty(this._remainingText))
			{
				return this.BuildNextCommand();
			}
			return null;
		}
		public override void Cancel()
		{
			if (this._activeReader != null)
			{
				SqliteDataReader sqliteDataReader = this._activeReader.Target as SqliteDataReader;
				if (sqliteDataReader != null)
				{
					sqliteDataReader.Cancel();
				}
			}
		}
		protected override DbParameter CreateDbParameter()
		{
			return this.CreateParameter();
		}
		public new SqliteParameter CreateParameter()
		{
			return new SqliteParameter();
		}
		private void InitializeForReader()
		{
			if (this._activeReader != null && this._activeReader.IsAlive)
			{
				throw new InvalidOperationException("DataReader already active on this command");
			}
			if (this._cnn == null)
			{
				throw new InvalidOperationException("No connection associated with this command");
			}
			if (this._cnn.State != ConnectionState.Open)
			{
				throw new InvalidOperationException("Database is not open");
			}
			if (this._cnn._version != this._version)
			{
				this._version = this._cnn._version;
				this.ClearCommands();
			}
			this._parameterCollection.MapParameters(null);
		}
		protected override DbDataReader ExecuteDbDataReader(CommandBehavior behavior)
		{
			return this.ExecuteReader(behavior);
		}
		public new SqliteDataReader ExecuteReader(CommandBehavior behavior)
		{
			this.InitializeForReader();
			SqliteDataReader sqliteDataReader = new SqliteDataReader(this, behavior);
			this._activeReader = new WeakReference(sqliteDataReader, false);
			return sqliteDataReader;
		}
		public new SqliteDataReader ExecuteReader()
		{
			return this.ExecuteReader(CommandBehavior.Default);
		}
		internal void ClearDataReader()
		{
			this._activeReader = null;
		}
		public override int ExecuteNonQuery()
		{
			int recordsAffected;
			using (SqliteDataReader sqliteDataReader = this.ExecuteReader(CommandBehavior.SingleResult | CommandBehavior.SingleRow))
			{
				while (sqliteDataReader.NextResult())
				{
				}
				recordsAffected = sqliteDataReader.RecordsAffected;
			}
			return recordsAffected;
		}
		public override object ExecuteScalar()
		{
			using (SqliteDataReader sqliteDataReader = this.ExecuteReader(CommandBehavior.SingleResult | CommandBehavior.SingleRow))
			{
				if (sqliteDataReader.Read())
				{
					return sqliteDataReader[0];
				}
			}
			return null;
		}
		public override void Prepare()
		{
		}
		public object Clone()
		{
			return new SqliteCommand(this);
		}
	}
}
