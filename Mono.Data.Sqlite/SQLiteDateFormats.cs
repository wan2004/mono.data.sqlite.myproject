using System;
namespace Mono.Data.Sqlite
{
	public enum SQLiteDateFormats
	{
		Ticks,
		ISO8601,
		JulianDay
	}
}
