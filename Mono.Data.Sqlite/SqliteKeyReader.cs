using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
namespace Mono.Data.Sqlite
{
	internal sealed class SqliteKeyReader : IDisposable
	{
		private struct KeyInfo
		{
			internal string databaseName;
			internal string tableName;
			internal string columnName;
			internal int database;
			internal int rootPage;
			internal int cursor;
			internal SqliteKeyReader.KeyQuery query;
			internal int column;
		}
		private sealed class KeyQuery : IDisposable
		{
			private SqliteCommand _command;
			internal SqliteDataReader _reader;
			internal bool IsValid
			{
				get
				{
					return this._reader != null;
				}
				set
				{
					if (value)
					{
						throw new ArgumentException();
					}
					if (this._reader != null)
					{
						this._reader.Dispose();
						this._reader = null;
					}
				}
			}
			internal KeyQuery(SqliteConnection cnn, string database, string table, params string[] columns)
			{
				using (SqliteCommandBuilder sqliteCommandBuilder = new SqliteCommandBuilder())
				{
					this._command = cnn.CreateCommand();
					for (int i = 0; i < columns.Length; i++)
					{
						columns[i] = sqliteCommandBuilder.QuoteIdentifier(columns[i]);
					}
				}
				this._command.CommandText = string.Format("SELECT {0} FROM [{1}].[{2}] WHERE ROWID = ?", string.Join(",", columns), database, table);
				this._command.Parameters.AddWithValue(null, 0L);
			}
			internal void Sync(long rowid)
			{
				this.IsValid = false;
				this._command.Parameters[0].Value = rowid;
				this._reader = this._command.ExecuteReader();
				this._reader.Read();
			}
			public void Dispose()
			{
				this.IsValid = false;
				if (this._command != null)
				{
					this._command.Dispose();
				}
				this._command = null;
			}
		}
		private SqliteKeyReader.KeyInfo[] _keyInfo;
		private SqliteStatement _stmt;
		private bool _isValid;
		internal int Count
		{
			get
			{
				return (this._keyInfo != null) ? this._keyInfo.Length : 0;
			}
		}
		internal SqliteKeyReader(SqliteConnection cnn, SqliteDataReader reader, SqliteStatement stmt)
		{
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
			Dictionary<string, List<string>> dictionary2 = new Dictionary<string, List<string>>();
			List<SqliteKeyReader.KeyInfo> list = new List<SqliteKeyReader.KeyInfo>();
			this._stmt = stmt;
			using (DataTable schema = cnn.GetSchema("Catalogs"))
			{
				foreach (DataRow dataRow in schema.Rows)
				{
					dictionary.Add((string)dataRow["CATALOG_NAME"], Convert.ToInt32(dataRow["ID"]));
				}
			}
			using (DataTable schemaTable = reader.GetSchemaTable(false, false))
			{
				foreach (DataRow dataRow2 in schemaTable.Rows)
				{
					if (dataRow2[SchemaTableOptionalColumn.BaseCatalogName] != DBNull.Value)
					{
						string key = (string)dataRow2[SchemaTableOptionalColumn.BaseCatalogName];
						string item = (string)dataRow2[SchemaTableColumn.BaseTableName];
						List<string> list2;
						if (!dictionary2.ContainsKey(key))
						{
							list2 = new List<string>();
							dictionary2.Add(key, list2);
						}
						else
						{
							list2 = dictionary2[key];
						}
						if (!list2.Contains(item))
						{
							list2.Add(item);
						}
					}
				}
				foreach (KeyValuePair<string, List<string>> current in dictionary2)
				{
					for (int i = 0; i < current.Value.Count; i++)
					{
						string text = current.Value[i];
						DataRow dataRow3 = null;
						using (DataTable schema2 = cnn.GetSchema("Indexes", new string[]
						{
							current.Key,
							null,
							text
						}))
						{
							int num = 0;
							while (num < 2 && dataRow3 == null)
							{
								foreach (DataRow dataRow4 in schema2.Rows)
								{
									if (num == 0 && (bool)dataRow4["PRIMARY_KEY"])
									{
										dataRow3 = dataRow4;
										break;
									}
									if (num == 1 && (bool)dataRow4["UNIQUE"])
									{
										dataRow3 = dataRow4;
										break;
									}
								}
								num++;
							}
							if (dataRow3 == null)
							{
								current.Value.RemoveAt(i);
								i--;
							}
							else
							{
								using (DataTable schema3 = cnn.GetSchema("Tables", new string[]
								{
									current.Key,
									null,
									text
								}))
								{
									int database = dictionary[current.Key];
									int rootPage = Convert.ToInt32(schema3.Rows[0]["TABLE_ROOTPAGE"]);
									int cursorForTable = stmt._sql.GetCursorForTable(stmt, database, rootPage);
									using (DataTable schema4 = cnn.GetSchema("IndexColumns", new string[]
									{
										current.Key,
										null,
										text,
										(string)dataRow3["INDEX_NAME"]
									}))
									{
										SqliteKeyReader.KeyQuery query = null;
										List<string> list3 = new List<string>();
										for (int j = 0; j < schema4.Rows.Count; j++)
										{
											bool flag = true;
											foreach (DataRow dataRow5 in schemaTable.Rows)
											{
												if (!dataRow5.IsNull(SchemaTableColumn.BaseColumnName))
												{
													if ((string)dataRow5[SchemaTableColumn.BaseColumnName] == (string)schema4.Rows[j]["COLUMN_NAME"] && (string)dataRow5[SchemaTableColumn.BaseTableName] == text && (string)dataRow5[SchemaTableOptionalColumn.BaseCatalogName] == current.Key)
													{
														schema4.Rows.RemoveAt(j);
														j--;
														flag = false;
														break;
													}
												}
											}
											if (flag)
											{
												list3.Add((string)schema4.Rows[j]["COLUMN_NAME"]);
											}
										}
										if ((string)dataRow3["INDEX_NAME"] != "sqlite_master_PK_" + text && list3.Count > 0)
										{
											string[] array = new string[list3.Count];
											list3.CopyTo(array);
											query = new SqliteKeyReader.KeyQuery(cnn, current.Key, text, array);
										}
										for (int k = 0; k < schema4.Rows.Count; k++)
										{
											string columnName = (string)schema4.Rows[k]["COLUMN_NAME"];
											list.Add(new SqliteKeyReader.KeyInfo
											{
												rootPage = rootPage,
												cursor = cursorForTable,
												database = database,
												databaseName = current.Key,
												tableName = text,
												columnName = columnName,
												query = query,
												column = k
											});
										}
									}
								}
							}
						}
					}
				}
			}
			this._keyInfo = new SqliteKeyReader.KeyInfo[list.Count];
			list.CopyTo(this._keyInfo);
		}
		internal void Sync(int i)
		{
			this.Sync();
			if (this._keyInfo[i].cursor == -1)
			{
				throw new InvalidCastException();
			}
		}
		internal void Sync()
		{
			if (this._isValid)
			{
				return;
			}
			SqliteKeyReader.KeyQuery keyQuery = null;
			for (int i = 0; i < this._keyInfo.Length; i++)
			{
				if (this._keyInfo[i].query == null || this._keyInfo[i].query != keyQuery)
				{
					keyQuery = this._keyInfo[i].query;
					if (keyQuery != null)
					{
						keyQuery.Sync(this._stmt._sql.GetRowIdForCursor(this._stmt, this._keyInfo[i].cursor));
					}
				}
			}
			this._isValid = true;
		}
		internal void Reset()
		{
			this._isValid = false;
			if (this._keyInfo == null)
			{
				return;
			}
			for (int i = 0; i < this._keyInfo.Length; i++)
			{
				if (this._keyInfo[i].query != null)
				{
					this._keyInfo[i].query.IsValid = false;
				}
			}
		}
		public void Dispose()
		{
			this._stmt = null;
			if (this._keyInfo == null)
			{
				return;
			}
			for (int i = 0; i < this._keyInfo.Length; i++)
			{
				if (this._keyInfo[i].query != null)
				{
					this._keyInfo[i].query.Dispose();
				}
			}
			this._keyInfo = null;
		}
		internal string GetDataTypeName(int i)
		{
			this.Sync();
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetDataTypeName(this._keyInfo[i].column);
			}
			return "integer";
		}
		internal Type GetFieldType(int i)
		{
			this.Sync();
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetFieldType(this._keyInfo[i].column);
			}
			return typeof(long);
		}
		internal string GetName(int i)
		{
			return this._keyInfo[i].columnName;
		}
		internal int GetOrdinal(string name)
		{
			for (int i = 0; i < this._keyInfo.Length; i++)
			{
				if (string.Compare(name, this._keyInfo[i].columnName, StringComparison.OrdinalIgnoreCase) == 0)
				{
					return i;
				}
			}
			return -1;
		}
		internal bool GetBoolean(int i)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetBoolean(this._keyInfo[i].column);
			}
			throw new InvalidCastException();
		}
		internal byte GetByte(int i)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetByte(this._keyInfo[i].column);
			}
			throw new InvalidCastException();
		}
		internal long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetBytes(this._keyInfo[i].column, fieldOffset, buffer, bufferoffset, length);
			}
			throw new InvalidCastException();
		}
		internal char GetChar(int i)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetChar(this._keyInfo[i].column);
			}
			throw new InvalidCastException();
		}
		internal long GetChars(int i, long fieldOffset, char[] buffer, int bufferoffset, int length)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetChars(this._keyInfo[i].column, fieldOffset, buffer, bufferoffset, length);
			}
			throw new InvalidCastException();
		}
		internal DateTime GetDateTime(int i)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetDateTime(this._keyInfo[i].column);
			}
			throw new InvalidCastException();
		}
		internal decimal GetDecimal(int i)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetDecimal(this._keyInfo[i].column);
			}
			throw new InvalidCastException();
		}
		internal double GetDouble(int i)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetDouble(this._keyInfo[i].column);
			}
			throw new InvalidCastException();
		}
		internal float GetFloat(int i)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetFloat(this._keyInfo[i].column);
			}
			throw new InvalidCastException();
		}
		internal Guid GetGuid(int i)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetGuid(this._keyInfo[i].column);
			}
			throw new InvalidCastException();
		}
		internal short GetInt16(int i)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetInt16(this._keyInfo[i].column);
			}
			long rowIdForCursor = this._stmt._sql.GetRowIdForCursor(this._stmt, this._keyInfo[i].cursor);
			if (rowIdForCursor == 0L)
			{
				throw new InvalidCastException();
			}
			return Convert.ToInt16(rowIdForCursor);
		}
		internal int GetInt32(int i)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetInt32(this._keyInfo[i].column);
			}
			long rowIdForCursor = this._stmt._sql.GetRowIdForCursor(this._stmt, this._keyInfo[i].cursor);
			if (rowIdForCursor == 0L)
			{
				throw new InvalidCastException();
			}
			return Convert.ToInt32(rowIdForCursor);
		}
		internal long GetInt64(int i)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetInt64(this._keyInfo[i].column);
			}
			long rowIdForCursor = this._stmt._sql.GetRowIdForCursor(this._stmt, this._keyInfo[i].cursor);
			if (rowIdForCursor == 0L)
			{
				throw new InvalidCastException();
			}
			return Convert.ToInt64(rowIdForCursor);
		}
		internal string GetString(int i)
		{
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetString(this._keyInfo[i].column);
			}
			throw new InvalidCastException();
		}
		internal object GetValue(int i)
		{
			if (this._keyInfo[i].cursor == -1)
			{
				return DBNull.Value;
			}
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.GetValue(this._keyInfo[i].column);
			}
			if (this.IsDBNull(i))
			{
				return DBNull.Value;
			}
			return this.GetInt64(i);
		}
		internal bool IsDBNull(int i)
		{
			if (this._keyInfo[i].cursor == -1)
			{
				return true;
			}
			this.Sync(i);
			if (this._keyInfo[i].query != null)
			{
				return this._keyInfo[i].query._reader.IsDBNull(this._keyInfo[i].column);
			}
			return this._stmt._sql.GetRowIdForCursor(this._stmt, this._keyInfo[i].cursor) == 0L;
		}
		internal void AppendSchemaTable(DataTable tbl)
		{
			SqliteKeyReader.KeyQuery keyQuery = null;
			for (int i = 0; i < this._keyInfo.Length; i++)
			{
				if (this._keyInfo[i].query == null || this._keyInfo[i].query != keyQuery)
				{
					keyQuery = this._keyInfo[i].query;
					if (keyQuery == null)
					{
						DataRow dataRow = tbl.NewRow();
						dataRow[SchemaTableColumn.ColumnName] = this._keyInfo[i].columnName;
						dataRow[SchemaTableColumn.ColumnOrdinal] = tbl.Rows.Count;
						dataRow[SchemaTableColumn.ColumnSize] = 8;
						dataRow[SchemaTableColumn.NumericPrecision] = 255;
						dataRow[SchemaTableColumn.NumericScale] = 255;
						dataRow[SchemaTableColumn.ProviderType] = DbType.Int64;
						dataRow[SchemaTableColumn.IsLong] = false;
						dataRow[SchemaTableColumn.AllowDBNull] = false;
						dataRow[SchemaTableOptionalColumn.IsReadOnly] = false;
						dataRow[SchemaTableOptionalColumn.IsRowVersion] = false;
						dataRow[SchemaTableColumn.IsUnique] = false;
						dataRow[SchemaTableColumn.IsKey] = true;
						dataRow[SchemaTableColumn.DataType] = typeof(long);
						dataRow[SchemaTableOptionalColumn.IsHidden] = true;
						dataRow[SchemaTableColumn.BaseColumnName] = this._keyInfo[i].columnName;
						dataRow[SchemaTableColumn.IsExpression] = false;
						dataRow[SchemaTableColumn.IsAliased] = false;
						dataRow[SchemaTableColumn.BaseTableName] = this._keyInfo[i].tableName;
						dataRow[SchemaTableOptionalColumn.BaseCatalogName] = this._keyInfo[i].databaseName;
						dataRow[SchemaTableOptionalColumn.IsAutoIncrement] = true;
						dataRow["DataTypeName"] = "integer";
						tbl.Rows.Add(dataRow);
					}
					else
					{
						keyQuery.Sync(0L);
						using (DataTable schemaTable = keyQuery._reader.GetSchemaTable())
						{
							foreach (DataRow dataRow2 in schemaTable.Rows)
							{
								object[] itemArray = dataRow2.ItemArray;
								DataRow dataRow3 = tbl.Rows.Add(itemArray);
								dataRow3[SchemaTableOptionalColumn.IsHidden] = true;
								dataRow3[SchemaTableColumn.ColumnOrdinal] = tbl.Rows.Count - 1;
							}
						}
					}
				}
			}
		}
	}
}
