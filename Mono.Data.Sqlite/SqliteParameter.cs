using System;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
namespace Mono.Data.Sqlite
{
	public sealed class SqliteParameter : DbParameter, ICloneable
	{
		internal int _dbType;
		private DataRowVersion _rowVersion;
		private object _objValue;
		private string _sourceColumn;
		private string _parameterName;
		private int _dataSize;
		private bool _nullable;
		private bool _nullMapping;
		public override bool IsNullable
		{
			get
			{
				return this._nullable;
			}
			set
			{
				this._nullable = value;
			}
		}
		[RefreshProperties(RefreshProperties.All), DbProviderSpecificTypeProperty(true)]
		public override DbType DbType
		{
			get
			{
				if (this._dbType != -1)
				{
					return (DbType)this._dbType;
				}
				if (this._objValue != null && this._objValue != DBNull.Value)
				{
					return SqliteConvert.TypeToDbType(this._objValue.GetType());
				}
				return DbType.String;
			}
			set
			{
				this._dbType = (int)value;
			}
		}
		public override ParameterDirection Direction
		{
			get
			{
				return ParameterDirection.Input;
			}
			set
			{
				if (value != ParameterDirection.Input)
				{
					throw new NotSupportedException();
				}
			}
		}
		public override string ParameterName
		{
			get
			{
				return this._parameterName;
			}
			set
			{
				this._parameterName = value;
			}
		}
		[DefaultValue(0)]
		public override int Size
		{
			get
			{
				return this._dataSize;
			}
			set
			{
				this._dataSize = value;
			}
		}
		public override string SourceColumn
		{
			get
			{
				return this._sourceColumn;
			}
			set
			{
				this._sourceColumn = value;
			}
		}
		public override bool SourceColumnNullMapping
		{
			get
			{
				return this._nullMapping;
			}
			set
			{
				this._nullMapping = value;
			}
		}
		public override DataRowVersion SourceVersion
		{
			get
			{
				return this._rowVersion;
			}
			set
			{
				this._rowVersion = value;
			}
		}
		[RefreshProperties(RefreshProperties.All), TypeConverter(typeof(StringConverter))]
		public override object Value
		{
			get
			{
				return this._objValue;
			}
			set
			{
				this._objValue = value;
				if (this._dbType == -1 && this._objValue != null && this._objValue != DBNull.Value)
				{
					this._dbType = (int)SqliteConvert.TypeToDbType(this._objValue.GetType());
				}
			}
		}
		public SqliteParameter() : this(null, (DbType)(-1), 0, null, DataRowVersion.Current)
		{
		}
		public SqliteParameter(string parameterName) : this(parameterName, (DbType)(-1), 0, null, DataRowVersion.Current)
		{
		}
		public SqliteParameter(string parameterName, object value) : this(parameterName, (DbType)(-1), 0, null, DataRowVersion.Current)
		{
			this.Value = value;
		}
		public SqliteParameter(string parameterName, DbType dbType) : this(parameterName, dbType, 0, null, DataRowVersion.Current)
		{
		}
		public SqliteParameter(string parameterName, DbType dbType, string sourceColumn) : this(parameterName, dbType, 0, sourceColumn, DataRowVersion.Current)
		{
		}
		public SqliteParameter(string parameterName, DbType dbType, string sourceColumn, DataRowVersion rowVersion) : this(parameterName, dbType, 0, sourceColumn, rowVersion)
		{
		}
		public SqliteParameter(DbType dbType) : this(null, dbType, 0, null, DataRowVersion.Current)
		{
		}
		public SqliteParameter(DbType dbType, object value) : this(null, dbType, 0, null, DataRowVersion.Current)
		{
			this.Value = value;
		}
		public SqliteParameter(DbType dbType, string sourceColumn) : this(null, dbType, 0, sourceColumn, DataRowVersion.Current)
		{
		}
		public SqliteParameter(DbType dbType, string sourceColumn, DataRowVersion rowVersion) : this(null, dbType, 0, sourceColumn, rowVersion)
		{
		}
		public SqliteParameter(string parameterName, DbType parameterType, int parameterSize) : this(parameterName, parameterType, parameterSize, null, DataRowVersion.Current)
		{
		}
		public SqliteParameter(string parameterName, DbType parameterType, int parameterSize, string sourceColumn) : this(parameterName, parameterType, parameterSize, sourceColumn, DataRowVersion.Current)
		{
		}
		public SqliteParameter(string parameterName, DbType parameterType, int parameterSize, string sourceColumn, DataRowVersion rowVersion)
		{
			this._parameterName = parameterName;
			this._dbType = (int)parameterType;
			this._sourceColumn = sourceColumn;
			this._rowVersion = rowVersion;
			this._objValue = null;
			this._dataSize = parameterSize;
			this._nullMapping = false;
			this._nullable = true;
		}
		private SqliteParameter(SqliteParameter source) : this(source.ParameterName, (DbType)source._dbType, 0, source.Direction, source.IsNullable, 0, 0, source.SourceColumn, source.SourceVersion, source.Value)
		{
			this._nullMapping = source._nullMapping;
		}
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public SqliteParameter(string parameterName, DbType parameterType, int parameterSize, ParameterDirection direction, bool isNullable, byte precision, byte scale, string sourceColumn, DataRowVersion rowVersion, object value) : this(parameterName, parameterType, parameterSize, sourceColumn, rowVersion)
		{
			this.Direction = direction;
			this.IsNullable = isNullable;
			this.Value = value;
		}
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public SqliteParameter(string parameterName, DbType parameterType, int parameterSize, ParameterDirection direction, byte precision, byte scale, string sourceColumn, DataRowVersion rowVersion, bool sourceColumnNullMapping, object value) : this(parameterName, parameterType, parameterSize, sourceColumn, rowVersion)
		{
			this.Direction = direction;
			this.SourceColumnNullMapping = sourceColumnNullMapping;
			this.Value = value;
		}
		public SqliteParameter(DbType parameterType, int parameterSize) : this(null, parameterType, parameterSize, null, DataRowVersion.Current)
		{
		}
		public SqliteParameter(DbType parameterType, int parameterSize, string sourceColumn) : this(null, parameterType, parameterSize, sourceColumn, DataRowVersion.Current)
		{
		}
		public SqliteParameter(DbType parameterType, int parameterSize, string sourceColumn, DataRowVersion rowVersion) : this(null, parameterType, parameterSize, sourceColumn, rowVersion)
		{
		}
		public override void ResetDbType()
		{
			this._dbType = -1;
		}
		public object Clone()
		{
			return new SqliteParameter(this);
		}
	}
}
