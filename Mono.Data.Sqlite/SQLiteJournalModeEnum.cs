using System;
namespace Mono.Data.Sqlite
{
	public enum SQLiteJournalModeEnum
	{
		Delete,
		Persist,
		Off
	}
}
