using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Globalization;
namespace Mono.Data.Sqlite
{
	[Editor("Microsoft.VSDesigner.Data.Design.DBParametersEditor, Microsoft.VSDesigner, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), ListBindable(false)]
	public sealed class SqliteParameterCollection : DbParameterCollection
	{
		private SqliteCommand _command;
		private List<SqliteParameter> _parameterList;
		private bool _unboundFlag;
		public override bool IsSynchronized
		{
			get
			{
				return true;
			}
		}
		public override bool IsFixedSize
		{
			get
			{
				return false;
			}
		}
		public override bool IsReadOnly
		{
			get
			{
				return false;
			}
		}
		public override object SyncRoot
		{
			get
			{
				return null;
			}
		}
		public override int Count
		{
			get
			{
				return this._parameterList.Count;
			}
		}
		public new SqliteParameter this[string parameterName]
		{
			get
			{
				return (SqliteParameter)this.GetParameter(parameterName);
			}
			set
			{
				this.SetParameter(parameterName, value);
			}
		}
		public new SqliteParameter this[int index]
		{
			get
			{
				return (SqliteParameter)this.GetParameter(index);
			}
			set
			{
				this.SetParameter(index, value);
			}
		}
		internal SqliteParameterCollection(SqliteCommand cmd)
		{
			this._command = cmd;
			this._parameterList = new List<SqliteParameter>();
			this._unboundFlag = true;
		}
		public override IEnumerator GetEnumerator()
		{
			return this._parameterList.GetEnumerator();
		}
		public SqliteParameter Add(string parameterName, DbType parameterType, int parameterSize, string sourceColumn)
		{
			SqliteParameter sqliteParameter = new SqliteParameter(parameterName, parameterType, parameterSize, sourceColumn);
			this.Add(sqliteParameter);
			return sqliteParameter;
		}
		public SqliteParameter Add(string parameterName, DbType parameterType, int parameterSize)
		{
			SqliteParameter sqliteParameter = new SqliteParameter(parameterName, parameterType, parameterSize);
			this.Add(sqliteParameter);
			return sqliteParameter;
		}
		public SqliteParameter Add(string parameterName, DbType parameterType)
		{
			SqliteParameter sqliteParameter = new SqliteParameter(parameterName, parameterType);
			this.Add(sqliteParameter);
			return sqliteParameter;
		}
		public int Add(SqliteParameter parameter)
		{
			int num = -1;
			if (!string.IsNullOrEmpty(parameter.ParameterName))
			{
				num = this.IndexOf(parameter.ParameterName);
			}
			if (num == -1)
			{
				num = this._parameterList.Count;
				this._parameterList.Add(parameter);
			}
			this.SetParameter(num, parameter);
			return num;
		}
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override int Add(object value)
		{
			return this.Add((SqliteParameter)value);
		}
		public SqliteParameter AddWithValue(string parameterName, object value)
		{
			SqliteParameter sqliteParameter = new SqliteParameter(parameterName, value);
			this.Add(sqliteParameter);
			return sqliteParameter;
		}
		public void AddRange(SqliteParameter[] values)
		{
			int num = values.Length;
			for (int i = 0; i < num; i++)
			{
				this.Add(values[i]);
			}
		}
		public override void AddRange(Array values)
		{
			int length = values.Length;
			for (int i = 0; i < length; i++)
			{
				this.Add((SqliteParameter)values.GetValue(i));
			}
		}
		public override void Clear()
		{
			this._unboundFlag = true;
			this._parameterList.Clear();
		}
		public override bool Contains(string parameterName)
		{
			return this.IndexOf(parameterName) != -1;
		}
		public override bool Contains(object value)
		{
			return this._parameterList.Contains((SqliteParameter)value);
		}
		public override void CopyTo(Array array, int index)
		{
			throw new NotImplementedException();
		}
		protected override DbParameter GetParameter(string parameterName)
		{
			return this.GetParameter(this.IndexOf(parameterName));
		}
		protected override DbParameter GetParameter(int index)
		{
			return this._parameterList[index];
		}
		public override int IndexOf(string parameterName)
		{
			int count = this._parameterList.Count;
			for (int i = 0; i < count; i++)
			{
				if (string.Compare(parameterName, this._parameterList[i].ParameterName, true, CultureInfo.InvariantCulture) == 0)
				{
					return i;
				}
			}
			return -1;
		}
		public override int IndexOf(object value)
		{
			return this._parameterList.IndexOf((SqliteParameter)value);
		}
		public override void Insert(int index, object value)
		{
			this._unboundFlag = true;
			this._parameterList.Insert(index, (SqliteParameter)value);
		}
		public override void Remove(object value)
		{
			this._unboundFlag = true;
			this._parameterList.Remove((SqliteParameter)value);
		}
		public override void RemoveAt(string parameterName)
		{
			this.RemoveAt(this.IndexOf(parameterName));
		}
		public override void RemoveAt(int index)
		{
			this._unboundFlag = true;
			this._parameterList.RemoveAt(index);
		}
		protected override void SetParameter(string parameterName, DbParameter value)
		{
			this.SetParameter(this.IndexOf(parameterName), value);
		}
		protected override void SetParameter(int index, DbParameter value)
		{
			this._unboundFlag = true;
			this._parameterList[index] = (SqliteParameter)value;
		}
		internal void Unbind()
		{
			this._unboundFlag = true;
		}
		internal void MapParameters(SqliteStatement activeStatement)
		{
			if (!this._unboundFlag || this._parameterList.Count == 0 || this._command._statementList == null)
			{
				return;
			}
			int num = 0;
			int num2 = -1;
			foreach (SqliteParameter current in this._parameterList)
			{
				num2++;
				string text = current.ParameterName;
				if (text == null)
				{
					text = string.Format(CultureInfo.InvariantCulture, ";{0}", new object[]
					{
						num
					});
					num++;
				}
				bool flag = false;
				int num3;
				if (activeStatement == null)
				{
					num3 = this._command._statementList.Count;
				}
				else
				{
					num3 = 1;
				}
				SqliteStatement sqliteStatement = activeStatement;
				for (int i = 0; i < num3; i++)
				{
					flag = false;
					if (sqliteStatement == null)
					{
						sqliteStatement = this._command._statementList[i];
					}
					if (sqliteStatement._paramNames != null && sqliteStatement.MapParameter(text, current))
					{
						flag = true;
					}
					sqliteStatement = null;
				}
				if (!flag)
				{
					text = string.Format(CultureInfo.InvariantCulture, ";{0}", new object[]
					{
						num2
					});
					sqliteStatement = activeStatement;
					for (int i = 0; i < num3; i++)
					{
						if (sqliteStatement == null)
						{
							sqliteStatement = this._command._statementList[i];
						}
						if (sqliteStatement._paramNames == null || sqliteStatement.MapParameter(text, current))
						{
						}
						sqliteStatement = null;
					}
				}
			}
			if (activeStatement == null)
			{
				this._unboundFlag = false;
			}
		}
	}
}
