using System;
namespace Mono.Data.Sqlite
{
	public enum UpdateEventType
	{
		Delete = 9,
		Insert = 18,
		Update = 23
	}
}
