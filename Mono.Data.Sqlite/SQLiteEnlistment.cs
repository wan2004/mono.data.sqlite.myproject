using System;
using System.Transactions;
namespace Mono.Data.Sqlite
{
	internal class SQLiteEnlistment : IEnlistmentNotification
	{
		internal SqliteTransaction _transaction;
		internal Transaction _scope;
		internal bool _disposeConnection;
		internal SQLiteEnlistment(SqliteConnection cnn, Transaction scope)
		{
			this._transaction = cnn.BeginTransaction();
			this._scope = scope;
			this._disposeConnection = false;
			this._scope.EnlistVolatile(this, EnlistmentOptions.None);
		}
		private void Cleanup(SqliteConnection cnn)
		{
			if (this._disposeConnection)
			{
				cnn.Dispose();
			}
			this._transaction = null;
			this._scope = null;
		}
		public void Commit(Enlistment enlistment)
		{
			SqliteConnection connection = this._transaction.Connection;
			connection._enlistment = null;
			try
			{
				this._transaction.IsValid(true);
				this._transaction.Connection._transactionLevel = 1;
				this._transaction.Commit();
				enlistment.Done();
			}
			finally
			{
				this.Cleanup(connection);
			}
		}
		public void InDoubt(Enlistment enlistment)
		{
			enlistment.Done();
		}
		public void Prepare(PreparingEnlistment preparingEnlistment)
		{
			if (!this._transaction.IsValid(false))
			{
				preparingEnlistment.ForceRollback();
			}
			else
			{
				preparingEnlistment.Prepared();
			}
		}
		public void Rollback(Enlistment enlistment)
		{
			SqliteConnection connection = this._transaction.Connection;
			connection._enlistment = null;
			try
			{
				this._transaction.Rollback();
				enlistment.Done();
			}
			finally
			{
				this.Cleanup(connection);
			}
		}
	}
}
