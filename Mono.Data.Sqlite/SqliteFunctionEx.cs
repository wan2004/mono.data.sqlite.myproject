using System;
namespace Mono.Data.Sqlite
{
	public class SqliteFunctionEx : SqliteFunction
	{
		protected CollationSequence GetCollationSequence()
		{
			return this._base.GetCollationSequence(this, this._context);
		}
	}
}
