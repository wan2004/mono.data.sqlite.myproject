using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Globalization;
namespace Mono.Data.Sqlite
{
	public sealed class SqliteDataReader : DbDataReader
	{
		private SqliteCommand _command;
		private int _activeStatementIndex;
		public SqliteStatement _activeStatement;
		private int _readingState;
		private int _rowsAffected;
		private int _fieldCount;
		private int _stepCount;
		private SQLiteType[] _fieldTypeArray;
		private CommandBehavior _commandBehavior;
		internal bool _disposeCommand;
		private SqliteKeyReader _keyInfo;
		internal long _version;
		public override int Depth
		{
			get
			{
				this.CheckClosed();
				return 0;
			}
		}
		public override int FieldCount
		{
			get
			{
				this.CheckClosed();
				if (this._keyInfo == null)
				{
					return this._fieldCount;
				}
				return this._fieldCount + this._keyInfo.Count;
			}
		}
		public int StepCount
	    {
	        get
	        {
	            CheckClosed();
	
	            return _stepCount;
	        }
	    }
		
		public override int VisibleFieldCount
		{
			get
			{
				this.CheckClosed();
				return this._fieldCount;
			}
		}
		public override bool HasRows
		{
			get
			{
				this.CheckClosed();
				return this._readingState != 1 || _stepCount > 0;
			}
		}
		public override bool IsClosed
		{
			get
			{
				return this._command == null;
			}
		}
		public override int RecordsAffected
		{
			get
			{
				return (this._rowsAffected >= 0) ? this._rowsAffected : 0;
			}
		}
		public override object this[string name]
		{
			get
			{
				return this.GetValue(this.GetOrdinal(name));
			}
		}
		public override object this[int i]
		{
			get
			{
				return this.GetValue(i);
			}
		}
		internal SqliteDataReader(SqliteCommand cmd, CommandBehavior behave)
		{
			this._command = cmd;
			this._version = this._command.Connection._version;
			this._commandBehavior = behave;
			this._activeStatementIndex = -1;
			this._activeStatement = null;
			this._rowsAffected = -1;
			this._fieldCount = 0;
			if (this._command != null)
			{
				this.NextResult();
			}
		}
		internal void Cancel()
		{
			this._version = 0L;
		}
		public override void Close()
		{
			try
			{
				if (this._command != null)
				{
					try
					{
						try
						{
							if (this._version != 0L)
							{
								try
								{
									while (this.NextResult())
									{
									}
								}
								catch
								{
								}
							}
							this._command.ClearDataReader();
						}
						finally
						{
							if ((this._commandBehavior & CommandBehavior.CloseConnection) != CommandBehavior.Default && this._command.Connection != null)
							{
								this._command.Connection.Close();
							}
						}
					}
					finally
					{
						if (this._disposeCommand)
						{
							this._command.Dispose();
						}
					}
				}
				this._command = null;
				this._activeStatement = null;
				this._fieldTypeArray = null;
			}
			finally
			{
				if (this._keyInfo != null)
				{
					this._keyInfo.Dispose();
					this._keyInfo = null;
				}
			}
		}
		private void CheckClosed()
		{
			if (this._command == null)
			{
				throw new InvalidOperationException("DataReader has been closed");
			}
			if (this._version == 0L)
			{
				throw new SqliteException(4, "Execution was aborted by the user");
			}
			if (this._command.Connection.State != ConnectionState.Open || this._command.Connection._version != this._version)
			{
				throw new InvalidOperationException("Connection was closed, statement was terminated");
			}
		}
		private void CheckValidRow()
		{
			if (this._readingState != 0)
			{
				throw new InvalidOperationException("No current row");
			}
		}
		public override IEnumerator GetEnumerator()
		{
			return new DbEnumerator(this, (this._commandBehavior & CommandBehavior.CloseConnection) == CommandBehavior.CloseConnection);
		}
		private TypeAffinity VerifyType(int i, DbType typ)
		{
			this.CheckClosed();
			this.CheckValidRow();
			TypeAffinity affinity = this.GetSQLiteType(i).Affinity;
			switch (affinity)
			{
			case TypeAffinity.Int64:
				if (typ == DbType.Int16)
				{
					return affinity;
				}
				if (typ == DbType.Int32)
				{
					return affinity;
				}
				if (typ == DbType.Int64)
				{
					return affinity;
				}
				if (typ == DbType.Boolean)
				{
					return affinity;
				}
				if (typ == DbType.Byte)
				{
					return affinity;
				}
				if (typ == DbType.DateTime)
				{
					return affinity;
				}
				if (typ == DbType.Single)
				{
					return affinity;
				}
				if (typ == DbType.Double)
				{
					return affinity;
				}
				if (typ == DbType.Decimal)
				{
					return affinity;
				}
				break;
			case TypeAffinity.Double:
				if (typ == DbType.Single)
				{
					return affinity;
				}
				if (typ == DbType.Double)
				{
					return affinity;
				}
				if (typ == DbType.Decimal)
				{
					return affinity;
				}
				if (typ == DbType.DateTime)
				{
					return affinity;
				}
				break;
			case TypeAffinity.Text:
				if (typ == DbType.SByte)
				{
					return affinity;
				}
				if (typ == DbType.String)
				{
					return affinity;
				}
				if (typ == DbType.SByte)
				{
					return affinity;
				}
				if (typ == DbType.Guid)
				{
					return affinity;
				}
				if (typ == DbType.DateTime)
				{
					return affinity;
				}
				if (typ == DbType.Decimal)
				{
					return affinity;
				}
				break;
			case TypeAffinity.Blob:
				if (typ == DbType.Guid)
				{
					return affinity;
				}
				if (typ == DbType.String)
				{
					return affinity;
				}
				if (typ == DbType.Binary)
				{
					return affinity;
				}
				break;
			}
			throw new InvalidCastException();
		}
		public override bool GetBoolean(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetBoolean(i - this.VisibleFieldCount);
			}
			this.VerifyType(i, DbType.Boolean);
			return Convert.ToBoolean(this.GetValue(i), CultureInfo.CurrentCulture);
		}
		public override byte GetByte(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetByte(i - this.VisibleFieldCount);
			}
			this.VerifyType(i, DbType.Byte);
			return Convert.ToByte(this._activeStatement._sql.GetInt32(this._activeStatement, i));
		}
		public override long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetBytes(i - this.VisibleFieldCount, fieldOffset, buffer, bufferoffset, length);
			}
			this.VerifyType(i, DbType.Binary);
			return this._activeStatement._sql.GetBytes(this._activeStatement, i, (int)fieldOffset, buffer, bufferoffset, length);
		}
		public override char GetChar(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetChar(i - this.VisibleFieldCount);
			}
			this.VerifyType(i, DbType.SByte);
			return Convert.ToChar(this._activeStatement._sql.GetInt32(this._activeStatement, i));
		}
		public override long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetChars(i - this.VisibleFieldCount, fieldoffset, buffer, bufferoffset, length);
			}
			this.VerifyType(i, DbType.String);
			return this._activeStatement._sql.GetChars(this._activeStatement, i, (int)fieldoffset, buffer, bufferoffset, length);
		}
		public override string GetDataTypeName(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetDataTypeName(i - this.VisibleFieldCount);
			}
			SQLiteType sQLiteType = this.GetSQLiteType(i);
			if (sQLiteType.Type == DbType.Object)
			{
				return SqliteConvert.SQLiteTypeToType(sQLiteType).Name;
			}
			return this._activeStatement._sql.ColumnType(this._activeStatement, i, out sQLiteType.Affinity);
		}
		public override DateTime GetDateTime(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetDateTime(i - this.VisibleFieldCount);
			}
			this.VerifyType(i, DbType.DateTime);
			return this._activeStatement._sql.GetDateTime(this._activeStatement, i);
		}
		public override decimal GetDecimal(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetDecimal(i - this.VisibleFieldCount);
			}
			this.VerifyType(i, DbType.Decimal);
			return decimal.Parse(this._activeStatement._sql.GetText(this._activeStatement, i), NumberStyles.AllowDecimalPoint | NumberStyles.AllowExponent, CultureInfo.InvariantCulture);
		}
		public override double GetDouble(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetDouble(i - this.VisibleFieldCount);
			}
			this.VerifyType(i, DbType.Double);
			return this._activeStatement._sql.GetDouble(this._activeStatement, i);
		}
		public override Type GetFieldType(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetFieldType(i - this.VisibleFieldCount);
			}
			return SqliteConvert.SQLiteTypeToType(this.GetSQLiteType(i));
		}
		public override float GetFloat(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetFloat(i - this.VisibleFieldCount);
			}
			this.VerifyType(i, DbType.Single);
			return Convert.ToSingle(this._activeStatement._sql.GetDouble(this._activeStatement, i));
		}
		public override Guid GetGuid(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetGuid(i - this.VisibleFieldCount);
			}
			TypeAffinity typeAffinity = this.VerifyType(i, DbType.Guid);
			if (typeAffinity == TypeAffinity.Blob)
			{
				byte[] array = new byte[16];
				this._activeStatement._sql.GetBytes(this._activeStatement, i, 0, array, 0, 16);
				return new Guid(array);
			}
			return new Guid(this._activeStatement._sql.GetText(this._activeStatement, i));
		}
		public override short GetInt16(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetInt16(i - this.VisibleFieldCount);
			}
			this.VerifyType(i, DbType.Int16);
			return Convert.ToInt16(this._activeStatement._sql.GetInt32(this._activeStatement, i));
		}
		public override int GetInt32(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetInt32(i - this.VisibleFieldCount);
			}
			this.VerifyType(i, DbType.Int32);
			return this._activeStatement._sql.GetInt32(this._activeStatement, i);
		}
		public override long GetInt64(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetInt64(i - this.VisibleFieldCount);
			}
			this.VerifyType(i, DbType.Int64);
			return this._activeStatement._sql.GetInt64(this._activeStatement, i);
		}
		public override string GetName(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetName(i - this.VisibleFieldCount);
			}
			return this._activeStatement._sql.ColumnName(this._activeStatement, i);
		}
		public override int GetOrdinal(string name)
		{
			this.CheckClosed();
			int num = this._activeStatement._sql.ColumnIndex(this._activeStatement, name);
			if (num == -1 && this._keyInfo != null)
			{
				num = this._keyInfo.GetOrdinal(name);
				if (num > -1)
				{
					num += this.VisibleFieldCount;
				}
			}
			return num;
		}
		public override DataTable GetSchemaTable()
		{
			return this.GetSchemaTable(true, false);
		}
		internal DataTable GetSchemaTable(bool wantUniqueInfo, bool wantDefaultValue)
		{
			this.CheckClosed();
			DataTable dataTable = new DataTable("SchemaTable");
			DataTable dataTable2 = null;
			string b = string.Empty;
			string b2 = string.Empty;
			string text = string.Empty;
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add(SchemaTableColumn.ColumnName, typeof(string));
			dataTable.Columns.Add(SchemaTableColumn.ColumnOrdinal, typeof(int));
			dataTable.Columns.Add(SchemaTableColumn.ColumnSize, typeof(int));
			dataTable.Columns.Add(SchemaTableColumn.NumericPrecision, typeof(short));
			dataTable.Columns.Add(SchemaTableColumn.NumericScale, typeof(short));
			dataTable.Columns.Add(SchemaTableColumn.IsUnique, typeof(bool));
			dataTable.Columns.Add(SchemaTableColumn.IsKey, typeof(bool));
			dataTable.Columns.Add(SchemaTableOptionalColumn.BaseServerName, typeof(string));
			dataTable.Columns.Add(SchemaTableOptionalColumn.BaseCatalogName, typeof(string));
			dataTable.Columns.Add(SchemaTableColumn.BaseColumnName, typeof(string));
			dataTable.Columns.Add(SchemaTableColumn.BaseSchemaName, typeof(string));
			dataTable.Columns.Add(SchemaTableColumn.BaseTableName, typeof(string));
			dataTable.Columns.Add(SchemaTableColumn.DataType, typeof(Type));
			dataTable.Columns.Add(SchemaTableColumn.AllowDBNull, typeof(bool));
			dataTable.Columns.Add(SchemaTableColumn.ProviderType, typeof(int));
			dataTable.Columns.Add(SchemaTableColumn.IsAliased, typeof(bool));
			dataTable.Columns.Add(SchemaTableColumn.IsExpression, typeof(bool));
			dataTable.Columns.Add(SchemaTableOptionalColumn.IsAutoIncrement, typeof(bool));
			dataTable.Columns.Add(SchemaTableOptionalColumn.IsRowVersion, typeof(bool));
			dataTable.Columns.Add(SchemaTableOptionalColumn.IsHidden, typeof(bool));
			dataTable.Columns.Add(SchemaTableColumn.IsLong, typeof(bool));
			dataTable.Columns.Add(SchemaTableOptionalColumn.IsReadOnly, typeof(bool));
			dataTable.Columns.Add(SchemaTableOptionalColumn.ProviderSpecificDataType, typeof(Type));
			dataTable.Columns.Add(SchemaTableOptionalColumn.DefaultValue, typeof(object));
			dataTable.Columns.Add("DataTypeName", typeof(string));
			dataTable.Columns.Add("CollationType", typeof(string));
			dataTable.BeginLoadData();
			for (int i = 0; i < this._fieldCount; i++)
			{
				DataRow dataRow = dataTable.NewRow();
				DbType type = this.GetSQLiteType(i).Type;
				dataRow[SchemaTableColumn.ColumnName] = this.GetName(i);
				dataRow[SchemaTableColumn.ColumnOrdinal] = i;
				dataRow[SchemaTableColumn.ColumnSize] = SqliteConvert.DbTypeToColumnSize(type);
				dataRow[SchemaTableColumn.NumericPrecision] = SqliteConvert.DbTypeToNumericPrecision(type);
				dataRow[SchemaTableColumn.NumericScale] = SqliteConvert.DbTypeToNumericScale(type);
				dataRow[SchemaTableColumn.ProviderType] = this.GetSQLiteType(i).Type;
				dataRow[SchemaTableColumn.IsLong] = false;
				dataRow[SchemaTableColumn.AllowDBNull] = true;
				dataRow[SchemaTableOptionalColumn.IsReadOnly] = false;
				dataRow[SchemaTableOptionalColumn.IsRowVersion] = false;
				dataRow[SchemaTableColumn.IsUnique] = false;
				dataRow[SchemaTableColumn.IsKey] = false;
				dataRow[SchemaTableOptionalColumn.IsAutoIncrement] = false;
				dataRow[SchemaTableColumn.DataType] = this.GetFieldType(i);
				dataRow[SchemaTableOptionalColumn.IsHidden] = false;
				
#if DEBUG				
				IntPtr ptr = this._activeStatement._sqlite_stmt;
				System.Console.Error.WriteLine(string.Format(" _activeStatement 0x{0:X}" , ptr.ToInt32()) ); 
#endif
				
				text = this._command.Connection._sql.ColumnOriginalName(this._activeStatement, i);
				if (!string.IsNullOrEmpty(text))
				{
					dataRow[SchemaTableColumn.BaseColumnName] = text;
				}
				dataRow[SchemaTableColumn.IsExpression] = string.IsNullOrEmpty(text);
				dataRow[SchemaTableColumn.IsAliased] = (string.Compare(this.GetName(i), text, true, CultureInfo.InvariantCulture) != 0);
				string value = this._command.Connection._sql.ColumnTableName(this._activeStatement, i);
				if (!string.IsNullOrEmpty(value))
				{
					dataRow[SchemaTableColumn.BaseTableName] = value;
				}
				value = this._command.Connection._sql.ColumnDatabaseName(this._activeStatement, i);
				if (!string.IsNullOrEmpty(value))
				{
					dataRow[SchemaTableOptionalColumn.BaseCatalogName] = value;
				}
				string text2 = null;
				if (!string.IsNullOrEmpty(text))
				{
					string value2;
					bool flag;
					bool flag2;
					bool flag3;
					this._command.Connection._sql.ColumnMetaData((string)dataRow[SchemaTableOptionalColumn.BaseCatalogName], (string)dataRow[SchemaTableColumn.BaseTableName], text, out text2, out value2, out flag, out flag2, out flag3);
					if (flag || flag2)
					{
						dataRow[SchemaTableColumn.AllowDBNull] = false;
					}
					dataRow[SchemaTableColumn.IsKey] = flag2;
					dataRow[SchemaTableOptionalColumn.IsAutoIncrement] = flag3;
					dataRow["CollationType"] = value2;
					string[] array = text2.Split(new char[]
					{
						'('
					});
					if (array.Length > 1)
					{
						text2 = array[0];
						array = array[1].Split(new char[]
						{
							')'
						});
						if (array.Length > 1)
						{
							array = array[0].Split(new char[]
							{
								',',
								'.'
							});
							if (this.GetSQLiteType(i).Type == DbType.String || this.GetSQLiteType(i).Type == DbType.Binary)
							{
								dataRow[SchemaTableColumn.ColumnSize] = Convert.ToInt32(array[0], CultureInfo.InvariantCulture);
							}
							else
							{
								dataRow[SchemaTableColumn.NumericPrecision] = Convert.ToInt32(array[0], CultureInfo.InvariantCulture);
								if (array.Length > 1)
								{
									dataRow[SchemaTableColumn.NumericScale] = Convert.ToInt32(array[1], CultureInfo.InvariantCulture);
								}
							}
						}
					}
					if (wantDefaultValue)
					{
						using (SqliteCommand sqliteCommand = new SqliteCommand(string.Format(CultureInfo.InvariantCulture, "PRAGMA [{0}].TABLE_INFO([{1}])", new object[]
						{
							dataRow[SchemaTableOptionalColumn.BaseCatalogName],
							dataRow[SchemaTableColumn.BaseTableName]
						}), this._command.Connection))
						{
							using (DbDataReader dbDataReader = sqliteCommand.ExecuteReader())
							{
								while (dbDataReader.Read())
								{
									if (string.Compare((string)dataRow[SchemaTableColumn.BaseColumnName], dbDataReader.GetString(1), true, CultureInfo.InvariantCulture) == 0)
									{
										if (!dbDataReader.IsDBNull(4))
										{
											dataRow[SchemaTableOptionalColumn.DefaultValue] = dbDataReader[4];
										}
										break;
									}
								}
							}
						}
					}
					if (wantUniqueInfo)
					{
						if ((string)dataRow[SchemaTableOptionalColumn.BaseCatalogName] != b || (string)dataRow[SchemaTableColumn.BaseTableName] != b2)
						{
							b = (string)dataRow[SchemaTableOptionalColumn.BaseCatalogName];
							b2 = (string)dataRow[SchemaTableColumn.BaseTableName];
							SqliteConnection arg_81C_0 = this._command.Connection;
							string arg_81C_1 = "Indexes";
							string[] expr_7F6 = new string[4];
							expr_7F6[0] = (string)dataRow[SchemaTableOptionalColumn.BaseCatalogName];
							expr_7F6[2] = (string)dataRow[SchemaTableColumn.BaseTableName];
							dataTable2 = arg_81C_0.GetSchema(arg_81C_1, expr_7F6);
						}
						foreach (DataRow dataRow2 in dataTable2.Rows)
						{
							SqliteConnection arg_892_0 = this._command.Connection;
							string arg_892_1 = "IndexColumns";
							string[] expr_858 = new string[5];
							expr_858[0] = (string)dataRow[SchemaTableOptionalColumn.BaseCatalogName];
							expr_858[2] = (string)dataRow[SchemaTableColumn.BaseTableName];
							expr_858[3] = (string)dataRow2["INDEX_NAME"];
							DataTable schema = arg_892_0.GetSchema(arg_892_1, expr_858);
							foreach (DataRow dataRow3 in schema.Rows)
							{
								if (string.Compare((string)dataRow3["COLUMN_NAME"], text, true, CultureInfo.InvariantCulture) == 0)
								{
									if (schema.Rows.Count == 1 && !(bool)dataRow[SchemaTableColumn.AllowDBNull])
									{
										dataRow[SchemaTableColumn.IsUnique] = dataRow2["UNIQUE"];
									}
									if (schema.Rows.Count != 1 || !(bool)dataRow2["PRIMARY_KEY"] || string.IsNullOrEmpty(text2) || string.Compare(text2, "integer", true, CultureInfo.InvariantCulture) == 0)
									{
									}
									break;
								}
							}
						}
					}
					if (string.IsNullOrEmpty(text2))
					{
						TypeAffinity typeAffinity;
						text2 = this._activeStatement._sql.ColumnType(this._activeStatement, i, out typeAffinity);
					}
					if (!string.IsNullOrEmpty(text2))
					{
						dataRow["DataTypeName"] = text2;
					}
				}
				dataTable.Rows.Add(dataRow);
			}
			if (this._keyInfo != null)
			{
				this._keyInfo.AppendSchemaTable(dataTable);
			}
			dataTable.AcceptChanges();
			dataTable.EndLoadData();
			return dataTable;
		}
		public override string GetString(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetString(i - this.VisibleFieldCount);
			}
			this.VerifyType(i, DbType.String);
			return this._activeStatement._sql.GetText(this._activeStatement, i);
		}
		public override object GetValue(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.GetValue(i - this.VisibleFieldCount);
			}
			SQLiteType sQLiteType = this.GetSQLiteType(i);
			return this._activeStatement._sql.GetValue(this._activeStatement, i, sQLiteType);
		}
		public override int GetValues(object[] values)
		{
			int num = this.FieldCount;
			if (values.Length < num)
			{
				num = values.Length;
			}
			for (int i = 0; i < num; i++)
			{
				values[i] = this.GetValue(i);
			}
			return num;
		}
		public override bool IsDBNull(int i)
		{
			if (i >= this.VisibleFieldCount && this._keyInfo != null)
			{
				return this._keyInfo.IsDBNull(i - this.VisibleFieldCount);
			}
			return this._activeStatement._sql.IsNull(this._activeStatement, i);
		}
//		public override bool NextResult()
//		{
//			this.CheckClosed();
//			SqliteStatement sqliteStatement = null;
//			int num;
//			while (true)
//			{
//				if (this._activeStatement != null && sqliteStatement == null)
//				{
//					this._activeStatement._sql.Reset(this._activeStatement);
//					if ((this._commandBehavior & CommandBehavior.SingleResult) != CommandBehavior.Default)
//					{
//						break;
//					}
//				}
//				sqliteStatement = this._command.GetStatement(this._activeStatementIndex + 1);
//				if (sqliteStatement == null)
//				{
//					return false;
//				}
//				if (this._readingState < 1)
//				{
//					this._readingState = 1;
//				}
//				this._activeStatementIndex++;
//				num = sqliteStatement._sql.ColumnCount(sqliteStatement);
//				if ((this._commandBehavior & CommandBehavior.SchemaOnly) != CommandBehavior.Default && num != 0)
//				{
//					goto IL_190;
//				}
//				if (sqliteStatement._sql.Step(sqliteStatement))
//				{
//					goto Block_10;
//				}
//				if (num != 0)
//				{
//					goto IL_189;
//				}
//				if (this._rowsAffected == -1)
//				{
//					this._rowsAffected = 0;
//				}
//				this._rowsAffected += sqliteStatement._sql.Changes;
//				sqliteStatement._sql.Reset(sqliteStatement);
//			}
//			while (true)
//			{
//				sqliteStatement = this._command.GetStatement(this._activeStatementIndex + 1);
//				if (sqliteStatement == null)
//				{
//					break;
//				}
//				this._activeStatementIndex++;
//				sqliteStatement._sql.Step(sqliteStatement);
//				if (sqliteStatement._sql.ColumnCount(sqliteStatement) == 0)
//				{
//					if (this._rowsAffected == -1)
//					{
//						this._rowsAffected = 0;
//					}
//					this._rowsAffected += sqliteStatement._sql.Changes;
//				}
//				sqliteStatement._sql.Reset(sqliteStatement);
//			}
//			return false;
//			Block_10:
//			this._readingState = -1;
//			goto IL_190;
//			IL_189:
//			this._readingState = 1;
//			IL_190:
//			this._activeStatement = sqliteStatement;
//			this._fieldCount = num;
//			this._fieldTypeArray = null;
//			if ((this._commandBehavior & CommandBehavior.KeyInfo) != CommandBehavior.Default)
//			{
//				this.LoadKeyInfo();
//			}
//			return true;
//		}
		
		
		/// <summary>
	    /// Moves to the next resultset in multiple row-returning SQL command.
	    /// </summary>
	    /// <returns>True if the command was successful and a new resultset is available, False otherwise.</returns>
	    public override bool NextResult()
	    {
			CheckClosed();
			
			SqliteStatement stmt = null;
			int fieldCount;
			bool schemaOnly = ((_commandBehavior & CommandBehavior.SchemaOnly) != 0);
			
			while (true)
			{
				if (stmt == null && _activeStatement != null && _activeStatement._sql != null && _activeStatement._sql.IsOpen())
				{
				  // Reset the previously-executed statement
				  if (!schemaOnly) _activeStatement._sql.Reset(_activeStatement);
				
				  // If we're only supposed to return a single rowset, step through all remaining statements once until
				  // they are all done and return false to indicate no more resultsets exist.
				  if ((_commandBehavior & CommandBehavior.SingleResult) != 0)
				  {
				    for (; ; )
				    {
				      stmt = _command.GetStatement(_activeStatementIndex + 1);
				      if (stmt == null) break;
				      _activeStatementIndex++;
				
				      if (!schemaOnly && stmt._sql.Step(stmt)) _stepCount++;
				      if (stmt._sql.ColumnCount(stmt) == 0)
				      {
				        if (_rowsAffected == -1) _rowsAffected = 0;
				        int changes = 0;
				        if (stmt.TryGetChanges(ref changes))
				            _rowsAffected += changes;
				        else
				            return false;
				      }
				      if (!schemaOnly) stmt._sql.Reset(stmt); // Gotta reset after every step to release any locks and such!
				    }
				    return false;
				  }
				}
				
				// Get the next statement to execute
				stmt = _command.GetStatement(_activeStatementIndex + 1);
				
				// If we've reached the end of the statements, return false, no more resultsets
				if (stmt == null)
				  return false;
				
				// If we were on a current resultset, set the state to "done reading" for it
				if (_readingState < 1)
				  _readingState = 1;
				
				_activeStatementIndex++;
				
				fieldCount = stmt._sql.ColumnCount(stmt);
				
				// If the statement is not a select statement or we're not retrieving schema only, then perform the initial step
				if (!schemaOnly || (fieldCount == 0))
				{
				  if (!schemaOnly && stmt._sql.Step(stmt))
				  {
				    _stepCount++;
				    _readingState = -1;
				  }
				  else if (fieldCount == 0) // No rows returned, if fieldCount is zero, skip to the next statement
				  {
				    if (_rowsAffected == -1) _rowsAffected = 0;
				    int changes = 0;
				    if (stmt.TryGetChanges(ref changes))
				      _rowsAffected += changes;
				    else
				      return false;
				    if (!schemaOnly) stmt._sql.Reset(stmt);
				    continue; // Skip this command and move to the next, it was not a row-returning resultset
				  }
				  else // No rows, fieldCount is non-zero so stop here
				  {
				    _readingState = 1; // This command returned columns but no rows, so return true, but HasRows = false and Read() returns false
				  }
				}
				
				// Ahh, we found a row-returning resultset eligible to be returned!
				_activeStatement = stmt;
				_fieldCount = fieldCount;
				_fieldTypeArray = null;
				
				if ((_commandBehavior & CommandBehavior.KeyInfo) != 0)
				  LoadKeyInfo();
				
				return true;
			}
	    }
		
		private SQLiteType GetSQLiteType(int i)
		{
			if (this._fieldTypeArray == null)
			{
				this._fieldTypeArray = new SQLiteType[this.VisibleFieldCount];
			}
			if (this._fieldTypeArray[i] == null)
			{
				this._fieldTypeArray[i] = new SQLiteType();
			}
			SQLiteType sQLiteType = this._fieldTypeArray[i];
			if (sQLiteType.Affinity == TypeAffinity.Uninitialized)
			{
				sQLiteType.Type = SqliteConvert.TypeNameToDbType(this._activeStatement._sql.ColumnType(this._activeStatement, i, out sQLiteType.Affinity));
			}
			else
			{
				sQLiteType.Affinity = this._activeStatement._sql.ColumnAffinity(this._activeStatement, i);
			}
			return sQLiteType;
		}
		public override bool Read()
		{
			this.CheckClosed();
			if (this._readingState == -1)
			{
				this._readingState = 0;
				return true;
			}
			if (this._readingState == 0)
			{
				if ((this._commandBehavior & CommandBehavior.SingleRow) == CommandBehavior.Default && this._activeStatement._sql.Step(this._activeStatement))
				{
					if (this._keyInfo != null)
					{
						this._keyInfo.Reset();
					}
					return true;
				}
				this._readingState = 1;
			}
			return false;
		}
		private void LoadKeyInfo()
		{
			if (this._keyInfo != null)
			{
				this._keyInfo.Dispose();
			}
			this._keyInfo = new SqliteKeyReader(this._command.Connection, this, this._activeStatement);
		}
	}
}
