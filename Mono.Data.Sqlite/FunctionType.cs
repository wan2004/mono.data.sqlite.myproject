using System;
namespace Mono.Data.Sqlite
{
	public enum FunctionType
	{
		Scalar,
		Aggregate,
		Collation
	}
}
