// /*--------------------------------------------------------------*/
// //     CopyRight  			Lucius 
// //     Anthor       			Lucius
// //     Email				lxwan2004@Gmail.com 
// //     Private license
// //     If commercial use please contact Email
// /*--------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Mono.Data.Sqlite
{
	public class Sqlite3Extend
	{
		public static IntPtr OpenDataBaseDefaultConfig(string dbFilePath)
		{
			byte[] array = SqliteConvert.ToUTF8(dbFilePath);
			GCHandle gCHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
			IntPtr pFilePath = gCHandle.AddrOfPinnedObject();
			IntPtr dbPtr = UnsafeNativeMethods.my_sqlite3_open_default(pFilePath);
			gCHandle.Free();
			return dbPtr;
		}
		
		public static int OpenDataBase(string dbFilePath,out IntPtr dbHandle,int flags)
		{
			IntPtr db = IntPtr.Zero;
			byte[] array = SqliteConvert.ToUTF8(dbFilePath);
			GCHandle gCHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
			IntPtr pFilePath = gCHandle.AddrOfPinnedObject();
			int inFlags = (int)flags;
			int relvalue = UnsafeNativeMethods.my_sqlite3_open_default_v2(pFilePath,ref db,inFlags);
			dbHandle = db;
			gCHandle.Free();
			
			return relvalue;
		}
		
		public static bool CloseDataBase(IntPtr dbHandle)
		{
			if(dbHandle != IntPtr.Zero && dbHandle != null)
			{
				int rel = UnsafeNativeMethods.my_sqlite3_close(dbHandle);
				return rel == 0;
			}else
				return false;
		}
		
		public static string GetLastError(SqliteConnection conn)
		{
			SqliteConnectionHandle dbHandle = conn._sql.GetSqliteHandle();
			
			IntPtr ptr = UnsafeNativeMethods.my_sqlite3_errmsg(dbHandle);
			string msg = Marshal.PtrToStringAnsi(ptr);
			return msg;
		}
		
		public static string GetStatementColumnStr( SqliteDataReader reader )
		{
			IntPtr ptr = IntPtr.Zero;
			
			int len = UnsafeNativeMethods.my_sqlite3_statement_columns(reader._activeStatement._sqlite_stmt , ref ptr);
			string str = Marshal.PtrToStringAnsi( ptr );
			
			UnsafeNativeMethods.my_sqlite3_output_free(ptr);
			int op = UnsafeNativeMethods.my_sqlite3_reset(reader._activeStatement._sqlite_stmt);
			if(op!=0)
				System.Console.Error.WriteLine("GetStatementColumnStr And Reset Statment Fail = "+reader._activeStatement._sqlite_stmt);
			
			return str;	
		}
		
		public static string GetSqlColumnStr(SqliteConnection conn, string sql )
		{
			
			return GetSqlColumnStr( (conn._sql as SQLite3).GetSqliteHandle(),sql);	
		}
		
		public static string GetSqlColumnStr(IntPtr connPtr, string sql )
		{
			IntPtr ptr = IntPtr.Zero;
			byte[] array = SqliteConvert.ToUTF8(sql);
			GCHandle gCHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
			IntPtr pSql = gCHandle.AddrOfPinnedObject();
			
			int len = UnsafeNativeMethods.my_sqlite3_sql_columns(connPtr, pSql, ref ptr);
			
			string str = Marshal.PtrToStringAnsi( ptr );
			
			UnsafeNativeMethods.my_sqlite3_output_free(ptr);
			gCHandle.Free();
			return str;	
		}
		
		public static List<string> GetStatementColumns( SqliteDataReader reader )
		{
			List<string> rel = new List<string>();
			string str = GetStatementColumnStr(reader);
			
			string[] names = str.Split("|".ToCharArray());
			foreach(string tName in names)
			{
				rel.Add(tName);
			}
			return rel;	
		}
	}
}

